// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XBUTTON_H
#define XBUTTON_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xbutton : this is a gui button.
///
/// Press away guys!
///

class xbutton : public xelement {
	public:

		///
		/// constructor.
		/// \param dev pointer to irrlicht
		/// \param id id.
		/// \param x2drect width and height.
		/// \param c name?
		///
		xbutton( irr::IrrlichtDevice *dev, int id, x2drect r, char * c );

		///
		/// destructor.
		///
		~xbutton();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		///
		/// show/hide element
		///
		void setVisible( bool v );
		void setParent( xelement * x );
		void setImage( xtexture * t );
		void setPressedImage( xtexture * t );
		void setText( char * c );
		void setDrawBorder( bool b );
		void setPosition( x2dpoint p );
        void setAlignment( int n1, int n2, int n3, int n4 );
    private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;            // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		IGUIButton * button;
		float w, h;
};

#endif // XBUTTON_H
