/* Lua translations. */

%module xcore

class xtexture {
	public:
		xtexture( irr::IrrlichtDevice * dev, char * filename );
		~xtexture ();
		irr::video::ITexture * texture;
};

class xfont {
	public:
		xfont( irr::IrrlichtDevice * dev, char * filename );
		xfont( irr::IrrlichtDevice * dev );
		~xfont ();
		void draw( char * t, x2drect r, xcolour c );
		irr::gui::IGUIFont * getFont();
};

class xresult {
	public:
		int getHeadCount();
		int getDataCount();
		int getRowCount();
		void printAll();
		const char *  getHead( int i );
		const char * getData( int i );
};

class xsql {
	public:
		xsql( char * tablename );
		~xsql ();
		xresult * exe( char * s_exe );
		void removeResult( xresult * r);
};

enum XOP_TYPE {
    XOP_CONN,
    XOP_DISCO,
    XOP_NICK,
    XOP_AUTH,
    XOP_ALLOW,
    XOP_DENY,
    XOP_VERSION,
    XOP_BADVER,
    XOP_VALID,
    XOP_DATA,
    XOP_ADD,
    XOP_DEL,
    XOP_POS,
    XOP_ROT,
    XOP_STAT,
    XOP_ACT,
    XOP_ZERO,
    XOP_CLEAR,
    XOP_ZONE,
    XOP_JOIN,
    XOP_PART,
    XOP_UPDATE,
    XOP_MSG,
    XOP_MSGTO,
    XOP_BOT,
    XOP_FREQ,
    XOP_TRANS,
    XOP_EQUIP,
    XOP_MANIFEST,
    XOP_KILL,
    XOP_SPAWN,
    XOP_GIVE,
    XOP_KICK,
    XOP_BAN,
    XOP_MODE,
    XOP_END,
    XOP_DBG
};

class xpacket {
	public:
		xpacket();
		~xpacket ();
		char * getMessage();
		int getID();
		int getType();
};

class xnet {
	public:
		xnet();
		~xnet ();
		void server( int p);
		void client( char * ip, int p );
		int update();
		void send( int t, char * m );
		void sendTo( int t, char * m, int uid );
		xpacket * get();
		int getPing();
        int getPacketsSent();
        int getPacketsReceived();
        void setVerbose( bool b );
};

class xbody {
	public:
		xbody();
		~xbody();

		void setID(int i);
		int getID();

		void addForce( x3dpoint f );
		void addRelForce( x3dpoint f );
		void addRelForceAtRelPos( x3dpoint f, x3dpoint p );
		void setLinearVel( x3dpoint v );
		void setAngularVel( x3dpoint v );
		x3dpoint getLinearVel();
		x3dpoint getAngularVel();

		x3dpoint getPosition();
		void setPosition( x3dpoint );
		x3dpoint getRotation();
		int getEnabled();
		void setEnabled(int i);

		void setMU( float f );
		void setBounce( float f );
		void setSoftERP( float f );
		void setSoftCFM( float f );

		float getMU();
		float getBounce();
		float getSoftCFM();
		float getSoftERP();

		void setStatic(int i);
		int getStatic();

		void alignToAxis();
		void damp( float f );

        void setCategoryBits( float bits );
        void setCollideBits( float bits );

};

class xcollision {
	public:
		xcollision(){};
		~xcollision(){};

		void seto1( xbody * o );
		void seto2( xbody * o );
        void setContactPoint( x3dpoint p );

		xbody * geto1();
		xbody * geto2();
		x3dpoint getContactPoint();
};

enum XDYNAMICS_TYPE {
	XDYNAMICS_STEP,
	XDYNAMICS_FAST,
	XDYNAMICS_QUICK
};

class xdynamics {
	public:
		xdynamics();
		~xdynamics();

		void update();
		xcollision * getCollision();
		xcollision * getCollisionFromLine( x3dpoint pos, x3dpoint rot, float len );
        void removeCollision();
        int getCollisionCount();

		void createBody( xbody *b );
		void destroyBody( xbody *b );

		void createBoxGeom( xbody *b, x3dpoint size, x3dpoint pos, x3dpoint rot );
		void createSphereGeom( xbody *b, float radius, x3dpoint pos, x3dpoint rot );
		void createTrimeshGeom( xbody *b, xmesh * m, x3dpoint pos, x3dpoint rot, bool mobile );
		void createTrimeshGeomFromTerrain( xbody *b, xterrainmesh * m, x3dpoint pos, x3dpoint rot, bool mobile );
		void destroyGeom( xbody *b );

		void setPlane2d (xbody * o1);
		void destroyJoints( xbody *b );

		void setGravity( x3dpoint g );
		void setERP( float erp );
		void setCFM( float cfm );
		void setContactSurface( float f );
		void setCorrectingVel( float f );
		void setAutoDisable( bool b );
		void setTimeStep( float f );
		void setMode( XDYNAMICS_TYPE m );
};

class xaudio{
	public:
		xaudio ();
		~xaudio ();

		void update ();
		void clear ();
		unsigned int addWAVSample ( char *path, bool loop, float gain = 1.0 );
		unsigned int addOGGSample ( char *fileName, float gain = 1.0 );
		void playSource ( unsigned int src );
		void setSourcePosition ( unsigned int src, float x, float y, float z );
		void setListenerPosition ( float x, float y, float z );

		std::string getLastErr ();
		bool isValid ();
		bool isPlaying ( unsigned int src );
		void stopSource ( unsigned int src );
		void pauseSource ( unsigned int src );
		void rewindSource ( unsigned int src );
		void setSourceGain ( unsigned int src, float gain );
		bool initOpenAL ();
		void closeDriver();
		void removeSource ( unsigned int src );

		int getFreeSource ();
		unsigned int getSource ();

		unsigned int loadALBuffer ( char *path );
		unsigned int getLoadedALBuffer (char *path );
		std::string getALErrorString ( ALenum err );
		std::string getALCErrorString ( ALenum err );

};

enum XDRIVER_TYPE {
	XDT_NULL,
	XDT_SOFTWARE,
	XDT_BURNINGSVIDEO,
	XDT_DIRECT3D8,
	XDT_DIRECT3D9,
	XDT_OPENGL
};

class xsimulation {
	public:
		xsimulation( XDRIVER_TYPE dtype = XDT_SOFTWARE, int width = 512, int height = 384,
			int depth = 16, bool fs = false, bool sbuff = false, bool vsynch = false  );
		~xsimulation();
		int update( void );

		int prerender ();
		int render ();
		int postrender ();
		void setColour ( xcolour c );

		bool event( irr::SEvent event );
		void destroy( void );

		void bindkey ( const char * a, XKEY_EVENT_TYPE e );
		bool isKey( const char * a );
		bool isKeyDown( const char * a );
		bool isKeyPressed( const char * a );
		bool isKeyUp( const char * a );

        void setKeyDelay( int d );
        void ignoreKeyRepeat( bool r );

		void bindGUI ( const char * a, int i, XGUI_EVENT_TYPE e );
		bool isGUI ( const char * a );

		void setWindowCaption( char * text );
		int getFPS();
		int getTris();
		void addArchive( char * text );

		int getEventCount();
		xevent * getEvent();
        void removeEvent();
		void printTree();

		xtext * addText(int id, char * c, int x1, int x2, int y1, int y2 );
		xcheckbox * addCheckbox(int id, bool b, char * c, int x1, int x2, int y1, int y2 );
		xbutton * addButton( int id, x2drect r, char * c );
		xwindow * addWindow( int id, x2drect r, char * c );
		xlight * addLight(  int id, x3dpoint pos, xcolour c, float radius );
		xcamera * addCamera(int id, XCAMERA_TYPE camtype, int x1, int y1, int z1, int x2, int y2, int z2 );
		xmesh * addMesh(int id, char * mesh_file);
		xcubemesh * addCubeMesh(int id);
		xspheremesh * addSphereMesh(int id);
		xterrainmesh * addTerrainMesh( int id, char * mesh_file );
		xparticlesystem * addParticleSystem(int id, char * tex_file);
		xskybox * addSkybox(int id, char * filename1,
					char * filename2,
					char * filename3,
					char * filename4,
					char * filename5,
					char * filename6);
		xgrid * addGrid( int id, int sz, int num );
		xtextnode * addTextNode( int id, xfont * f, char * txt, xcolour c );
		xscrollbar * addScrollbar( int id, bool o, x2drect r );
		xlistbox * addListbox( int id, bool b, x2drect r );
		xcombobox * addCombobox( int id, x2drect r );
		xcontextmenu * addContextMenu( int id, x2drect r );
		xcontextmenu * addMenu( int id );
		xtable * addTable( int id, bool b, x2drect r );
		xeditbox * addEditbox( int id, x2drect r );
		xtextbox * addTextbox( int id, x2drect r );
        xchatbox * addChatbox( int id, x2drect r );
		x3dline * add3dLine( int id, x3dpoint s, x3dpoint e );
		x3dcircle * add3dCircle( int id, x3dpoint c, float r );

		xbeam * addBeam( int id, char * tx_fn1, char * tx_fn2  );
		xbillboard * addBillboard( int id, x2dpoint p );
		xsprite * addSprite( int id, char * tx_fn1, char * tx_fn2, char * tx_fn3, char * tx_fn4  );

		ximage * addImage( int id, xtexture * t, x2dpoint p, x2drect r, xcolour c );

		xshader * addShader();
		int getNodeIDFromScreenCoordinates( x2dpoint p );
		x3dpoint getIntersection( x2dpoint p,  int nid );
        x2dpoint getScreenCoordinatesFrom3DPoint( x3dpoint p );
        x3dline * get3dlineFromScreenCoordinates( x2dpoint p );

		void setSkinColor ( XGUI_DEFAULT_COLOR which, xcolour c );
		xcolour * getSkinColor ( XGUI_DEFAULT_COLOR which );
		xtexture * getTexture( char * file );
		xfont * getFont(  char * file );
		void setFont( xfont  *f, int which );
		xfont * getBuiltInFont();
		void makeColorKeyTexture(xtexture * t, x2dpoint);
		void draw2DImage(xtexture * t, x2dpoint p, x2drect r, xcolour c);

		void setTimestep( float f );
		void setAmbientLight( xcolour c );
		void setShadowColour( xcolour c );
		void setFog ( xcolour colour,  bool linear, float start, float end, float density, bool pixel, bool range );

		int renderWithBloom();
		int renderWithBlur();
		int initBloom( float dist, float str, float mult);
		int initBlur( float str );

		void initShadows();
		void addShadowLight( int id, x3dpoint pos, x3dpoint tgt, xcolour c, float nv, float fv, float radius, bool directional );
		void setShadowLightPosition(int i, x3dpoint pos );
		void setShadowLightTarget(int i, x3dpoint pos );
		void addShadowToElement( xelement * e );
		void removeShadowFromElement( xelement * e );
		int renderWithShadows();
		void addPostProcessingEffect( char * filename );
};

enum XGUI_DEFAULT_COLOR{
	XGDC_3D_DARK_SHADOW,
	XGDC_3D_SHADOW,
	XGDC_3D_FACE,
	XGDC_3D_LIGHT,
	XGDC_ACTIVE_BORDER,
	XGDC_ACTIVE_CAPTION,
	XGDC_APP_WORKSPACE,
	XGDC_BUTTON_TEXT,
	XGDC_GRAY_TEXT,
	XGDC_HIGH_LIGHT,
	XGDC_HIGH_LIGHT_TEXT,
	XGDC_INACTIVE_BORDER,
	XGDC_INACTIVE_CAPTION,
	XGDC_TOOLTIP,
	XGDC_TOOLTIP_BACKGROUND,
	XGDC_SCROLLBAR,
	XGDC_WINDOW,
	XGDC_WINDOW_SYMBOL,
	XGDC_ICON,
	XGDC_ICON_HIGH_LIGHT,
	XGDC_COUNT
};

class xcolour{
	public:
		xcolour(){};
		xcolour (float a, float r, float g, float b);
		~xcolour (){};
		float A;
		float R;
		float G;
		float B;
};

enum XMATERIAL_FLAG{
	XMF_WIREFRAME = 0x1,
	XMF_POINTCLOUD = 0x2,
	XMF_GOURAUD_SHADING = 0x4,
	XMF_LIGHTING = 0x8,
	XMF_ZBUFFER = 0x10,
	XMF_ZWRITE_ENABLE = 0x20,
	XMF_BACK_FACE_CULLING = 0x40,
	XMF_FRONT_FACE_CULLING = 0x80,
	XMF_BILINEAR_FILTER = 0x100,
	XMF_TRILINEAR_FILTER = 0x200,
	XMF_ANISOTROPIC_FILTER = 0x400,
	XMF_FOG_ENABLE = 0x800,
	XMF_NORMALIZE_NORMALS = 0x1000,
	XMF_TEXTURE_WRAP = 0x2000,
	XMF_ANTI_ALIASING = 0x4000,
	XMF_COLOR_MASK = 0x8000
};

enum XMATERIAL_TYPE{
	XMT_SOLID,
	XMT_SOLID_2_LAYER,
	XMT_LIGHTMAP,
	XMT_LIGHTMAP_ADD,
	XMT_LIGHTMAP_M2,
	XMT_LIGHTMAP_M4,
	XMT_LIGHTMAP_LIGHTING,
	XMT_LIGHTMAP_LIGHTING_M2,
	XMT_LIGHTMAP_LIGHTING_M4,
	XMT_DETAIL_MAP,
	XMT_SPHERE_MAP,
	XMT_REFLECTION_2_LAYER,
	XMT_TRANSPARENT_ADD_COLOR,
	XMT_TRANSPARENT_ALPHA_CHANNEL,
	XMT_TRANSPARENT_ALPHA_CHANNEL_REF,
	XMT_TRANSPARENT_VERTEX_ALPHA,
	XMT_TRANSPARENT_REFLECTION_2_LAYER,
	XMT_NORMAL_MAP_SOLID,
	XMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR,
	XMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA,
	XMT_PARALLAX_MAP_SOLID,
	XMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR,
	XMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA,
	XMT_ONETEXTURE_BLEND,
	XMT_FORCE_32BIT
};

enum X_HARDWARE_MAPPING{
	XHM_NEVER=0,
	XHM_STATIC,
	XHM_DYNAMIC,
	XHM_STREAM
};

enum X_BUFFER_TYPE{
	XBT_NONE=0,
	XBT_VERTEX,
	XBT_INDEX,
	XBT_VERTEX_AND_INDEX
};

class x3dpoint{
	public:
		x3dpoint();
		x3dpoint(float x, float y, float z);
		~x3dpoint();
		void trace();
		void normalize();
		float X;
		float Y;
		float Z;

};

class x2dpoint{
	public:
		x2dpoint();
		x2dpoint (float x, float y);
		~x2dpoint ();
		float X;
		float Y;
};

class x2drect{
	public:
		x2drect();
		x2drect (float x1, float y1, float x2, float y2);
		x2drect (x2dpoint p1, x2dpoint p2);
		~x2drect (){};
		float X1;
		float Y1;
		float X2;
		float Y2;
};

class xmatrix4{
	public:
		xmatrix4(){};
		~xmatrix4 (){};
		void setRotationDegrees( x3dpoint *p );
		void transformVect( x3dpoint *p );
		void buildTextureTransform( float r, x2dpoint * c, x2dpoint * t, x2dpoint * s);
};

enum XELEMENT_TYPE{
	XELEMENT_TYPE_NULL,
	XELEMENT_TYPE_AREA,
	XELEMENT_TYPE_DIALOG,
	XELEMENT_TYPE_SELECT,
	XELEMENT_TYPE_OPTION,
	XELEMENT_TYPE_INPUT,
	XELEMENT_TYPE_TEXT,
	XELEMENT_TYPE_IMAGE,
	XELEMENT_TYPE_BUTTON,
	XELEMENT_TYPE_WINDOW,
	XELEMENT_TYPE_SCROLLBAR,
	XELEMENT_TYPE_LISTBOX,
	XELEMENT_TYPE_TEXTBOX,
	XELEMENT_TYPE_EDITBOX,
	XELEMENT_TYPE_CONTEXTMENU,
	XELEMENT_TYPE_ENVIRONMENT,
	XELEMENT_TYPE_CAMERA,
	XELEMENT_TYPE_LIGHT,
	XELEMENT_TYPE_FIELD,
	XELEMENT_TYPE_MESH,
	XELEMENT_TYPE_ANIMATEDMESH,
	XELEMENT_TYPE_TERRAIN,
	XELEMENT_TYPE_PARTICLESYSTEM,
	XELEMENT_TYPE_SKYBOX,
	XELEMENT_TYPE_SCRIPT,
	XELEMENT_TYPE_BODY,
	XELEMENT_TYPE_TABLE,
	XELEMENT_TYPE_COMBOBOX,
	XELEMENT_TYPE_GRID,
	XELEMENT_TYPE_TEXTNODE,
	XELEMENT_TYPE_BEAM,
	XELEMENT_TYPE_BONE,
	XELEMENT_TYPE_CHECKBOX,
	XELEMENT_TYPE_TAB,
	XELEMENT_TYPE_LINE,
	XELEMENT_TYPE_CIRCLE,
	XELEMENT_TYPE_CHATBOX
};

class xelement {
	public:
		xelement( irr::IrrlichtDevice *dev, int id );
		~xelement();

		void setPosition( x3dpoint p);
		x3dpoint getPosition();

		void setLocation( x2dpoint p);
		x2dpoint getLocation();

		void setRotation( x3dpoint p);
		x3dpoint getRotation();

		void setAttr( char * name, char * value );
		char * getAttr( char * name );

		void setParent( xelement * x );
		xelement * getParent();
		XELEMENT_TYPE getType();
		int getID();

};

enum XEVENT_TYPE{
	XGUI_EVENT,
	XMOUSE_EVENT,
	XKEY_EVENT,
	XJOYSTICK_EVENT,
	XLOG_EVENT
};

enum XKEY_EVENT_TYPE{
	XKEY_LBUTTON          = 0x01,
	XKEY_RBUTTON          = 0x02,
	XKEY_CANCEL           = 0x03,
	XKEY_MBUTTON          = 0x04,
	XKEY_XBUTTON1         = 0x05,
	XKEY_XBUTTON2         = 0x06,
	XKEY_BACK             = 0x08,
	XKEY_TAB              = 0x09,
	XKEY_CLEAR            = 0x0C,
	XKEY_RETURN           = 0x0D,
	XKEY_SHIFT            = 0x10,
	XKEY_CONTROL          = 0x11,
	XKEY_MENU             = 0x12,
	XKEY_PAUSE            = 0x13,
	XKEY_CAPITAL          = 0x14,
	XKEY_KANA             = 0x15,
	XKEY_HANGUEL          = 0x15,
	XKEY_HANGUL           = 0x15,
	XKEY_JUNJA            = 0x17,
	XKEY_FINAL            = 0x18,
	XKEY_HANJA            = 0x19,
	XKEY_KANJI            = 0x19,
	XKEY_ESCAPE           = 0x1B,
	XKEY_CONVERT          = 0x1C,
	XKEY_NONCONVERT       = 0x1D,
	XKEY_ACCEPT           = 0x1E,
	XKEY_MODECHANGE       = 0x1F,
	XKEY_SPACE            = 0x20,
	XKEY_PRIOR            = 0x21,
	XKEY_NEXT             = 0x22,
	XKEY_END              = 0x23,
	XKEY_HOME             = 0x24,
	XKEY_LEFT             = 0x25,
	XKEY_UP               = 0x26,
	XKEY_RIGHT            = 0x27,
	XKEY_DOWN             = 0x28,
	XKEY_SELECT           = 0x29,
	XKEY_PRINT            = 0x2A,
	XKEY_EXECUT           = 0x2B,
	XKEY_SNAPSHOT         = 0x2C,
	XKEY_INSERT           = 0x2D,
	XKEY_DELETE           = 0x2E,
	XKEY_HELP             = 0x2F,
	XKEY_0            = 0x30,
	XKEY_1            = 0x31,
	XKEY_2            = 0x32,
	XKEY_3            = 0x33,
	XKEY_4            = 0x34,
	XKEY_5            = 0x35,
	XKEY_6            = 0x36,
	XKEY_7            = 0x37,
	XKEY_8            = 0x38,
	XKEY_9            = 0x39,
	XKEY_A            = 0x41,
	XKEY_B            = 0x42,
	XKEY_C            = 0x43,
	XKEY_D            = 0x44,
	XKEY_E            = 0x45,
	XKEY_F            = 0x46,
	XKEY_G            = 0x47,
	XKEY_H            = 0x48,
	XKEY_I            = 0x49,
	XKEY_J            = 0x4A,
	XKEY_K            = 0x4B,
	XKEY_L            = 0x4C,
	XKEY_M            = 0x4D,
	XKEY_N            = 0x4E,
	XKEY_O            = 0x4F,
	XKEY_P            = 0x50,
	XKEY_Q            = 0x51,
	XKEY_R            = 0x52,
	XKEY_S            = 0x53,
	XKEY_T            = 0x54,
	XKEY_U            = 0x55,
	XKEY_V            = 0x56,
	XKEY_W            = 0x57,
	XKEY_X            = 0x58,
	XKEY_Y            = 0x59,
	XKEY_Z            = 0x5A,
	XKEY_LWIN             = 0x5B,
	XKEY_RWIN             = 0x5C,
	XKEY_APPS             = 0x5D,
	XKEY_SLEEP            = 0x5F,
	XKEY_NUMPAD0          = 0x60,
	XKEY_NUMPAD1          = 0x61,
	XKEY_NUMPAD2          = 0x62,
	XKEY_NUMPAD3          = 0x63,
	XKEY_NUMPAD4          = 0x64,
	XKEY_NUMPAD5          = 0x65,
	XKEY_NUMPAD6          = 0x66,
	XKEY_NUMPAD7          = 0x67,
	XKEY_NUMPAD8          = 0x68,
	XKEY_NUMPAD9          = 0x69,
	XKEY_MULTIPLY         = 0x6A,
	XKEY_ADD              = 0x6B,
	XKEY_SEPARATOR        = 0x6C,
	XKEY_SUBTRACT         = 0x6D,
	XKEY_DECIMAL          = 0x6E,
	XKEY_DIVIDE           = 0x6F,
	XKEY_F1               = 0x70,
	XKEY_F2               = 0x71,
	XKEY_F3               = 0x72,
	XKEY_F4               = 0x73,
	XKEY_F5               = 0x74,
	XKEY_F6               = 0x75,
	XKEY_F7               = 0x76,
	XKEY_F8               = 0x77,
	XKEY_F9               = 0x78,
	XKEY_F10              = 0x79,
	XKEY_F11              = 0x7A,
	XKEY_F12              = 0x7B,
	XKEY_F13              = 0x7C,
	XKEY_F14              = 0x7D,
	XKEY_F15              = 0x7E,
	XKEY_F16              = 0x7F,
	XKEY_F17              = 0x80,
	XKEY_F18              = 0x81,
	XKEY_F19              = 0x82,
	XKEY_F20              = 0x83,
	XKEY_F21              = 0x84,
	XKEY_F22              = 0x85,
	XKEY_F23              = 0x86,
	XKEY_F24              = 0x87,
	XKEY_NUMLOCK          = 0x90,
	XKEY_SCROLL           = 0x91,
	XKEY_LSHIFT           = 0xA0,
	XKEY_RSHIFT           = 0xA1,
	XKEY_LCONTROL         = 0xA2,
	XKEY_RCONTROL         = 0xA3,
	XKEY_LMENU            = 0xA4,
	XKEY_RMENU            = 0xA5,
	XKEY_PLUS             = 0xBB,
	XKEY_COMMA            = 0xBC,
	XKEY_MINUS            = 0xBD,
	XKEY_PERIOD           = 0xBE,
	XKEY_ATTN             = 0xF6,
	XKEY_CRSEL            = 0xF7,
	XKEY_EXSEL            = 0xF8,
	XKEY_EREOF            = 0xF9,
	XKEY_PLAY             = 0xFA,
	XKEY_ZOOM             = 0xFB,
	XKEY_PA1              = 0xFD,
	XKEY_OEM_CLEAR        = 0xFE,
	XKEY_XKEY_CODES_COUNT  = 0xFF
};

enum XGUI_EVENT_TYPE{
    XGUI_ELEMENT_FOCUS_LOST = 0,
    XGUI_ELEMENT_FOCUSED,
    XGUI_ELEMENT_HOVERED,
    XGUI_ELEMENT_LEFT,
    XGUI_ELEMENT_CLOSED,
    XGUI_BUTTON_CLICKED,
    XGUI_SCROLL_BAR_CHANGED,
    XGUI_CHECKBOX_CHANGED,
    XGUI_LISTBOX_CHANGED,
    XGUI_LISTBOX_SELECTED_AGAIN,
    XGUI_FILE_SELECTED,
    XGUI_DIRECTORY_SELECTED,
    XGUI_FILE_CHOOSE_DIALOG_CANCELLED,
    XGUI_MESSAGEBOX_YES,
    XGUI_MESSAGEBOX_NO,
    XGUI_MESSAGEBOX_OK,
    XGUI_MESSAGEBOX_CANCEL,
    XGUI_EDITBOX_ENTER,
    XGUI_EDITBOX_CHANGED,
    XGUI_EDITBOX_MARKING_CHANGED,
    XGUI_TAB_CHANGED,
    XGUI_MENU_ITEM_SELECTED,
    XGUI_COMBO_BOX_CHANGED,
    XGUI_SPINBOX_CHANGED,
    XGUI_TABLE_CHANGED,
    XGUI_TABLE_HEADER_CHANGED,
    XGUI_TABLE_SELECTED_AGAIN,
    XGUI_TREEVIEW_NODE_DESELECT,
    XGUI_TREEVIEW_NODE_SELECT,
    XGUI_TREEVIEW_NODE_EXPAND,
    XGUI_TREEVIEW_NODE_COLLAPS,
    XGUI_COUNT
};

enum XLOG_EVENT_TYPE{
	XLOG_INFORMATION,
	XLOG_WARNING,
	XLOG_ERROR,
	XLOG_NONE
};

enum XMOUSE_EVENT_TYPE{
	XMOUSE_L_PRESSED_DOWN,
	XMOUSE_R_PRESSED_DOWN,
	XMOUSE_M_PRESSED_DOWN,
	XMOUSE_L_LEFT_UP,
	XMOUSE_R_LEFT_UP,
	XMOUSE_M_LEFT_UP,
	XMOUSE_MOVED,
	XMOUSE_WHEEL
};

class xevent {
	public:
		xevent();
		~xevent();
		XEVENT_TYPE getEventType();
		XKEY_EVENT_TYPE getKeyEventType();
		bool isKeyPressedDown();
		bool isKeyShift();
		bool isKeyControl();
		XGUI_EVENT_TYPE getGUIEventType();
		xelement* getCaller();
		XLOG_EVENT_TYPE getLogEventType();
		XMOUSE_EVENT_TYPE getMouseEventType();
		float getMouseX();
		float getMouseY();
		float getMouseWheel();

		float getAxis( int i );
		int   ButtonStates;
		int   Joystick;
};

class xtext : public xelement {
	public:
		xtext( irr::IrrlichtDevice *dev, int id, char * c, int x1, int x2, int y1, int y2 );
		~xtext();
		int update( void );
		int render( void );
		bool event( void );
		void destroy( void );
		void setVisible( bool v );
		void setParent( xelement * x );
		char * getText();
		void setText( char * c );
		void setColour( xcolour c );
		void setFont( xfont * f );
		void setAlignment( int h, int v );
        void setPosition( x2dpoint p );
};

enum XCAMERA_TYPE {
	CAMERA_STATIC,
	CAMERA_FPS,
	CAMERA_RTS,
	CAMERA_RPG,
	CAMERA_COCKPIT
};

class xlight : public xelement {
	public:
		xlight( irr::IrrlichtDevice *dev, int id, x3dpoint pos, xcolour c, float radius );
		~xlight();
		int update();
		int render();
		bool event();
		void destroy();
		void setPosition( x3dpoint p );
		x3dpoint getPosition();
};

class xcamera : public xelement {
	public:
		xcamera( irr::IrrlichtDevice *dev, int id, XCAMERA_TYPE camtype,
			int x1, int y1, int z1, int x2, int y2, int z2 );
		~xcamera();
		int update( void );
		int render( void );
		bool event( void );
		void destroy( void );
		void setParent( xmesh *x );
		void setPosition ( x3dpoint p );
		x3dpoint getPosition();
		void setTarget ( x3dpoint p );
		void setFOV( float fov );
		void setAspectRatio( float a );
		void setFarValue( float v);
		void setOrthoMode( float a, float b, float c, float d);
};

class xmesh : public xelement {
	public:
		xmesh( irr::IrrlichtDevice *dev, int id, char * mesh_fn );
		~xmesh();
		int update( void );
		int render( void );
		bool event( void );
		void destroy( void );

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint p );
		void setScale( x3dpoint p );
		void setMaterialFlag( XMATERIAL_FLAG mf, bool val );
		void setMaterialType( XMATERIAL_TYPE mt );
		void addNormalMap( char * bump, float factor );
		void addTexture( char * fn, int index );
		void setMD2Animation( int i );

		int getStartFrame();
		int getEndFrame ();
		void setAnimationSpeed( int fps);
		void setCurrentFrame( int frame );
		void setFrameLoop( int begin, int end );
		void setLoopMode( bool loop );

		void addShadowVolume( bool zfail );
		void setParent( xelement * x );
		xbone * getBone (  char * jointName );

		x3dpoint getPosition();
		x3dpoint getRotation();
		x3dpoint getScale();

		void setDebug( bool b);
		void setHardwareMapping( X_HARDWARE_MAPPING hint, X_BUFFER_TYPE buffer );
};

class xcubemesh : public xelement {
	public:
		xcubemesh( irr::IrrlichtDevice *dev, int id );
		~xcubemesh();
		int update( void );
		int render( void );
		bool event( void );
		void destroy( void );

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint p );
		void setScale( x3dpoint p );
		void setVisible( bool val );
		void setMaterialFlag( XMATERIAL_FLAG mf, bool val );
		void setMaterialType( XMATERIAL_TYPE mt );
		void addNormalMap( char * bump, float factor );
		void addTexture( char * fn, int index );
		void createTriangleSelector();

		x3dpoint getPosition();
		x3dpoint getRotation();
		x3dpoint getScale();
};

class xspheremesh : public xelement {
	public:
		xspheremesh( irr::IrrlichtDevice *dev, int id );
		~xspheremesh();
		int update( void );
		int render( void );
		bool event( void );
		void destroy( void );

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint p );
		void setScale( x3dpoint p );
		void setMaterialFlag( XMATERIAL_FLAG mf, bool val );
		void setMaterialType( XMATERIAL_TYPE mt );
		void addNormalMap( char * bump, float factor );
		void addTexture( char * fn, int index );

		x3dpoint getPosition();
		x3dpoint getRotation();
		x3dpoint getScale();
};

class xterrainmesh : public xelement {
	public:
		xterrainmesh( irr::IrrlichtDevice *dev, int id, char * mesh_fn );
		~xterrainmesh();
		int update( void );
		int render( void );
		bool event( void );
		void destroy( void );

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint p );
		void setScale( x3dpoint p );
		void setMaterialFlag( XMATERIAL_FLAG mf, bool val );
		void setMaterialType( XMATERIAL_TYPE mt );
		void addNormalMap( char * bump, float factor );
		void addTexture( char * fn, int index );
		void scaleTexture( float a, float b );

		x3dpoint getPosition();
		x3dpoint getRotation();
		x3dpoint getScale();
};

class xparticlesystem : public xelement {
	public:
		xparticlesystem( irr::IrrlichtDevice *dev, int id, char * tex_fn );
		~xparticlesystem();
		int update( void );
		int render( void );
		bool event( void );
		void destroy( void );

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint p );
		void setScale( x3dpoint p );
		void setMaterialFlag( XMATERIAL_TYPE mt, bool val );
		void setMaterialType( XMATERIAL_TYPE mt );

		x3dpoint getPosition();
		x3dpoint getRotation();
		x3dpoint getScale();

		void addBoxEmitter( x3dpoint direction, int minpps, int maxpps, xcolour mincolour, xcolour maxcolour,
			int minlife, int maxlife, float angle, float minsize, float maxsize );
		void addRingEmitter(x3dpoint center, float radius, float thickness, x3dpoint direction, int minpps,
			int maxpps, xcolour mincolour, xcolour maxcolour, int minlife, int maxlife, float angle, float minsize, float maxsize);
		void addSphereEmitter(x3dpoint center, float radius, x3dpoint direction, int minpps, int maxpps,
			xcolour mincolour, xcolour maxcolour, int minlife, int maxlife, float angle, float minsize, float maxsize);
		void createPointEmitter( x3dpoint direction, int minpps, int maxpps, xcolour mincolour, xcolour maxcolour,
			int minlife, int maxlife, float angle, float minsize, float maxsize );

		void addFadeOutAffector(xcolour colour, float time);
		void addRotationAffector( x3dpoint speed, x3dpoint pivot );
		void addAttractionAffector( x3dpoint point, float speed, bool attract );
		void addGravityAffector( x3dpoint gravity, int force );
        void addScaleAffector( x2dpoint scaleTo );

		void setMinParticlesPerSecond( int n );
		void setMaxParticlesPerSecond( int n );
};

class xskybox : public xelement {
	public:
		xskybox( irr::IrrlichtDevice *dev, int id,
			char * filename1,
			char * filename2,
			char * filename3,
			char * filename4,
			char * filename5,
			char * filename6 );
		~xskybox();
		void setRotation( x3dpoint p);
		void destroy( void );
};

class xgrid : public xelement {
	public:
		xgrid( irr::IrrlichtDevice *dev, int id, int sz, int num );
		~xgrid();
		int update( void );
		int render( void );
		bool event( void );
		void destroy( void );

		void addForce( x2dpoint p, float str, float radius, float time );
        void setColour( xcolour c );
        void setPosition( x3dpoint p );
};

class xtextnode : public xelement {
	public:
		xtextnode( irr::IrrlichtDevice *dev, int id, xfont * f, char* txt, xcolour c );
		~xtextnode();
		void destroy();

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint p );
		void setScale( x3dpoint p );
        void setColour( xcolour c  );

		void setText( char * c );

		x3dpoint getPosition();
		x3dpoint getRotation();
		x3dpoint getScale();
};

class xbeam : public xelement {
	public:
		xbeam( irr::IrrlichtDevice *dev, int id, char * tx_fn1, char * tx_fn2 );
		~xbeam();
		int update( void );
		int render( void );
		bool event( void );
		void destroy( void );

		void setMaterialFlag( XMATERIAL_FLAG mf, bool val );
		void setMaterialType( XMATERIAL_TYPE mt );
		void addNormalMap( char * bump, float factor );
		void addTexture( char * fn, int index );

		void setLine(x3dpoint start, x3dpoint end, float scale );
        void setColour(xcolour c );
};

class xbillboard : public xelement {
	public:
		xbillboard( irr::IrrlichtDevice *dev, int id, x2dpoint p );
		~xbillboard();
		int update( void );
		int render( void );
		bool event( void );
		void destroy( void );

		void setMaterialFlag( XMATERIAL_FLAG mf, bool val );
		void setMaterialType( XMATERIAL_TYPE mt );
		void addTexture( char * fn, int index );
        void setTextureMatrix( xmatrix4 m );

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint p );
		void setScale( x3dpoint p );
        void setVisible( bool val );

		x3dpoint getPosition();
		x3dpoint getRotation();
		x3dpoint getScale();
};

class xsprite : public xelement {
	public:
        xsprite( irr::IrrlichtDevice *dev, int id, char * tx_fn1, char * tx_fn2, char * tx_fn3, char * tx_fn4);
        ~xsprite();
        int update();
        int render();
        bool event();
        void destroy();
        void setPosition( x3dpoint p );
        void setSize( x2dpoint p );
        void setVisible( bool val );

        void setMaterialFlag( XMATERIAL_FLAG mf, bool val );
        void setMaterialType( XMATERIAL_TYPE mt );
        void setTextureMatrix( xmatrix4 m );
        void setTexture( char * tex, int layer );

        x3dpoint getPosition();
        x2dpoint getSize();
};

class xscrollbar : public xelement {
	public:
		xscrollbar( irr::IrrlichtDevice *dev, int id, bool o, x2drect r );
		~xscrollbar();
		void destroy();
        void setVisible( bool v );
		void setMax(int n);
		float getMax();
		void setMin(int n);
		float getMin();
		void setPos(int n);
		float getPos();
		void setSmallStep(int n);
		float getSmallStep();
		void setLargeStep(int n);
		float getLargeStep();

		void setParent( xelement * x );
};

class xlistbox : public xelement {
	public:
		xlistbox( irr::IrrlichtDevice *dev, int id, bool b, x2drect r );
		~xlistbox();
		void addItem( char * c );
		void destroy();
		void setVisible( bool v );
		void clear();
		char * getItem( int i );
		int getSelected();
		void setSelected( int n );
		void setParent( xelement * x );
		int getItemCount();
		void setItemColor( int i, int n, xcolour c );
		int setAutoScroll( bool b );
        int removeItem( int n );
        void setPosition( x2dpoint p );
};

class xcombobox : public xelement {
	public:
		xcombobox( irr::IrrlichtDevice *dev, int id, x2drect r );
		~xcombobox();
		void addItem( char * c );
		void destroy();
		void setVisible( bool v );
		void clear();
		char * getItem( int i );
		int getSelected();
		void setSelected( int n );
		void setParent( xelement * x );
};

class xcontextmenu : public xelement {
	public:
		xcontextmenu( irr::IrrlichtDevice *dev, int id, x2drect r );
		xcontextmenu( irr::IrrlichtDevice *dev, int id );
		~xcontextmenu();
		void addItem( int id, char * c, bool sub );
		void destroy();
		void setVisible( bool v );
		char * getItem( int i );
		int getSelected();
		void setParent( xelement * x );
		xcontextmenu * getSubMenu( int id, int nid  );
		void addSeparator();
};

class xtable : public xelement {
	public:
		xtable( irr::IrrlichtDevice *dev, int id, bool b, x2drect r );
		~xtable();
		void destroy();
		void setVisible( bool v );
		void clear();
		void addColumn( char * caption, int index );
		void addRow( int index );
		char * getCellText ( int i, int n  );
		void setCellText ( int row, int col, char * t );
		int getSelected();
		void setSelected( int n );
		void setParent( xelement * x );
};

class xeditbox : public xelement {
	public:
		xeditbox( irr::IrrlichtDevice *dev, int id, x2drect r );
		~xeditbox();
		void destroy();
		char * getText();
		void setText( char * c );
		void setVisible( bool v );
		void setParent( xelement * x );
		void setMultiLine( bool b );
        void setPasswordBox( bool b );
        void setTextAlignment( int h, int v );
        void setWordWrap( bool b );
};

class xtextbox : public xelement {
	public:
		xtextbox( irr::IrrlichtDevice *dev, int id, x2drect r );
		~xtextbox();
		void destroy();
		char * getText();
		void setText( char * c );
		void setVisible( bool v );
		void setParent( xelement * x );
};

class xchatbox : public xelement {
	public:
		xchatbox( irr::IrrlichtDevice *dev, int id, x2drect r );
		~xchatbox();
		void destroy();
		void setVisible( bool v );
		void setParent( xelement * x );
        void addMessage( char * txt, xcolour c );
        void setLife( int i );
        void setFont( xfont * f );
};

class xbutton : public xelement {
	public:
		xbutton( irr::IrrlichtDevice *dev, int id, x2drect r, char * c );
		~xbutton();
		void destroy();
		void setVisible( bool v );
		void setParent( xelement * x );
		void setImage( xtexture * t );
		void setPressedImage( xtexture * t );
		void setText( char * c );
		void setDrawBorder( bool b );
        void setPosition( x2dpoint p );
        void setAlignment( int n1, int n2, int n3, int n4 );
};

class xwindow : public xelement {
	public:
		xwindow( irr::IrrlichtDevice *dev, int id, x2drect r, char * c );
		~xwindow();
		void setVisible( bool v );
		void destroy();
		void setParent( xelement * x );
		void setSize( int w, int h );
		void setText( char * c );
		void setTitleBarVisible( bool b );
};

class x3dline : public xelement {
	public:
		x3dline( irr::IrrlichtDevice *dev, int id, x3dpoint s, x3dpoint e );
		~x3dline();
		void destroy();
		void setVisible( bool v );
		void setColour( xcolour c );
		void setStart( x3dpoint p );
		void setEnd( x3dpoint p );
		x3dpoint getStart();
		x3dpoint getEnd();

		x3dpoint getClosestPoint( x3dpoint p );
};

class x3dcircle : public xelement {
	public:
		x3dcircle( irr::IrrlichtDevice *dev, int id, x3dpoint c, float r );
		~x3dcircle();
		void destroy();
		void setVisible( bool v );
		void setColour( xcolour c );
		void setPosition( x3dpoint p );
		void setRotation( x3dpoint r );
        x3dpoint getPosition();
		x3dpoint getRotation();
};

class ximage : public xelement {
	public:
		ximage( irr::IrrlichtDevice *dev, int id, xtexture * t, x2dpoint p, x2drect r, xcolour c );
		~ximage();
		void destroy();
		void setColour( xcolour c );
		void setPosition( x2dpoint p );
		void setParent( xelement * x );
		void setVisible( bool v );
		void setRect( x2drect r );
		void setImage ( xtexture * t );
		void setSize( int w, int h );
};

class xtimer {
	public:
		xtimer();
		~xtimer();
		float getTime();
		float getFactor();
		void reset();
		void update();
};

class xbone : public xelement {
	public:
		xbone( irr::IrrlichtDevice *dev, int id, scene::IBoneSceneNode* b );
		~xbone();
		int update();
		int render();
		bool event();
		void destroy();
};

class xcheckbox : public xelement {
	public:
		xcheckbox( irr::IrrlichtDevice *dev, int id, bool b, char * c, int x1, int x2, int y1, int y2 );
		~xcheckbox();
		int update();
		int render();
		bool event();
		void destroy();
		void setVisible( bool v );
		void setParent( xelement * x );
		bool isChecked();
		void setChecked( bool b );
};

/* end of file */
