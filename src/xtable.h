// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xtable.h

#ifndef XTABLE_H
#define XTABLE_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xtable
///
/// This is a table element.
///

class xtable : public xelement {
	public:

		///
		/// constructor.
		///
		xtable( irr::IrrlichtDevice *dev, int id, bool b, x2drect r );

		///
		/// destructor.
		///
		~xtable();

		///
		/// update seq
		///
		int update();

		///
		/// render seq
		///
		int render();

		///
		/// event seq
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		///
		/// Add item to table
		///
		void addColumn( char * caption, int index );
		void addRow( int index );

		char * getCellText ( int i, int n );
		void setCellText ( int row, int col, char * t );

		void setVisible( bool v );
		void clear();
		int getSelected();
		void setSelected( int n );
		void setParent( xelement * x );

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;            // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		IGUITable* table;
};

#endif // XLISTBOX_H
