// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XEDITBOX_H
#define XEDITBOX_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xeditbox : static text
///
/// This is a static text element.
///

class xeditbox : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param txt string.
		/// \param x1 an integer argument.
		/// \param x2 an integer argument.
		/// \param y1 an integer argument.
		/// \param y21 an integer argument.
		///
		xeditbox( irr::IrrlichtDevice *dev, int id, x2drect r );

		///
		/// destructor.
		///
		~xeditbox();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		///
		/// get/set editable text
		///
		char * getText();
		void setText( char * c );

		void setVisible( bool v );
		void setParent( xelement * x );
		void setMultiLine( bool b );


        // void 	setOverrideColor (video::SColor color)=0
        // void 	setOverrideFont (IGUIFont *font=0)=0
        void setPasswordBox( bool b );
        void setTextAlignment( int h, int v );
        void setWordWrap( bool b );

	private:
		ostringstream stream;      // conversion variables
		core::stringw swdata;      // conversion variables
		const wchar_t* wsdata;     // conversion variables
		// std::string result;     // conversion variables
		IGUIEditBox* editbox;
};

#endif // XEDITBOX_H
