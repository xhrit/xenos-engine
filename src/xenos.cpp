// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xenos.cpp

float xcore_version = 1.0;

#include "xcore.h"

int main(int argc,char *argv[]){
	// launch core; pass args
	// cout << "Useage: xenjine [OPTION...] [MODULE]" << endl;
	char * module;
	if (argc == 1){
		cout << "[xenos " << xcore_version <<"] Loading default module " << endl;
		module = "default";
	} else {
		for (int i=1;i<argc;i++) {
			if (i == argc-1){
			    cout << "[xenos " << xcore_version <<"] Loading module " << argv[argc-1] << endl;
				// cout << "module: " << argv[argc-1] << endl;
				module = argv[argc-1];
			} else {
				cout << "option: " << argv[i] << endl;
			}

		}
	}

	xcore *core = new xcore();
	core->init(module); // pass module name

	try {
        while( core->update() ){ core->render(); }
    }

    catch ( ... ) {
        std::cout << "Exception Thrown" << std::endl;
    }

	return 0;
}
