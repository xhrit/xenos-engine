// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xgrid.cpp

#include "xgrid.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xgrid::xgrid( irr::IrrlichtDevice *dev, int id, int sz, int num ) : xelement( dev, id ){
	cout << "[xgrid] create xgrid" << endl;

	grid = new CGrid( num, num, dev->getSceneManager(), irr::core::dimension2d<irr::s32>(sz, sz) );

	setType( XELEMENT_TYPE_GRID );
	// node_reference = node;
}

xgrid::~xgrid(){
	cout << "[xgrid] delete xgrid" << endl;
}

int xgrid::update(){
	// cout << "[xgrid] xgrid::update" << endl;
	return 1;
}

int xgrid::render(){
	// cout << "[xgrid] xgrid::render" << endl;
	return 1;
} // xgrid::render

bool xgrid::event(){
	cout << "[xgrid] xgrid::event" << endl;
	return false;
} // xgrid::event

void xgrid::destroy(){
	cout << "[xgrid] xgrid::destroy" << endl;
	grid->remove();
	setType( XELEMENT_TYPE_NULL );
} // xgrid::destroy

void xgrid::addForce( x2dpoint p, float str, float radius, float time ){
	grid->addForce( irr::core::vector2df(p.X, p.Y), str, radius, time); // 1000.0f, 30.0f, 0.1f);
}

void xgrid::setColour( xcolour c ){
	grid->setColor( c.getSColour() );
}

void xgrid::setPosition( x3dpoint p ){
    position = p;
    // node->createCircle( position.getv3df(), radius, 40, core::vector3df(0,1,0) );
}
