// xengine Lukas Irwin [ sysop@xhrit.com ]
// copyright (c) 2007
// This program is distributed under the terms of the GNU General Public License

#include "xtabcontrol.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xtabcontrol::xtabcontrol( irr::IrrlichtDevice *dev, int id, x2drect r ) : xelement( dev, id ){
	cout << "[xtabcontrol] create xtabcontrol" << endl;

	// tabcontrol = new IGUITabControl( irr_env->getBuiltInFont() ,L"Text", irr_env,
	//	irr::core::rect<irr::s32>(r.X1, r.Y1, r.X2, r.Y2), irr_env->getRootGUIElement(), id );

	setType( XELEMENT_TYPE_TAB );
}

xtabcontrol::~xtabcontrol(){
	cout << "[xtabcontrol] delete xtabcontrol" << endl;
}

int xtabcontrol::update(){
	// cout << "[xtabcontrol] xtabcontrol::update" << endl;
	return 1;
}

int xtabcontrol::render(){
	// cout << "[xtabcontrol] xtabcontrol::render" << endl;
	return 1;
}

bool xtabcontrol::event(){
	// cout << "[xtabcontrol] xtabcontrol::event" << endl;
	return false;
}

void xtabcontrol::destroy(){
	// cout << "[xtabcontrol] xtabcontrol::destroy" << endl;
	// tabcontrol->remove();
}

xtab* xtabcontrol::addTab( char * c, int id ){
	cout << "[xtabcontrol] xtabcontrol::addTab" << endl;
	xtab * t = new xtab(irr_device, id, x2drect( 0, 0, 0, 0) );

	// xtab->set( tabcontrol->addTab (c, id) );

	return t;
}

int xtabcontrol::getActiveTab (){
	cout << "[xtabcontrol] xtabcontrol::getActiveTab" << endl;
}

void xtabcontrol::setActiveTab ( int id){

}

void xtabcontrol::setParent( xelement * x ){
	// parent = x;
	// tabcontrol->setParent( x->getGfxElement() );
} // xtabcontrol::setParent
