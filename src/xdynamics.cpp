// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xdynamics.h"

#include <iostream>

using namespace std;

// dWorldSetGravity( (*m_sim).World, gravity.X, gravity.Y, gravity.Z);

// collision/physics functions
xdynamics::xdynamics() {
	// cout << "[xdynamics] new xdynamics " << endl;
	dInitODE();

	world = dWorldCreate();
	space = dSimpleSpaceCreate(0); // do i need more then one space!?

	contactgroup = dJointGroupCreate(0);

	dWorldSetGravity( world, 0, 0, 0);
	dWorldSetERP( world, 0.2);
	dWorldSetCFM( world, 0.5);
	step = 0.01666;
	dmode = XDYNAMICS_STEP;
	body_count = 0;

} // xdynamics::InitDyanmics

xdynamics::~xdynamics(){
	// cout << "[xdynamics] end xdynamics " << endl;
	// Destroy all joints in our joint group
	dJointGroupDestroy( contactgroup );
	// Destroy the collision space.
	dSpaceDestroy( space );
	// Destroy the world and everything in it.
	dWorldDestroy( world );
} // xdynamics::EndDyanmics

void xdynamics::setContactSurface( float f ){
	dWorldSetContactSurfaceLayer( world, f);
} // xdynamics::setContactSurface

void xdynamics::setCorrectingVel( float f ){
	dWorldSetContactMaxCorrectingVel( world, f);
} // xdynamics::setCorrectingVel

void xdynamics::setAutoDisable( bool b ){
	dWorldSetAutoDisableFlag( world, b);
} // xdynamics::setAutoDisable

void xdynamics::setTimeStep( float f ){
	step = f;
} // xdynamics::setTimeStep

void xdynamics::setMode( XDYNAMICS_TYPE m ){
	dmode = m;
} // xdynamics::setMode

void xdynamics::update(){
	// cout << "[xdynamics] update " << endl;
	void (*collisionfunc)(void*, dGeomID, dGeomID) = & nearCollision;
	// float ts = timeStep/100;
	// cout << "[xdynamics] collisions " << endl;
	// build the collision joints for all objects in 'theSpace'
	dSpaceCollide( space, this, collisionfunc ); // crashes here?

    // cout << "[xdynamics] step dyanmics" << endl;
	// Now we advance the simulation by calling dWorldQuickStep.
	if ( dmode == XDYNAMICS_QUICK ){
	    // cout << "[xdynamics] dWorldQuickStep " << endl;
		dWorldQuickStep( world, step );

	} else if  ( dmode == XDYNAMICS_FAST ){
	    // cout << "[xdynamics] dWorldStepFast1 " << endl;
		dWorldStepFast1( world, step, 20);

	} else if ( dmode == XDYNAMICS_STEP ) {
	    // cout << "[xdynamics] dWorldStep " << endl;
		dWorldStep ( world, step );
	}
	// cout << "[xdynamics] clean up " << endl;
	// Remove all temporary collision joints now that the world has been stepped
	dJointGroupEmpty( contactgroup );
	// cout << "[xdynamics] /update" << endl;
} // xdynamics::UpdateDyanmics

//
void xdynamics::nearCollision (void *pData, dGeomID o1, dGeomID o2) {
	// pData is the "this" that was passed into dSpaceCollide
	xdynamics* pThis = (xdynamics*) pData;
	if (pThis)
	pThis->collision(o1, o2);
} // xdynamics::NearCollision

void xdynamics::collision (dGeomID o1, dGeomID o2) {
	// cout << "[xdynamics] collision" << endl;
	if (!(o1 && o2)) {
		cout << "[xdynamics] Collision with null geometry" << endl;
		return;
	}

	// ignore collisions between bodies that are connected by the same joint
	dBodyID Body1 = NULL, Body2 = NULL;
	if (o1) Body1 = dGeomGetBody (o1);
	if (o2) Body2 = dGeomGetBody (o2);

	if (Body1 && Body2 && dAreConnected (Body1, Body2)) {
		cout << "[xdynamics] Collision between bodies that are connected by the same joint" << endl;
		return;
	}

    /*if o1 or o2 is a ray, we exit nearCallback whithout doing anything,
    and specially without creating contact joints*/
    if ( dGeomGetClass(o1) == 5 || dGeomGetClass(o2) == 5 ) {
        return;
    }

	const int iContacts = MAX_CONTACTS;
	dContact contact[iContacts];

	// cout << "[xdynamics] Colliding" << endl;
	int iCount = dCollide ( o1, o2, iContacts, &contact[0].geom, sizeof (dContact) );

    // cout << "[xdynamics] find xbodies" << endl;
	// use avarage ov two bodies material properties?
	xbody * xbody1 = getxbodyByGeomID( o1 );
	xbody * xbody2 = getxbodyByGeomID( o2 );

    if (!( xbody1 && xbody2 )) {
		cout << "[xdynamics] Collision geometry with null xbody" << endl;
		return;
	}

    if ((dGeomGetCategoryBits (o1) & dGeomGetCollideBits (o2)) || (dGeomGetCategoryBits (o2) & dGeomGetCollideBits (o1))) {

    } else {
        // do nothing, o1 and o2 do not collide
		return;
    }

    // cout << "[xdynamics] gather properties from xbody" << endl;
	float mu = xbody1->getMU();
	float mu2 = xbody2->getMU();
	float bounce = xbody1->getBounce() + xbody2->getBounce() / 2;
	float softERP = xbody1->getSoftERP() + xbody2->getSoftERP() / 2;
	float softCFM = xbody1->getSoftCFM() + xbody2->getSoftCFM() / 2;

	// cout << "[xdynamics] build collision data" << endl;
	if (iCount) {
		int iMax = min ( iCount, iContacts );
		for ( int iContact = 0; iContact < iMax; iContact++ ) {
			contact[iContact].surface.mode = dContactMu2 | dContactBounce | dContactSoftERP | dContactSoftCFM;
			contact[iContact].surface.bounce = bounce;
			contact[iContact].surface.mu = mu;
			contact[iContact].surface.mu2 = mu2;
			contact[iContact].surface.soft_erp = softERP;
			contact[iContact].surface.soft_cfm = softCFM;

			dJointID ContactJoint = dJointCreateContact ( world, contactgroup, &contact[iContact] );
			dJointAttach ( ContactJoint, Body1, Body2 );

			// populate vector with collision event info
			xcollision * c = new xcollision();
			c->seto1(xbody1);
			c->seto2(xbody2);
			// TODO: set additional data (speed, heading, contact point, etc)
			collision_que.push_back( c );
		}
	}
} // xdynamics::collision

xcollision * xdynamics::getCollision() {
	xcollision * n;
	if ( !collision_que.empty() ) {
		n = collision_que[ 0 ];
		// collision_que.erase( collision_que.begin() );
	} else {
		return 0; // n = "null";
	}
	return n;
} // xdynamics::getCollision

void xdynamics::removeCollision() {
	// cout << "[xdynamics] removeCollision" << endl;
    xcollision *c = collision_que.front();
    // remove it from the list
    collision_que.erase( collision_que.begin() );
    // delete the pointer
    delete c;
} // xdynamics::removeCollision

void xdynamics::clearCollisionQue(){
	// cout << "[xsimulation] clearing event que " << endl;
    // delete all pointers in the vector
    while ( !collision_que.empty() ) {
        // get first 'element'
        xcollision *c = collision_que.front();
        // remove it from the list
        collision_que.erase( collision_que.begin() );
        // delete the pointer
        delete c;
    }
} // xdynamics::clearCollisionQue

int xdynamics::getCollisionCount() {
	return collision_que.size();
} // xdynamics::getCollisionCount

void xdynamics::createBody( xbody *b){
	// cout << "[xdynamics] createBody" << endl;
	b->setBody( dBodyCreate( world ) );
	b->setID( body_count );
	addReference( b );
	body_count++;
} // xdynamics::createBody

void xdynamics::destroyBody( xbody *b){
    // cout << "[xdynamics] destroyBody" << endl;
	dBodyDestroy( b->getBody() );
    removeReference( b );
} // xdynamics::destroyBody

void xdynamics::destroyGeom( xbody *b){
    // cout << "[xdynamics] destroyGeom" << endl;
	dGeomDestroy( b->getGeom(0) );
} // xdynamics::destroyGeom

void xdynamics::destroyJoints( xbody *b ){
    // cout << "[xdynamics] destroyJoints" << endl;
	dJointGroupDestroy( b->getJointGroup(0) );
} // xdynamics::destroyJoints

void xdynamics::addReference(xbody* e ){
	references.push_back( e );
} // xdynamics::addReference

void xdynamics::removeReference(xbody* e ){
	for (int i = references.size()-1; i > 0 ; i--){
        if ( references[i]->getID() == e->getID() ){
            // cout << "[xdynamics] remove reference " << i << endl;
            // delete references[i];
            references.erase( references.begin()+ i );
        }
	}
} // xdynamics::removeReference

void xdynamics::createBoxGeom( xbody *b, x3dpoint size, x3dpoint pos, x3dpoint rot ){ //
	// cout << "[xdynamics] createBoxGeom" << endl;
	// cout << "[xdynamics] rot :" << rot.X << ":" << rot.Y << ":" << rot.Z << endl;

	// set geom??!!
	b->setGeom( dCreateBox( space, size.X, size.Y, size.Z) );
	dGeomSetPosition( b->getGeom(0), pos.X, pos.Y, pos.Z ); // need position??

	// set a pointer to our object,
	dGeomSetData( b->getGeom(0), (void*)this ); // ---- ??

	// create and combine body and mass
	b->setBody( dBodyCreate( world ) );
	dMassSetBox( b->getMass(), 1.0, size.X, size.Y, size.Z );

	// this mass has nothing to do w/ bounding box. large objects are going to handel unrealisticly.
	// dMassSetBox(&b->getMass(), 1.0, (dReal)1.0,(dReal)1.0,(dReal)1.0);

	dBodySetMass(b->getBody(), b->getMass() );

    if ( b->getStatic() == 0 ){
        // add the body to the geom
        dGeomSetBody( b->getGeom(0), b->getBody() );

        // set the bodys position (same as geom position)
        dBodySetPosition( b->getBody(), pos.X, pos.Y, pos.Z);

        dQuaternion Q;
        eulerToQuaternion( irr::core::vector3df(rot.X, rot.Y, rot.Z) ,Q);
        dBodySetQuaternion ( b->getBody(), Q );

        dBodySetData( b->getBody(), (void*)this );
        // cout << "[xdynamics] end createBoxGeom" << endl;
    } else {
        // cout << "[xdynamics] static object, does not have body" << endl;
		dQuaternion Q;
		eulerToQuaternion( irr::core::vector3df(rot.X, rot.Y, rot.Z) ,Q);
		dGeomSetQuaternion ( b->getGeom(0), Q);
        dGeomSetPosition ( b->getGeom(0), pos.X, pos.Y, pos.Z);

		// geom converted from mesh has no body
		dGeomSetBody( b->getGeom(0), 0);
    }
} // xdynamics::createBoxGeom

void xdynamics::createSphereGeom( xbody *b, float radius, x3dpoint pos, x3dpoint rot ){
	// cout << "[xdynamics] createSphereGeom" << endl;

	b->setGeom( dCreateSphere( space, radius) );

	// set a pointer to our object,
	dGeomSetData( b->getGeom(0), (void*)this );

	// create and combine body and mass
	b->setBody( dBodyCreate( world ) );
	dMassSetSphere ( b->getMass(), 1, radius);
	dBodySetMass(b->getBody(), b->getMass() );

    // b->setStatic( 0 );
    if ( b->getStatic() == 0 ){
        // cout << "[xdynamics] dynamic object" << endl;
        // add the body to the geom
        dGeomSetBody( b->getGeom(0), b->getBody() );

        // set the body's position (same as geom position)
        dBodySetPosition( b->getBody(), pos.X, pos.Y, pos.Z);

        // set the body's rotation
        dQuaternion Q;
        eulerToQuaternion( irr::core::vector3df(rot.X, rot.Y, rot.Z) ,Q);
        dBodySetQuaternion ( b->getBody(), Q );

        // const dReal* rot = dBodyGetRotation( b->getBody() );
        // rot[0]

        dBodySetData( b->getBody(), (void*)this );

    } else {
        // cout << "[xdynamics] static object, does not have body" << endl;
		// test
		dQuaternion Q;
		eulerToQuaternion( irr::core::vector3df(rot.X, rot.Y, rot.Z) ,Q);
		dGeomSetQuaternion ( b->getGeom(0), Q);
        dGeomSetPosition ( b->getGeom(0), pos.X, pos.Y, pos.Z);

		// geom converted from mesh has no body
		dGeomSetBody( b->getGeom(0), 0);
    }
} // xdynamics::setCCylinderGeomData

// set triangle mesh geom data
void xdynamics::createTrimeshGeom( xbody *b, xmesh * m, x3dpoint pos, x3dpoint rot, bool mobile ){
	// cout << "\ncreateTrimeshGeom via xmesh" << endl;

	irr::scene::IMesh* mesh = m->mesh;
	dVector3 *vertices; // vertex array for trimesh geom
	int *indices; // index array for trimesh geom
	int vertexcount; // number of vertices in the vertex array
	int indexcount; // number of indices in the index array

	// do nothing if the mesh is NULL
	if ( mesh == NULL ) return;

	int i,j,ci,cif,cv;
	indexcount=0;
	vertexcount=0;
	// count vertices and indices
	for( i=0; i < mesh->getMeshBufferCount(); i++ ){
		irr::scene::IMeshBuffer* mb=mesh->getMeshBuffer(i);
		indexcount+=mb->getIndexCount();
		vertexcount+=mb->getVertexCount();
	}

	// build structure for ode trimesh geom
	vertices=new dVector3[vertexcount];
	indices=new int[indexcount];
	// fill trimesh geom
	ci=0; // current index in the indices array
	cif=0; // offset of the irrlicht-vertex-index in the vetices array
	cv=0; // current index in the vertices array

	// cout << "Mesh Buffer Count :" << mesh->getMeshBufferCount() << endl;
	x3dpoint scale = m->getScale();

	for ( i=0; i< mesh->getMeshBufferCount(); i++ ){
		irr::scene::IMeshBuffer* mb = mesh->getMeshBuffer(i);
		irr::u16* mb_indices = mb->getIndices();
		for(j=0; j < mb->getIndexCount(); j++){
			// scale the indices from multiple meshbuffers to single index array
			indices[ci] = cif+mb_indices[j];
			ci++;
		}
		// update the offset for the next meshbuffer
		cif=cif+mb->getVertexCount();
		if ( mb->getVertexType()==irr::video::EVT_STANDARD ){
			irr::video::S3DVertex* mb_vertices=
				(irr::video::S3DVertex*)mb->getVertices();
			for( j=0; j < mb->getVertexCount(); j++ ){
				vertices[cv][0] = mb_vertices[j].Pos.X*scale.X;
				vertices[cv][1] = mb_vertices[j].Pos.Y*scale.Y;
				vertices[cv][2] = mb_vertices[j].Pos.Z*scale.Z;
				cv++;
			}
		} else if ( mb->getVertexType() == irr::video::EVT_2TCOORDS ){
			irr::video::S3DVertex2TCoords* mb_vertices = (irr::video::S3DVertex2TCoords*)mb->getVertices();
			for ( j=0; j < mb->getVertexCount(); j++ ){
				vertices[cv][0] = mb_vertices[j].Pos.X*scale.X;
				vertices[cv][1] = mb_vertices[j].Pos.Y*scale.Y;
				vertices[cv][2] = mb_vertices[j].Pos.Z*scale.Z;
				cv++;
			}
		}
	}

	// build the trimesh data
	dTriMeshDataID data=dGeomTriMeshDataCreate();
	dGeomTriMeshDataBuildSimple( data, (dReal*) vertices,
			vertexcount, (dTriIndex*)indices, indexcount);

	// build the trimesh geom
	b->setGeom( dCreateTriMesh( space, data, 0, 0, 0 ) );
	// set the geom position
	dGeomSetPosition( b->getGeom( 0 ), pos.X, pos.Y, pos.Z);

	// lets have a pointer to our bounceable
	// we could need this in the collision callback
	dGeomSetData( b->getGeom( 0 ), (void*)this );

	if ( mobile == true ){
		// cout << "object has body" << endl;
		// geom converted from meshes has a body
		/*
		vector3df extend = (*m_v)[n_index].proto.box.getExtent();

		// create and combine body and mass
		b->setBody( dBodyCreate( world ) );
		dMassSetBox( &b->getMass, 1.0, (dReal)extend.X,(dReal)extend.Y,(dReal)extend.Z);
		// this mass has nothing to do w/ bounding box. large objects are going to handel unrealisticly.
		// dMassSetBox(&(*m_v)[n_index].dynamics.Mass, 1.0, (dReal)1.0,(dReal)1.0,(dReal)1.0);
		dBodySetMass( b->getBody(), &b->getMass);

		// add the body to the geom
		dGeomSetBody( b->getGeom(0), b->getBody() );
		// set the bodys position (same as geom position)
		dBodySetPosition( b->getBody(), pos.X,pos.Y,pos.Z);
		dMatrix3 R;

		dRFromEulerAngles (R, rot.X, rot.Y, rot.Z);
		dBodySetRotation ( b->getBody(), R);

		dBodySetData( b->getBody(), (void*)this);

		if ( (*m_v)[n_index].proto.bUseGrav == 0 ){
			// cout << "object is unaffected by gravity" << endl;
			dBodySetGravityMode ((*m_v)[n_index].dynamics.Body, 0);
		}
		*/
	} else {
		// cout << "[xdynamics] object does not have body" << endl;
		dQuaternion Q;
		eulerToQuaternion( irr::core::vector3df(rot.X, rot.Y, rot.Z) ,Q);
		dGeomSetQuaternion ( b->getGeom(0), Q);

		// geom converted from mesh has no body
		dGeomSetBody( b->getGeom(0), 0);
	}
} // xdynamics::createTrimeshGeom

// set triangle mesh geom data
void xdynamics::createTrimeshGeomFromTerrain( xbody *b, xterrainmesh * t, x3dpoint pos, x3dpoint rot, bool mobile ){
	// cout << "[xdynamics] createTrimeshGeom via xterrainmesh" << endl;

	irr::scene::IMesh* mesh = t->terrain->getMesh();

	dVector3 *vertices; // vertex array for trimesh geom
	int *indices; // index array for trimesh geom
	int vertexcount; // number of vertices in the vertex array
	int indexcount; // number of indices in the index array

	// do nothing if the mesh is NULL
	if ( mesh == NULL ) return;

	int i,j,ci,cif,cv;
	indexcount=0;
	vertexcount=0;
	// count vertices and indices

	for( i=0; i < mesh->getMeshBufferCount(); i++ ){
		scene::CDynamicMeshBuffer* mb = new scene::CDynamicMeshBuffer(video::EVT_2TCOORDS, video::EIT_16BIT);
        	t->terrain->getMeshBufferForLOD(*mb, 0);

		// terrain->getMeshBufferForLOD( &mb, 0 );
		indexcount+=mb->getIndexCount();
		vertexcount+=mb->getVertexCount();
		// mb->drop(); // When done drop the buffer again.
	}

	// build structure for ode trimesh geom
	vertices=new dVector3[vertexcount];
	indices=new int[indexcount];
	// fill trimesh geom
	ci=0; // current index in the indices array
	cif=0; // offset of the irrlicht-vertex-index in the vetices array
	cv=0; // current index in the vertices array

	// cout << "Mesh Buffer Count :" << mesh->getMeshBufferCount() << endl;
	x3dpoint scale = t->getScale();
	for ( i=0; i < mesh->getMeshBufferCount(); i++ ){
		scene::CDynamicMeshBuffer* mb = new scene::CDynamicMeshBuffer(video::EVT_2TCOORDS, video::EIT_16BIT);
        	t->terrain->getMeshBufferForLOD( *mb, 0 );

		irr::u16* mb_indices = mb->getIndices();
		for ( j=0; j < mb->getIndexCount(); j++ ){
			// scale the indices from multiple meshbuffers to single index array
			indices[ci]=cif+mb_indices[j];
			ci++;
		}
		// update the offset for the next meshbuffer
		cif=cif+mb->getVertexCount();
		if ( mb->getVertexType()==irr::video::EVT_STANDARD ){
			irr::video::S3DVertex* mb_vertices=
				(irr::video::S3DVertex*)mb->getVertices();
			for( j=0; j < mb->getVertexCount(); j++ ){
				vertices[cv][0] = mb_vertices[j].Pos.X*scale.X;
				vertices[cv][1] = mb_vertices[j].Pos.Y*scale.Y;
				vertices[cv][2] = mb_vertices[j].Pos.Z*scale.Z;
				cv++;
			}
		} else if ( mb->getVertexType() == irr::video::EVT_2TCOORDS ){
			irr::video::S3DVertex2TCoords* mb_vertices = (irr::video::S3DVertex2TCoords*)mb->getVertices();
			for ( j=0; j < mb->getVertexCount(); j++ ){
				vertices[cv][0] = mb_vertices[j].Pos.X*scale.X;
				vertices[cv][1] = mb_vertices[j].Pos.Y*scale.Y;
				vertices[cv][2] = mb_vertices[j].Pos.Z*scale.Z;
				cv++;
			}
		}
	}

	// build the trimesh data
	dTriMeshDataID data = dGeomTriMeshDataCreate();
	dGeomTriMeshDataBuildSimple( data, (dReal*) vertices,
			vertexcount, (dTriIndex*) indices, indexcount);

	// build the trimesh geom
	b->setGeom( dCreateTriMesh( space, data, 0, 0, 0 ) );
	// set the geom position
	dGeomSetPosition( b->getGeom(0), pos.X, pos.Y, pos.Z);

	// lets have a pointer to our bounceable
	// we could need this in the collision callback
	dGeomSetData( b->getGeom(0), (void*)this );

	if ( mobile == true ){
	} else {
		// cout << "[xdynamics] object is immobile" << endl;
		dMatrix3 R;
		dRFromEulerAngles (R, rot.X, rot.Y, rot.Z);
		dGeomSetRotation ( b->getGeom( 0 ), R);
		// geom converted from mesh has no body
		dGeomSetBody( b->getGeom(0), 0);
	}
} // xdynamics::createTrimeshGeom

void xdynamics::setPlane2d (xbody * o1) {
	// TODO: setJointGroup should return an int, the index that it was inserted at?
	// TODO: delete goints & groups
	o1->setJointGroup( dJointGroupCreate ( 0 ) );
	o1->setJoint( dJointCreatePlane2D ( world, o1->getJointGroup ( 0 ) ) );
	dJointAttach ( o1->getJoint(0), o1->getBody(), 0 );

	// is this all?
} // xdynamics::setPlane2d

xcollision * xdynamics::getCollisionFromLine( x3dpoint pos, x3dpoint rot, float len ) { // pos, rot, length
	// cout << "[xdynamics] getCollisionFromLine" << endl;
	// m_driver->setTransform(video::ETS_WORLD, core::matrix4());
	rot.X -= 90;

	dQuaternion result;
	eulerToQuaternion(rot.getv3df(), result);
	dGeomID ray = dCreateRay(space, len); // ??
	vector3df vpos = pos.getv3df();
    dGeomSetCollideBits ( ray, 0 );

	dGeomSetQuaternion (ray, result);
	dGeomSetPosition (ray, vpos.X, vpos.Y, vpos.Z);
	dContactGeom contact;

	int num = dSpaceGetNumGeoms(space);
	for (int ii=0; ii<num; ++ii) {
		dGeomID geo = dSpaceGetGeom(space, ii);
		if ( dCollide( ray, geo, 1, &contact, sizeof(contact) ) ){
			// if (geo !=  dynamics.Geom[0]){
				dSpaceRemove( space, ray );
				dGeomDestroy( ray );

                cout << "[xdynamics] generating contact data" << endl;
                vector3df hPos = vector3df( contact.pos[0], contact.pos[1], contact.pos[2] );

				xcollision * c = new xcollision();
                c->seto1( getxbodyByGeomID( geo ) );
                c->setContactPoint( x3dpoint( hPos.X, hPos.Y, hPos.Z)  );
                collision_que.push_back( c );

                return c;
			// }
		}
	}
	dSpaceRemove( space, ray );
	dGeomDestroy( ray );

    /*
	dGeomID ray = dCreateRay( space, len );
	dReal *r;
	r[0] = rot.X;
	r[1] = rot.Y;
	r[2] = rot.Z;

    cout << "[xdynamics] setting length" << endl;
    dGeomRaySetLength( ray, len );
    // dGeomRaySet ( ray, pos.X, pos.Y, pos.Z, rot.X, rot.Y, rot.Z);

    cout << "[xdynamics] setting rotation" << endl;
	dGeomSetRotation (ray, r);
	cout << "[xdynamics] setting position" << endl;
	dGeomSetPosition (ray, pos.X, pos.Y, pos.Z);

    cout << "[xdynamics] building contact list" << endl;

	int newRange, oldRange, hitIndex;
	int did_hit = 0;
	oldRange = 99999;
	vector < dGeomID > hitObject;
	vector < vector3df > hitPos;

	dContactGeom contact; // dont create a contact joint for rays?

	int num = dSpaceGetNumGeoms( space );
	cout << "[xdynamics] casting ray in space with " << num << " geoms" << endl;
	// gather hit pos & object_index from all objects along ray's path

	for ( int ii=0; ii<num; ++ii ) {
		dGeomID geo = dSpaceGetGeom( space, ii );
		cout << "[xdynamics] testing geom #" << ii <<", type : " << dGeomGetClass( geo ) << endl;
		// exclude by range
		// dGeomGetPosition ( geo );

        if ( dCollide( ray, geo, 1, &contact, sizeof(contact) ) ){
            did_hit = 1;
            cout << "[xdynamics] generating contact data" << endl;
            vector3df hPos = vector3df( contact.pos[0], contact.pos[1], contact.pos[2] );
            hitPos.push_back( hPos );

            hitObject.push_back( geo );
		}
	}

	dSpaceRemove( space, ray );
	dGeomDestroy( ray );

	if (did_hit == 1){

        cout << "[xdynamics] sorting by closest contact" << endl;
        hitIndex = 0;
        //order by closest
        for ( int i = 0; i < hitPos.size(); i++){
            float Ax = pos.X; // get absolute position!
            float Ay = pos.Y;
            float Az = pos.Z;

            float Bx = hitPos[i].X;
            float By = hitPos[i].Y;
            float Bz = hitPos[i].Z;

            float dx, dy, dz;

            dx = Ax-Bx;
            dy = Ay-By;
            dz = Az-Bz;
            float zerorange = sqrt(dx*dx + dy*dy + dz*dz);
            newRange = zerorange;
            if (newRange < oldRange){
                oldRange = newRange;
                hitIndex = i;
            }
        }

        // cout << "[xdynamics] e" << endl;

	    cout << "[xdynamics] building collision data" << endl;
		// build collision data
		xcollision * c = new xcollision();
        c->seto1( getxbodyByGeomID( hitObject[ hitIndex ] ) );
        c->setContactPoint( x3dpoint( hitPos[ hitIndex ].X, hitPos[ hitIndex ].Y, hitPos[ hitIndex ].Z)  );

		return c;
	}
	*/
    cout << "[xdynamics] no collision, returning nil" << endl;
    return NULL; // hmm
} // xdynamics::getCollisionFromLine


/*
void xdynamics::setBallJoint (xbody * o1, xbody * o2, x3dpoint pos){
	// TODO: add support for more then one joint?
	o1->JointGroup[0] = dJointGroupCreate (0);
	o1->getJoint(0) = dJointCreateBall ( world, o1->JointGroup[0] );

	dJointAttach ( o1->getJoint(0), o1->getBody(), o2->getBody() ); //

	// dJointSetFixed ( (*m_v)[n_index].dynamics.Joint[0] );
	dJointSetBallAnchor ( o1->getJoint(0), (dReal)pos.X, (dReal)pos.Y, (dReal)pos.Z);

	// wtf do i do this for?!
	dVector3 result;
	dJointGetBallAnchor ( o1->getJoint(0), result);
	dJointGetBallAnchor2 ( o1->getJoint(0), result);
} // xdynamics::setBallJoint

void xdynamics::setHinge2Joint ( xbody * o1, xbody * o2, x3dpoint pos, x3dpoint xaxis, x3dpoint zaxis ){
	o1->JointGroup[0] = dJointGroupCreate (0);
	o1->getJoint(0) = dJointCreateHinge2 ( world, o1->JointGroup[0] );

	dJointAttach ( o1->getJoint(0), o1->getBody(), o2->getBody() );
	dJointSetHinge2Anchor ( o1->getJoint(0), (dReal)pos.X, (dReal)pos.Y, (dReal)pos.Z );

	// uhoh. need 2 x3dpoints?
	int x0 = xaxis.X;
	int y0 = xaxis.Y;
	int z0 = xaxis.Z;

	int x2 = zaxis.X;
	int y2 = zaxis.Y;
	int z2 = zaxis.Z;

	dJointSetHinge2Axis1 ( o1->getJoint(0), (dReal)x0, (dReal)y0, (dReal)z0 );
	dJointSetHinge2Axis2 ( o1->getJoint(0), (dReal)x2, (dReal)y2, (dReal)z2 );

	dJointSetHinge2Param ( o1->getJoint[0], dParamSuspensionERP, 0.8);   // set this?
	dJointSetHinge2Param ( o1->getJoint[0], dParamSuspensionCFM, 1e-5);  // set this?
	// dJointSetHinge2Param ( o1->.Joint[0], dParamVel2, 0);
	// dJointSetHinge2Param ( o1->.Joint[0], dParamFMax2, 25);

} // xdynamics::setHinge2Joint

void xdynamics::setAMotor ( xbody * o1 ){ // TODO: make xbody auto index joints
	intj; // jointindex?
	// wtf is this?
	if ( o2 == null ){
		o1->Joint[j]=dJointCreateAMotor( world, 0 ); // Amotor
		dJointAttach( o1->Joint[j], o1->getBody() , 0 );
	} else {
		o1->Joint[j] = dJointCreateAMotor( world, o1->JointGroup[0] );
		dJointAttach ( o1->getJoint(j), o1->getBody(), o2->getBody() );
	}

	dJointSetAMotorMode( o1->getJoint[j], dAMotorEuler );

	// hmmm... how to do this?
	int x0 = o1->jointXAxis.X;
	int y0 = o1->jointXAxis.Y;
	int z0 = o1->jointXAxis.Z;

	int x2 = o1->jointZAxis.X;
	int y2 = o1->jointZAxis.Y;
	int z2 = o1->jointZAxis.Z;

	dJointSetAMotorAxis( o1->getJoint(j), 0, 1, x0, y0, z0 );
	dJointSetAMotorAxis( o1->getJoint(j), 2, 1, x2, y2, z2 );

	int loX = (*m_v)[n].proto.jointLoStop.X;
	int loY = (*m_v)[n].proto.jointLoStop.Y;
	int loZ = (*m_v)[n].proto.jointLoStop.Z;

	int hiX = (*m_v)[n].proto.jointHiStop.X;
	int hiY = (*m_v)[n].proto.jointHiStop.Y;
	int hiZ = (*m_v)[n].proto.jointHiStop.Z;

	if (loX != X_INFINITY ){
		dJointSetAMotorParam( o1->Joint[j],dParamLoStop,  loX);
	} else {
		// cout << "dInfinity" << endl;
		dJointSetAMotorParam( o1->Joint[j],dParamLoStop,  -dInfinity);
	}

	if (loY != X_INFINITY){
		dJointSetAMotorParam( o1->Joint[j],dParamLoStop2, loY);
	} else {
		// cout << "dInfinity" << endl;
		dJointSetAMotorParam( o1->Joint[j],dParamLoStop2, -dInfinity);
	}

	if (loZ != X_INFINITY){
		dJointSetAMotorParam( o1->Joint[j],dParamLoStop3, loZ);
	} else {
		// cout << "dInfinity" << endl;
		dJointSetAMotorParam( o1->Joint[j],dParamLoStop3, -dInfinity);
	}

	if (hiX != X_INFINITY){
		dJointSetAMotorParam( o1->Joint[j],dParamHiStop,  hiX);
	} else {
		// cout << "dInfinity" << endl;
		dJointSetAMotorParam( o1->Joint[j],dParamHiStop,  dInfinity);
	}

	if (hiY != X_INFINITY){
		dJointSetAMotorParam( o1->Joint[j],dParamHiStop2, hiY);
	} else {
		// cout << "dInfinity" << endl;
		dJointSetAMotorParam( o1->Joint[j],dParamHiStop2, dInfinity);
	}

	if (hiZ != X_INFINITY){
		dJointSetAMotorParam( o1->Joint[j],dParamHiStop3, hiZ);
	} else {
		// cout << "dInfinity" << endl;
		dJointSetAMotorParam( o1->Joint[j],dParamHiStop3, dInfinity);
	}

	dJointSetAMotorParam( o1->getJoint(j), dParamFudgeFactor, (*m_v)[n].proto.fJointFactor );
	dJointSetAMotorParam( o1->getJoint(j), dParamFMax,    (*m_v)[n].proto.fJointFMax );
	dJointSetAMotorParam( o1->getJoint(j), dParamBounce,  (*m_v)[n].proto.fJointBounce );
	dJointSetAMotorParam( o1->getJoint(j), dParamVel,     (*m_v)[n].proto.fJointVel );
} // xdynamics::setAMotor
*/

void xdynamics::setGravity( x3dpoint g ){
	// cout << "[xdynamics] setGravity" << endl;
	dWorldSetGravity( world, g.X, g.Y, g.Z);
} // xdynamics::setGravity

void xdynamics::setERP( float erp ){
	dWorldSetERP( world, erp);
} // xdynamics::setERP

void xdynamics::setCFM( float cfm ){
	dWorldSetCFM( world, cfm);
} // xdynamics::setCFM

xbody * xdynamics::getxbodyByGeomID( dGeomID gid ){
	for (int i = 0; i < references.size(); i++){
        if ( references[i]->getGeom(0) == gid) {
            return references[i];
        }
	}
	cout << "[xdynamics] error; invalid geomid" << endl;
	return NULL;
}

void xdynamics::eulerToQuaternion( const irr::core::vector3df &euler, dQuaternion quaternion ){
	double _heading=euler.Z*irr::core::DEGTORAD/2.0; //GRAD_PI2
	double _attitude=euler.Y*irr::core::DEGTORAD/2.0;
	double _bank=euler.X*irr::core::DEGTORAD/2.0;
	double c1 = cos(_heading);
	double s1 = sin(_heading);
	double c2 = cos(_attitude);
	double s2 = sin(_attitude);
	double c3 = cos(_bank);
	double s3 = sin(_bank);
	double c1c2 = c1*c2;
	double s1s2 = s1*s2;

	//w
	quaternion[0]=(dReal) (c1c2*c3 + s1s2*s3);
	//x
	quaternion[1]=(dReal) (c1c2*s3 - s1s2*c3);
	//y
	quaternion[2]=(dReal) (c1*s2*c3 + s1*c2*s3);
	//z
	quaternion[3]=(dReal) (s1*c2*c3 - c1*s2*s3);
} // xdynamics::eulerToQuaternion

x3dpoint xdynamics::quaternionToEuler( const dQuaternion quaternion ){
// void simsys::QuaternionToEuler(const dQuaternion quaternion, irr::core::vector3df &euler){
	irr::core::vector3df euler;
	dReal w,x,y,z;
	w=quaternion[0];
	x=quaternion[1];
	y=quaternion[2];
	z=quaternion[3];
	double sqw = w*w;
	double sqx = x*x;
	double sqy = y*y;
	double sqz = z*z;
	// heading
	euler.Z = (irr::f32) (atan2(2.0 * (x*y + z*w),(sqx - sqy - sqz + sqw))*irr::core::RADTODEG); // GRAD_PI
	// bank
	euler.X = (irr::f32) (atan2(2.0 * (y*z + x*w),(-sqx - sqy + sqz + sqw))*irr::core::RADTODEG);
	// attitude
	euler.Y = (irr::f32) (asin(-2.0 * (x*z - y*w))*irr::core::RADTODEG);

	return x3dpoint(euler.X, euler.Y, euler.Z);
} // xdynamics::quaternionToEuler

