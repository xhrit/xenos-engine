// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef X3DCIRCLE_H
#define X3DCIRCLE_H

#include "xelement.h"       // Simulation objects
#include "CCircle.h"       // Simulation objects

#include <irrlicht.h>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// x3circle : this is a line in 3d space
///

class x3dcircle : public xelement {
	public:

		///
		/// constructor.
		/// \param dev pointer to irrlicht
		/// \param id id.
		/// \param x2drect width and height.
		/// \param c name?
		///
		x3dcircle( irr::IrrlichtDevice *dev, int id, x3dpoint center, float radius );

		///
		/// destructor.
		///
		~x3dcircle();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		void setColour( xcolour c );

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint r );

        x3dpoint getPosition();
		x3dpoint getRotation();

	private:
		x3dpoint position;
		x3dpoint rotation;
        float radius;

		xcolour colour;

		CCircle* node;
};

#endif // X3DCIRCLE_H
