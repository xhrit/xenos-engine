// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef X3DLINE_H
#define X3DLINE_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// x3dline : this is a line in 3d space
///

class x3dline : public xelement {
	public:

		///
		/// constructor.
		/// \param dev pointer to irrlicht
		/// \param id id.
		/// \param x2drect width and height.
		/// \param c name?
		///
		x3dline( irr::IrrlichtDevice *dev, int id, x3dpoint s, x3dpoint e );

		///
		/// destructor.
		///
		~x3dline();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		void setColour( xcolour c );

		void setStart( x3dpoint p );
		void setEnd( x3dpoint p );
		x3dpoint getStart();
		x3dpoint getEnd();
        x3dpoint getClosestPoint( x3dpoint p );

	private:
		x3dpoint startpoint, endpoint;
		xcolour colour;
};

#endif // X3DLINE_H
