// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xtextbox.h

#ifndef XTEXTBOX_H
#define XTEXTBOX_H

#include "xelement.h"       // Simulation objects
#include "CGUITextBox.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xtextbox : static text
///
/// This is a static text element.
///

class xtextbox : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param txt string.
		/// \param x1 an integer argument.
		/// \param x2 an integer argument.
		/// \param y1 an integer argument.
		/// \param y21 an integer argument.
		///
		xtextbox( irr::IrrlichtDevice *dev, int id, x2drect r );

		///
		/// destructor.
		///
		~xtextbox();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		char * getText();
		void setText( char * c );
		void setParent( xelement * x );

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;            // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		CGUITextBox* textbox;
};

#endif // XTEXTBOX_H
