// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xmesh.h

#ifndef XMESH_H
#define XMESH_H

#include "xelement.h"       // Simulation objects
#include "xbone.h"

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xmesh : 3d mesh
///
/// This is a static 3d mesh element. It supports many file formats.
///

class xmesh : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param mesh character array ov mesh to load.
		/// \param tex texture to load.
		///
		xmesh( irr::IrrlichtDevice *dev, int id, char * mesh_fn);

		///
		/// destructor.
		///
		~xmesh();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint p );
		void setScale( x3dpoint p );

		void setMaterialFlag( XMATERIAL_FLAG mf, bool val );
		void setMaterialType( XMATERIAL_TYPE mt );
		void addNormalMap( char * bump, float factor );
		void addTexture( char * fn, int index );
		void setMD2Animation( int i );
		void setFrameLoop( int begin, int end );
		void createTriangleSelector();

		x3dpoint getPosition();
		x3dpoint getRotation();
		x3dpoint getScale();

		// new animation functions
		int getStartFrame();
		int getEndFrame();
		void setCurrentFrame( int frame );

		void setAnimationSpeed( int fps);
		void setLoopMode( bool loop );
		void addShadowVolume( bool zfail );

		void setParent( xelement * x );
		xbone * getBone (  char * jointName );

		void setDebug( bool b);
		void setHardwareMapping( X_HARDWARE_MAPPING hint, X_BUFFER_TYPE buffer );

		IAnimatedMesh* mesh;
		IAnimatedMeshSceneNode* node;
		ITriangleSelector* selector;
	 	IShadowVolumeSceneNode * shadow;

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;      // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		x3dpoint scale;
		bool hasShadow;
};

#endif // XMESH_H
