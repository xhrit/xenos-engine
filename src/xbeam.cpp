// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xbeam.cpp

#include "xbeam.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xbeam::xbeam( irr::IrrlichtDevice *dev, int id, char * tx_fn1, char * tx_fn2 ) : xelement( dev, id ){
	// cout << "[xbeam] create xbeam" << endl;
	beam = new scene::CBeamNode( 0, irr_smgr, id, tx_fn1, tx_fn2 );

	setType( XELEMENT_TYPE_BEAM );
	node_reference = beam;
}

xbeam::~xbeam(){
	// cout << "[xbeam] delete xbeam" << endl;
}

int xbeam::update(){
	// cout << "[xbeam] xbeam::update" << endl;
	return 1;
}

int xbeam::render(){
	// cout << "[xbeam] xbeam::render" << endl;
	if ( getType() == XELEMENT_TYPE_BEAM ){
	    beam->OnRegisterSceneNode();
    }
	// cout << "[xbeam] /xbeam::render" << endl;
	return 1;
} // xbeam::render

bool xbeam::event(){
	// cout << "[xbeam] xbeam::event" << endl;
	return false;
} // xbeam::event

void xbeam::destroy(){
	// cout << "[xbeam] xbeam::destroy" << endl;
    delete beam;
    setType( XELEMENT_TYPE_NULL );
} // xbeam::destroy

void xbeam::setLine(x3dpoint start, x3dpoint end, float scale ){
	//now set the beam
	beam->setLine( start.getv3df(), end.getv3df(), scale);
} // xbeam::setLine

void xbeam::setColour( xcolour c ){
	beam->setColour( c.getSColour() );
} // xbeam::setColor

void xbeam::setMaterialFlag( XMATERIAL_FLAG mf, bool val ){
	beam->setMaterialFlag ( (E_MATERIAL_FLAG)mf, val );

} // xbeam::setMaterialFlag

// TODO: fixme!!!
void xbeam::setMaterialType( XMATERIAL_TYPE mt){ // ?
	// cout << "[xbeam] xbeam::setMaterialType" << endl;

	beam->setMaterialType( (E_MATERIAL_TYPE)mt );
	video::IMaterialRenderer* renderer = irr_driver->getMaterialRenderer( (E_MATERIAL_TYPE)mt );

	// display some problem text when problem
	if (!renderer || renderer->getRenderCapability() != 0){
		cout << "[xbeam] unable to render effect: " << mt << endl;;
	}

} // xbeam::setMaterialType

void xbeam::addNormalMap( char * bump, float factor ){
	// cout << "[xbeam] xbeam::addNormalMap" << endl;
	video::ITexture* normalMap = irr_driver->getTexture( bump );
	irr_driver->makeNormalMapTexture( normalMap, factor );
	beam->setMaterialTexture( 1, normalMap );
	// node->setMaterialType( EMT_NORMAL_MAP_SOLID);

} // xbeam::addNormalMap

void xbeam::addTexture( char * fn, int index ){
	// cout << "[xbeam] addTexture: " << tex_fn << endl;
	beam->setMaterialTexture( index, irr_driver->getTexture( fn ) );
} // xbeam::addTexture

