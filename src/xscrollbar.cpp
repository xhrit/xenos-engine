// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xscrollbar.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xscrollbar::xscrollbar( irr::IrrlichtDevice *dev, int id, bool o, x2drect r) : xelement( dev, id ){
	// cout << "[xscrollbar] create xscrollbar" << endl;

	scrollbar = irr_env->addScrollBar(o,
		rect<s32>(r.X1, r.Y1, r.X2, r.Y2),
		0, id);
	element_reference = scrollbar;

	setType( XELEMENT_TYPE_SCROLLBAR );
}

xscrollbar::~xscrollbar(){
	// cout << "[xscrollbar] delete xscrollbar" << endl;
}

int xscrollbar::update(){
	// cout << "[xscrollbar] xelement::update" << endl;
	return 1;
}

int xscrollbar::render(){
	// cout << "[xscrollbar] xelement::render" << endl;
	return 1;
}

bool xscrollbar::event(){
	// cout << "[xscrollbar] xelement::event" << endl;
	return false;
}

void xscrollbar::destroy(){
	// cout << "[xscrollbar] xelement::destroy" << endl;
	scrollbar->remove();
}

void xscrollbar::setParent( xelement * x ){
	parent = x;
	scrollbar->setParent( x->getGfxElement() );
} // xscrollbar::setParent

void xscrollbar::setVisible( bool v ){
	// cout << "[xscrollbar" << element_id <<"] setVisible" << endl;
	scrollbar->setVisible( v );
} // xscrollbar::setVisible
