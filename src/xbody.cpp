// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xbody.h"

#include <iostream>

using namespace std;

// collision/physics functions
xbody::xbody() {
	geomCount = 0;
	jointCount = 0;
	jointGroupCount = 0;
	mu = dInfinity;
	bounce = 0;
	softERP = 0;
	softCFM = 0;
	is_static = 0;
}

xbody::~xbody() {}

void xbody::addForce( x3dpoint f ){
	dBodyAddRelForce ( body, f.X, f.Y, f.Z );
} // xbody::addForce

void xbody::addRelForce( x3dpoint f ){
	dBodyAddRelForce ( body, f.X, f.Y, f.Z );
} // xbody::addRelForceAtRelPos

void xbody::addRelForceAtRelPos( x3dpoint f, x3dpoint p ){
	dBodyAddRelForceAtRelPos( body, f.X, f.Y, f.Z, p.X, p.Y, p.Z);
} // xbody::addRelForceAtRelPos

void xbody::setLinearVel( x3dpoint v ){
	dBodySetLinearVel ( body, v.X, v.Y, v.Z  );
} // xbody::addRelForceAtRelPos

void xbody::setAngularVel( x3dpoint v ){
	dBodySetAngularVel ( body, v.X, v.Y, v.Z );
} // xbody::addRelForceAtRelPos

x3dpoint xbody::getLinearVel(){
    dReal* ode_pos;
   	x3dpoint vel;

    ode_pos=(dReal*)dBodyGetLinearVel( body );

	vel.X = ode_pos[0];
	vel.Y = ode_pos[1];
	vel.Z = ode_pos[2];

    return vel;
} // xbody::getLinearVel


x3dpoint xbody::getAngularVel(){
    dReal* ode_pos;
   	x3dpoint vel;

    ode_pos=(dReal*)dBodyGetAngularVel( body );

	vel.X = ode_pos[0];
	vel.Y = ode_pos[1];
	vel.Z = ode_pos[2];

    return vel;

} // xbody::getAngularVel


void xbody::setID(int i){bodyID = i; }
int xbody::getID(){ return bodyID;}

void xbody::setStatic(int i){ is_static = i; }
int xbody::getStatic(){ return is_static;}

dBodyID xbody::getBody(){ return body; }
dMass * xbody::getMass(){ return &mass; }
dGeomID xbody::getGeom( int i ){ return geom[i]; }
dJointID xbody::getJoint( int i ){ return joint[i]; }
dJointGroupID xbody::getJointGroup( int i ){ return jointGroup[i]; }

void xbody::setBody(dBodyID b){ body = b; }
void xbody::setMass( dMass m ){ mass = m; }
void xbody::setGeom( dGeomID g ){
	geom[ geomCount ] = g;
	geomCount++;
} // xbody::setGeom

void xbody::setJoint( dJointID j ){
	joint[ jointCount ] = j;
	jointCount++;
} // xbody::setGeom

void xbody::setJointGroup( dJointGroupID j ){
	jointGroup[ jointGroupCount ] = j;
	jointGroupCount++;
} // xbody::setGeom

void xbody::setMU( float f ) { mu = f; }
void xbody::setBounce( float f ){ bounce = f; }
void xbody::setSoftERP( float f ){ softERP = f; }
void xbody::setSoftCFM( float f ){ softCFM = f; }

float xbody::getMU() { return mu; }
float xbody::getBounce() { return bounce; }
float xbody::getSoftERP() { return softERP; }
float xbody::getSoftCFM() { return softCFM; }

int xbody::getGeomCount(){ return geomCount; }
int xbody::getJointCount(){ return jointCount; }
void xbody::setGeomCount(int i ){ geomCount = i; }
void xbody::setJointCount(int i ){ jointCount = i; }

void xbody::setCategoryBits( float bits ) { dGeomSetCategoryBits ( getGeom(0), bits ); }
void xbody::setCollideBits( float bits ) { dGeomSetCollideBits ( getGeom(0), bits ); }

x3dpoint xbody::getPosition(){
    // cout << "[xbody] getPosition" << endl;
	x3dpoint pos;
    dReal* ode_pos;

    if ( is_static == 0 ){
        // cout << "[xbody] dyn pos" << endl;
        ode_pos=(dReal*)dBodyGetPosition( body );
    } else {
        // cout << "[xbody] static pos" << endl;
        ode_pos=(dReal*)dGeomGetPosition( getGeom(0) );
    }
	pos.X = ode_pos[0];
	pos.Y = ode_pos[1];
	pos.Z = ode_pos[2];

	return pos;
} // xbody::getPosition

void xbody::setPosition( x3dpoint p ){
    // cout << "[xbody] setPosition" << endl;
    if ( is_static == 1 ){
        dGeomSetPosition ( getGeom(0), p.X, p.Y, p.Z);
    } else {
        // cout << "[xbody] setting position of non-static body" << endl;
        dBodySetPosition ( body, p.X, p.Y, p.Z );
    }
}

// TODO: fix problem code
x3dpoint xbody::getRotation(){
	// cout << "[xbody] getRotation" << endl;
	// get the rotation quaternion
	dQuaternion result;
	float qs = dBodyGetQuaternion( body )[0];
	float qx = dBodyGetQuaternion( body )[1];
	float qy = dBodyGetQuaternion( body )[2];
	float qz = dBodyGetQuaternion( body )[3];
	result[0] = qs;
	result[1] = qx;
	result[2] = qy;
	result[3] = qz;

	// cout << "[xbody] quat:" << result[0] << ":" << result[1] << ":" << result[2] << ":" << result[3] <<  endl;

	// convert it to eulerangles
	return quaternionToEuler( result );
} // xbody::getRotation

void xbody::setEnabled( int i){
	dBodySetAutoDisableFlag(body, i);
}

int xbody::getEnabled(){
	return dBodyGetAutoDisableFlag(body);
}

x3dpoint xbody::quaternionToEuler( const dQuaternion quaternion ){
	// cout << "[xbody] quaternionToEuler" << endl;
	x3dpoint rot;

	dReal w,x,y,z;
	w=quaternion[0];
	x=quaternion[1];
	y=quaternion[2];
	z=quaternion[3];

	// cout << "[xbody] quat:" << w << ":" << x << ":" << y << ":" << z <<  endl;

	double sqw = w*w;
	double sqx = x*x;
	double sqy = y*y;
	double sqz = z*z;
	// heading
	rot.Z = (atan2(2.0 * (x*y + z*w),(sqx - sqy - sqz + sqw))*irr::core::RADTODEG);
	// bank
	rot.X = (atan2(2.0 * (y*z + x*w),(-sqx - sqy + sqz + sqw))*irr::core::RADTODEG);
	// attitude
	rot.Y = (asin(-2.0 * (x*z - y*w))*irr::core::RADTODEG);

	// cout << "[xbody] rot:" << rot.X << ":" << rot.Y << ":" << rot.Z << endl;
	//TODO: convert euler back to quat to validate output?

	return rot;
} // xbody::quaternionToEuler

void xbody::eulerToQuaternion(const irr::core::vector3df &euler, dQuaternion quaternion){
	double _heading  = euler.Z * irr::core::DEGTORAD/2.0;
	double _attitude = euler.Y * irr::core::DEGTORAD/2.0;
	double _bank     = euler.X * irr::core::DEGTORAD/2.0;
	double c1 = cos(_heading);
	double s1 = sin(_heading);
	double c2 = cos(_attitude);
	double s2 = sin(_attitude);
	double c3 = cos(_bank);
	double s3 = sin(_bank);
	double c1c2 = c1*c2;
	double s1s2 = s1*s2;

	//w
	quaternion[0]=(dReal) (c1c2*c3 + s1s2*s3);
	//x
	quaternion[1]=(dReal) (c1c2*s3 - s1s2*c3);
	//y
	quaternion[2]=(dReal) (c1*s2*c3 + s1*c2*s3);
	//z
	quaternion[3]=(dReal) (s1*c2*c3 - c1*s2*s3);
} // xbody::eulerToQuaternion

void xbody::alignToAxis() { // align to y?
	// cout << "[xbody" << bodyID << "] alignToAxis" << endl;
	const dReal *rot = dBodyGetAngularVel( body );
	const dReal *quat_ptr;
	dReal quat[4], quat_len;
	quat_ptr = dBodyGetQuaternion( body );
	quat[0] = quat_ptr[0];
	quat[1] = 0;
	quat[2] = 0;
	quat[3] = quat_ptr[3];
	quat_len = sqrt( quat[0] * quat[0] + quat[3] * quat[3] );
	quat[0] /= quat_len;
	quat[3] /= quat_len;
	dBodySetQuaternion( body, quat );
	dBodySetAngularVel( body, 0, 0, rot[2] );
} // xbody::alignToAxis

void xbody::damp( float f ){
	// cout << "[xbody" << bodyID << "] damp" << endl;
	const dReal *LinearVel = dBodyGetLinearVel( body );
	const dReal *AngularVel = dBodyGetAngularVel( body );
	dBodySetLinearVel  ( body, LinearVel[0] - (LinearVel[0]*f), LinearVel[1] - (LinearVel[1]*f), LinearVel[2] - (LinearVel[2]*f));
	dBodySetAngularVel ( body, AngularVel[0] - (AngularVel[0]*f), AngularVel[1] - (AngularVel[1]*f), AngularVel[2] - (AngularVel[2]*f) );
} // xbody::damp
