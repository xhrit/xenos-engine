// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XLISTBOX_H
#define XLISTBOX_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xlistbox : static text
///
/// This is a static text element.
///

class xlistbox : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param txt string.
		/// \param x1 an integer argument.
		/// \param x2 an integer argument.
		/// \param y1 an integer argument.
		/// \param y21 an integer argument.
		///
		xlistbox( irr::IrrlichtDevice *dev, int id, bool b, x2drect r );

		///
		/// destructor.
		///
		~xlistbox();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		///
		/// Add item to list
		///
		void addItem( char * c );
		char * getItem( int i );
		int getSelected();
		void setSelected( int n );
		void setParent( xelement * x );

		void setVisible( bool v );
		void clear();

		int getItemCount();
		void setItemColor( int i, int n, xcolour c );
		int setAutoScroll( bool b );
        int removeItem( int n );
		void setPosition( x2dpoint p );

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;            // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		IGUIListBox* listbox;
		float w, h;
};

#endif // XLISTBOX_H
