// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xcheckbox.cpp

#include "xcheckbox.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xcheckbox::xcheckbox( irr::IrrlichtDevice *dev, int id, bool b, char * c, int x1, int x2, int y1, int y2 ) : xelement( dev, id ){
	// cout << "[xcheckbox" << id <<"] create xcheckbox" << endl;

/* bool   checked,
  const core::rect< s32 > &   rectangle,
  IGUIElement *   parent = 0,
  s32   id = -1,
  const wchar_t *   text = 0
*/

	stream.str("");
	stream.clear();
	swdata  = c;
	wsdata  = swdata.c_str();
	checkbox  = irr_env->addCheckBox( b, rect<int>( x1, x2, y1, y2 ), 0, id, wsdata );
	element_reference = checkbox;

	setType( XELEMENT_TYPE_CHECKBOX );
}

xcheckbox::~xcheckbox(){
	cout << "[xcheckbox" << element_id <<"] delete xcheckbox" << endl;
	// TODO: finish this.
}

int xcheckbox::update(){
	// cout << "[xcheckbox" << element_id <<"] xcheckbox::update" << endl;
	return 1;
}

int xcheckbox::render(){
	// cout << "[xcheckbox" << element_id <<"] xcheckbox::render" << endl;
	return 1;
}

bool xcheckbox::event(){
	// cout << "[xcheckbox" << element_id <<"] xcheckbox::event" << endl;
	return false;
}

void xcheckbox::destroy(){
	// cout << "[xcheckbox" << element_id <<"] xcheckbox::destroy" << endl;
	checkbox->remove();
}

void xcheckbox::setVisible( bool v ){
	// cout << "[xcheckbox" << element_id <<"] setVisible" << endl;
	checkbox->setVisible( v );
} // xcheckbox::setVisible

void xcheckbox::setParent( xelement * x ){
	// cout << "[xcheckbox" << element_id <<"] setParent" << endl;
	parent = x;
	checkbox->setParent( x->getGfxElement() );
} // xcheckbox::setParent

bool xcheckbox::isChecked(){
	// cout << "[xcheckbox" << element_id <<"] xcheckbox::getText" << endl;
	return checkbox->isChecked();
} // xcheckbox::getText

void xcheckbox::setChecked( bool b ){
	// cout << "[xcheckbox" << element_id <<"] xcheckbox::setText" << endl;
	checkbox->setChecked( b );
} // xcheckbox::setText

