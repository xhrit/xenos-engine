// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XSHADER_H
#define XSHADER_H

#include "xelement.h"
#include <irrlicht.h>

#include <iostream>
#include <cstdlib>


class shaderCallBack : public video::IShaderConstantSetCallBack {
	protected:
		irr::IrrlichtDevice       *irr_device;
		irr::video::IVideoDriver  *irr_driver;

	public:
		void init( irr::IrrlichtDevice *dev ){
			cout << "[xshader] init" << endl;
			irr_device = dev;                                    // collect irrlicht device pointer
			irr_driver = irr_device->getVideoDriver();           // collect irrlicht driver pointer
		}

		virtual void OnSetConstants( video::IMaterialRendererServices* services, s32 userData) {

			// set inverted world matrix
			core::matrix4 invWorld = irr_driver->getTransform(video::ETS_WORLD);
			invWorld.makeInverse();

			services->setVertexShaderConstant("mInvWorld", invWorld.pointer(), 16);

			// set clip matrix
			core::matrix4 worldViewProj;
			worldViewProj = irr_driver->getTransform(video::ETS_PROJECTION);
			worldViewProj *= irr_driver->getTransform(video::ETS_VIEW);
			worldViewProj *= irr_driver->getTransform(video::ETS_WORLD);

			services->setVertexShaderConstant("mWorldViewProj", worldViewProj.pointer(), 16);

			// set light and cam position
			core::vector3df pos = irr_device->getSceneManager()->getActiveCamera()->getAbsolutePosition();
			services->setVertexShaderConstant("mCameratPos", reinterpret_cast<f32*>(&pos), 3);

			pos = core::vector3df(0.0f, 0.0f, 0.0f);
			services->setVertexShaderConstant("mLightPos", reinterpret_cast<f32*>(&pos), 3);

			// set light color
			video::SColorf col(1.0f, 1.0f, 1.0f, 1.0f);
			services->setVertexShaderConstant("mLightColor", reinterpret_cast<f32*>(&col), 4);

			// set transposed world matrix
			core::matrix4 world = irr_driver->getTransform(video::ETS_WORLD);
			world = world.getTransposed();

			services->setVertexShaderConstant("mTransWorld", world.pointer(), 16);

			pos = core::vector3df(1.0f, 1.0f, 1.0f);
			services->setVertexShaderConstant("v3InvWavelength",reinterpret_cast<f32*>(&pos), 16);
/*
		pSkyShader->SetUniformParameter1f("fCameraHeight", vCamera.Magnitude());
		pSkyShader->SetUniformParameter1f("fCameraHeight2", vCamera.MagnitudeSquared());
		pSkyShader->SetUniformParameter1f("fInnerRadius", m_fInnerRadius);
		pSkyShader->SetUniformParameter1f("fInnerRadius2", m_fInnerRadius*m_fInnerRadius);
		pSkyShader->SetUniformParameter1f("fOuterRadius", m_fOuterRadius);
		pSkyShader->SetUniformParameter1f("fOuterRadius2", m_fOuterRadius*m_fOuterRadius);
		pSkyShader->SetUniformParameter1f("fKrESun", m_Kr*m_ESun);
		pSkyShader->SetUniformParameter1f("fKmESun", m_Km*m_ESun);
		pSkyShader->SetUniformParameter1f("fKr4PI", m_Kr4PI);
		pSkyShader->SetUniformParameter1f("fKm4PI", m_Km4PI);
		pSkyShader->SetUniformParameter1f("fScale", 1.0f / (m_fOuterRadius - m_fInnerRadius));
		pSkyShader->SetUniformParameter1f("fScaleDepth", m_fRayleighScaleDepth);
		pSkyShader->SetUniformParameter1f("fScaleOverScaleDepth", (1.0f / (m_fOuterRadius - m_fInnerRadius)) / m_fRayleighScaleDepth);
		pSkyShader->SetUniformParameter1f("g", m_g);
		pSkyShader->SetUniformParameter1f("g2", m_g*m_g);
		pSkyShader->SetUniformParameter1i("nSamples", m_nSamples);
		pSkyShader->SetUniformParameter1f("fSamples", m_nSamples);
		m_tOpticalDepth.Enable();
		pSkyShader->SetUniformParameter1f("tex", 0);
*/

		}
};

class xshader {
	protected:
		irr::IrrlichtDevice       *irr_device;    ///< irrlicht device pointer
		irr::video::IVideoDriver  *irr_driver;    ///< irrlicht video driver pointer

	public:
		xshader( irr::IrrlichtDevice *dev );
		~xshader();
		int addMaterial( char * psFile, char * vsFile, int baseMaterial );
};

#endif // XSHADER_H
