// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xbone.cpp

#include "xbone.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xbone::xbone( irr::IrrlichtDevice *dev, int id, scene::IBoneSceneNode* b  ) : xelement( dev, id ){
	// cout << "[xbone] create xbone" << endl;
	bone = b;

	setType( XELEMENT_TYPE_BONE );
	node_reference = bone;
	// cout << "[xbone] /create xbone" << endl;
}

xbone::~xbone(){
	// cout << "[xbone] delete xbone" << endl;
}

int xbone::update(){
	// cout << "[xbone] xbone::update" << endl;
	bone->updateAbsolutePositionOfAllChildren();
	return 1;
}

int xbone::render(){
	// cout << "[xbone] xbone::render" << endl;
	return 1;
} // xbone::render

bool xbone::event(){
	// cout << "[xbone] xbone::event" << endl;
	return false;
} // xbone::event

void xbone::destroy(){
	// cout << "[xbone] xbone::destroy" << endl;
	bone->remove();
} // xbone::destroy

