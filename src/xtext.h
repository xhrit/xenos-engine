// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xtext.h

#ifndef XTEXT_H
#define XTEXT_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xtext : static text
///
/// This is a static text element.
///


class xtext : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		///
		xtext( irr::IrrlichtDevice *dev, int id,  char * c, int x1, int x2, int y1, int y2 );

		///
		/// destructor.
		///
		~xtext();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();
		void setVisible( bool v );
		void setParent( xelement * x );
		char * getText();
		void setText( char * c );
		void setColour( xcolour c );
		void setFont( xfont * f );
		void setAlignment( int h, int v );
		void setPosition( x2dpoint p );

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;      // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		IGUIStaticText * text;
		float w, h;
};

#endif // XTEXT_H
