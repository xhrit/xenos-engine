// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xeditbox.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xeditbox::xeditbox( irr::IrrlichtDevice *dev, int id, x2drect r ) : xelement( dev, id ){
	// cout << "[xeditbox" << id <<"] create xeditbox" << endl;

	editbox = irr_env->addEditBox( L"", rect<s32>(r.X1, r.Y1, r.X2, r.Y2));
	// addEditBox ( "", core::rect<int>(x,y,x+100,y+20), true, m_win_chat, GUI_EB_CHAT_TEXT);

	element_reference = editbox;

	setType( XELEMENT_TYPE_EDITBOX );
}

xeditbox::~xeditbox(){
	// cout << "[xeditbox" << element_id <<"] delete xeditbox" << endl;
}

int xeditbox::update(){
	// cout << "[xeditbox] xeditbox::update" << endl;
	return 1;
}

int xeditbox::render(){
	// cout << "[xeditbox] xeditbox::render" << endl;
	return 1;
}

bool xeditbox::event(){
	// cout << "[xeditbox] xeditbox::event" << endl;
	return false;
}

void xeditbox::destroy(){
	// cout << "[xeditbox] xeditbox::destroy" << endl;
	editbox->remove();
}

char * xeditbox::getText(){
	// cout << "[xeditbox" << element_id <<"] xeditbox::getText" << endl;
	core::stringc s = editbox->getText();
	return strdup( s.c_str() );
} // xeditbox::getText

void xeditbox::setText( char * c ){
	// cout << "[xeditbox" << element_id <<"] xeditbox::setText" << endl;
	swdata  = c;
	wsdata  = swdata.c_str();
	editbox->setText( wsdata );
} // xeditbox::setText

void xeditbox::setVisible( bool v ){
	// cout << "[xeditbox" << element_id <<"] setVisible" << endl;
	editbox->setVisible( v );
} // xeditbox::setVisible

void xeditbox::setParent( xelement * x ){
	parent = x;
	editbox->setParent( x->getGfxElement() );
} // xeditbox::setParent

void xeditbox::setMultiLine( bool b ){
	editbox->setMultiLine( b );
} // xeditbox::setMultiLine

void xeditbox::setPasswordBox( bool b ){
	editbox->setPasswordBox( b );
} // xeditbox::setPasswordBox

void xeditbox::setTextAlignment( int h, int v ){
	editbox->setTextAlignment( (EGUI_ALIGNMENT)h, (EGUI_ALIGNMENT)v );

} // xeditbox::setTextAlignment

void xeditbox::setWordWrap( bool b ){
	editbox->setWordWrap( b );
} // xeditbox::setWordWrap

