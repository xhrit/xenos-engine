// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xbutton.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xbutton::xbutton( irr::IrrlichtDevice *dev, int id, x2drect r, char * c  ) : xelement( dev, id ){
	// cout << "[xbutton" << id <<"] create xbutton" << endl;

	stream.str("");
	stream.clear();
	swdata  = c;
	wsdata  = swdata.c_str();

	button = irr_env->addButton( rect<s32>( r.X1, r.Y1, r.X2, r.Y2 ), 0, id, wsdata);
	element_reference = button;

	w = r.X2 - r.X1;
	h = r.Y2 - r.Y1;
    // cout << "[xbutton] w:" << w << endl;
    // cout << "[xbutton] h:" << h << endl;

	setType( XELEMENT_TYPE_BUTTON );
}

xbutton::~xbutton(){
	// cout << "[xbutton" << element_id <<"] delete xbutton" << endl;
}

int xbutton::update(){
	// cout << "[xbutton] xbutton::update" << endl;
	return 1;
}

int xbutton::render(){
	 // cout << "[xbutton] xbutton::render" << endl;
	return 1;
} // xbutton::render

bool xbutton::event(){
	// cout << "[xbutton" << element_id <<"] xbutton::event" << endl;
	return false;
} // xbutton::event

void xbutton::destroy(){
	// cout << "[xbutton" << element_id <<"] xbutton::destroy" << endl;
	button->remove();
} // xbutton::destroy

void xbutton::setVisible( bool v ){
	// cout << "[xbutton" << element_id <<"] setVisible" << endl;
	button->setVisible( v );
	// cout << "[xbutton" << element_id <<"] /setVisible" << endl;
} // xbutton::setVisible

void xbutton::setParent( xelement * x ){
	parent = x;
	button->setParent( x->getGfxElement() );
} // xbutton::setParent

void xbutton::setImage( xtexture * t ){
	button->setImage ( t->texture );
	button->setUseAlphaChannel(true);
}

void xbutton::setPressedImage( xtexture * t ){
	button->setPressedImage ( t->texture );
	button->setUseAlphaChannel(true);
}

void xbutton::setAlignment( int n1, int n2, int n3, int n4 ){
    button->setAlignment( (EGUI_ALIGNMENT)n1, (EGUI_ALIGNMENT)n2, (EGUI_ALIGNMENT)n3, (EGUI_ALIGNMENT)n4 );
}

void xbutton::setText( char * c ){
	// cout << "[xtext" << element_id <<"] xtext::setText" << endl;
	swdata  = c;
	wsdata  = swdata.c_str();
	button->setText( wsdata );
} // xtext::setText

void xbutton::setDrawBorder( bool b ){
	button->setDrawBorder( b );
}

void xbutton::setPosition( x2dpoint p ){
	// cout << "[xbutton] setPosition" << endl;
	// position = p;
	button->setRelativePosition ( irr::core::rect<int>( p.X, p.Y, p.X+w, p.Y+h ) );
} // xbutton::setPosition
