// zp3 c 2006-2009 xhrit.com
// xaudio

#include "xaudio.h"

// Position of the listener.
ALfloat ListenerPos[] = { 0.0, 0.0, 0.0 };

// Velocity of the listener.
ALfloat ListenerVel[] = { 0.0, 0.0, 0.0 };

// Orientation of the listener. (first 3 elements are "at", second 3 are "up")
ALfloat ListenerOri[] = { 0.0, 0.0, -1.0, 0.0, 1.0, 0.0 };

xaudio::xaudio () {
	// cout << "[xaudio] xaudio" << endl;
	valid = initOpenAL ();
}

xaudio::~xaudio () {
}

bool xaudio::initOpenAL () {
	// cout << "[xaudio] InitOpenAL" << endl;
	maxSources = 16;
	ALCdevice *pDevice = alcOpenDevice (NULL);	// deviceName);
	ALCcontext *pContext = alcCreateContext (pDevice, NULL);

	alcMakeContextCurrent (pContext);

	if (alcGetError (pDevice) != ALC_NO_ERROR){
		// cout << "[xaudio] Error in OpenAL subsystem" << endl;
		return bool (AL_FALSE);
	}

	return bool (AL_TRUE);
} // xaudio::InitOpenAL

bool xaudio::isValid () {
	//cout << "xaudio::isValid" << endl;
	return valid;
} // xaudio::isValid

void xaudio::clear (){
	// cout << "[xaudio] clear" << endl;
	// stop all playing samples?
	// cout << "[xaudio] Sources.size():" << Sources.size() << endl;

	for (int i = 0; i < Sources.size(); i++){
		// cout << "stop source " << i << endl;
		alSourceStop ( Sources[i] );
	}

	// cout << "[xaudio] clear loaded files" << endl;
	LoadedFiles.clear ();

	vector<ALuint>::iterator iter;
	// cout << "[xaudio] Release all buffer data" << endl;
	// Release all buffer data.
	for (iter = Buffers.begin (); iter != Buffers.end (); ++iter)
		alDeleteBuffers (1, (ALuint*)&(*iter));

	// cout << "[xaudio] Release all source data" << endl;
	// Release all source data.
	for (iter = Sources.begin (); iter != Sources.end (); ++iter)
		alDeleteSources (1, (ALuint*)&(*iter));

	// cout << "[xaudio] clear" << endl;
	// Destroy the lists.
	Buffers.clear ();
	Sources.clear ();
} // xaudio::Clear

void xaudio::removeSource ( ALuint Source ){
	alDeleteSources (1, &Source);
} // xaudio::RemoveSource

void xaudio::closeDriver () {
	// cout << "[xaudio] CloseDriver" << endl;
	ALCcontext* pCurContext;
	ALCdevice* pCurDevice;

	// Get the current context.
	pCurContext = alcGetCurrentContext();

	// Get the device used by that context.
	pCurDevice = alcGetContextsDevice(pCurContext);

	// Reset the current context to NULL.
	alcMakeContextCurrent(NULL);

	// Release the context and the device.
	alcDestroyContext(pCurContext);
	alcCloseDevice(pCurDevice);
} // xaudio::CloseDriver

void xaudio::update () {
	// cout << "xaudio::UpdateSources" << endl;
	ALint State;
	for (int i = 0; i < Sources.size(); i++){
		alGetSourcei(Sources[i], AL_SOURCE_STATE, &State);
		if ( State == AL_PLAYING ){
			SourcesActive[i] = true;
		} else {
			SourcesActive[i] = false;
		}
		// cout << "source #" << i << ", id " << Sources[i] << ", state = " << State << endl;
	}
} // xaudio::UpdateSources

int xaudio::getFreeSource () {
	// cout << "[xaudio] GetFreeSource" << endl;
	for (int i = 0; i < Sources.size(); i++){
		if ( SourcesActive[i] == false ){
			// cout << "[xaudio] GetFreeSource (source " << i << ")" << endl;
			return i;
		}
	}
	// cout << "[xaudio] GetFreeSource ( none free )" << endl;
	return -1;
} // xaudio::GetFreeSource

ALuint xaudio::getSource () {
	// cout << "[xaudio] xaudio::GetSource" << endl;
	// check for free source.
	int freesource = getFreeSource ();
	// cout << "[xaudio] free source id :" << freesource << endl;
	// cout << "[xaudio] Sources.size() :" << Sources.size() << endl;
	if (freesource == -1){ // if no source is free,
		if ( Sources.size() < maxSources ) {
			// make a new source if below source max
			ALsource source;
			ALenum result;

			// Generate a source.
			alGenSources (1, &source);

			if ((result = alGetError ()) != AL_NO_ERROR) {
				errStr = getALErrorString (result);
				cout << errStr << endl;
				// return 0;
			}
			// cout << "[xaudio] no free source, make new, id :" << source << endl;
			return source;
		} else {
			// free an old source if above source max.
			// cout << "[xaudio] max sources, none free. returning " <<  Sources[15] << endl;
			// todo, take free source instead ov last source. is this the cause ov our stuttering?
			return Sources[15];
		}
	} else {
		// cout << "[xaudio] returning free source, id" << Sources[freesource] << endl;
		return Sources[freesource];
	}
} // xaudio::GetSource

// ALuint xaudio::LoadOGG(char *fileName, vector<char> &buffer, ALenum &format, ALsizei &freq) {
ALuint xaudio::addOGGSample(char *fileName, float gain) {
	// cout << "[xaudio] addOGGSample " << fileName << endl;
	ALuint buffer;

	vector<char> bufferData;                // The sound buffer data from file
	ALenum format;                          // The sound data format
	ALsizei freq;                           // The frequency of the sound data

	// cout << "[xaudio] Generate buffer" << endl;
	// Generate a buffer. Check that it was created successfully.
	alGenBuffers (1, &buffer);

	// cout << "[xaudio] Get source" << endl;
	ALsource source = getSource();
	// Sources.push_back(source);

	// set up source data
	alSourcef (source, AL_PITCH, 1.0);
	alSourcef (source, AL_GAIN, gain);

	int endian = 0;                         // 0 for Little-Endian, 1 for Big-Endian
	int bitStream;
	long bytes;
	char array[BUFFER_SIZE];                // Local fixed size array
	FILE *f;


	// cout << "[xaudio] Open file for binary reading" << endl;

	// Open for binary reading
	f = fopen(fileName, "rb");

	if (f == NULL) {
		// cerr << "[xaudio] Cannot open " << fileName << " for reading..." << endl;
		exit(-1);
	} // end if

	vorbis_info *pInfo;
	OggVorbis_File oggFile;

	// Try opening the given file
	if (ov_open(f, &oggFile, NULL, 0) != 0) {
		// cerr << "[xaudio] Error opening " << fileName << " for decoding..." << endl;
		exit(-1);
	} // end if

	// cout << "[xaudio] Get some information about the OGG file" << endl;

	// Get some information about the OGG file
	pInfo = ov_info(&oggFile, -1);

	// Check the number of channels... always use 16-bit samples
	if (pInfo->channels == 1) {
		format = AL_FORMAT_MONO16;
	} else {
		format = AL_FORMAT_STEREO16;
	} // end if

	// The frequency of the sampling rate
	freq = pInfo->rate;

	// cout << "[xaudio] Keep reading until all is read" << endl;

	// Keep reading until all is read
	do {
		// Read up to a buffer's worth of decoded sound data
		bytes = ov_read(&oggFile, array, BUFFER_SIZE, endian, 2, 1, &bitStream);

		if (bytes < 0)
		{
		ov_clear(&oggFile);
		// cerr << "[xaudio] Error decoding " << fileName << "..." << endl;
		exit(-1);
		}
		// end if

		// Append to end of buffer
		bufferData.insert(bufferData.end(), array, array + bytes);
	} while (bytes > 0);

	// Clean up!
	ov_clear(&oggFile);

	// cout << "[xaudio] add to free source and buffer" << endl;

	// add to free source and buffer...
	// Set the source and listener to the same location
	// alListener3f(AL_POSITION, 0.0f, 0.0f, 0.0f);
	// alSource3f(sourceID, AL_POSITION, 0.0f, 0.0f, 0.0f);

	// Upload sound data to buffer
	alBufferData(buffer, format, &bufferData[0], static_cast<ALsizei>(bufferData.size()), freq);

	// Attach sound buffer to source
	alSourcei(source, AL_BUFFER, buffer);

	// Save the source id.
	Sources.push_back (source);
	// mark as active.
	SourcesActive.push_back (true);

	return source;
} // xaudio::LoadOGG

ALuint xaudio::addWAVSample ( char *path, bool loop, float gain ) { //
	// cout << "[xaudio] addWAVSample" << endl;
	// cout << "[xaudio] path:" << path << endl;

	ALfloat srcPos[] = { 0.0, 0.0, 0.0 }; // <-- ??????
	ALfloat srcVel[] = { 0.0, 0.0, 0.0 };
	ALsource source;
	ALuint buffer;
	ALenum result;

	// Get the files buffer id (load it if necessary).
	buffer = getLoadedALBuffer (path);

	source = getSource();

	if (gain < 0.0)
		gain = 0.0;
	if (gain > 1.0)
		gain = 1.0;
	gain = 1.0;

	// Setup the source properties.
	alSourcei (source, AL_BUFFER, buffer);
	alSourcef (source, AL_PITCH, 1.0);
	alSourcef (source, AL_GAIN, gain);
	alSourcefv (source, AL_POSITION, srcPos);
	alSourcefv (source, AL_VELOCITY, srcVel);
	alSourcei (source, AL_LOOPING, loop);

	int src = -1;
	for (int i = 0; i< Sources.size(); i++){
		if ( Sources[i] ==  source){
			src = i;
		}
	}

	if (src == -1){
		// Save the source id.
		Sources.push_back (source); // only push back if has not been gotten yet?
		// mark as active.
		SourcesActive.push_back (true);
	} else {
		SourcesActive[src] == true;
	}


	// Return the source id.
	return source;
} // xaudio::addWAVSample

ALuint xaudio::loadALBuffer (char *path) {
	// cout << "[xaudio] Load buffer : " << path << endl;
	// Variables to store data which defines the buffer.
	ALenum format;
	ALsizei size;
	ALvoid *wave;
	ALsizei freq;
	ALboolean loop;

	// Buffer id and error checking variable.
	ALuint buffer;
	ALenum result;

	// Generate a buffer. Check that it was created successfully.
	alGenBuffers (1, &buffer);

	if ((result = alGetError ()) != AL_NO_ERROR) {
		errStr = getALErrorString (result);
		cout << errStr << endl;
		// return 0;
	}

	// cout << "[xaudio] Read in the wav data from file" << endl;

	// Read in the wav data from file. Check that it loaded correctly.
	alutLoadWAVFile ((ALbyte*)path, &format, &wave, &size, &freq, &loop);

	if ((result = alGetError ()) != AL_NO_ERROR) {
		errStr = getALErrorString (result);
		cout << errStr << endl;
		// return 0;
	}

	// cout << "[xaudio] Send the wav data into the buffer" << endl;
	// Send the wav data into the buffer. Check that it was received properly.
	alBufferData (buffer, format, wave, size, freq);

	if ((result = alGetError ()) != AL_NO_ERROR) {
		errStr = getALErrorString (result);
		cout << errStr << endl;
		// return 0;
	}

	// Get rid of the temporary data.
	// cout << "[xaudio] Get rid of the temporary data" << endl;
	alutUnloadWAV (format, wave, size, freq);

	if ((result = alGetError ()) != AL_NO_ERROR) {
		errStr = getALErrorString (result);
		cout << errStr << endl;
		// return 0;
	}

	// cout << "[xaudio] return id " <<  buffer << endl;

	// Return the buffer id.
	return buffer;
} // xaudio::LoadALBuffer

ALuint xaudio::getLoadedALBuffer (char *path) {
	// cout << "[xaudio] GetLoadedALBuffer" << endl;
	int count = 0;		// 'count' will be an index to the buffer list.
	ALuint buffer;		// Buffer id for the loaded buffer.

	// Iterate through each file path in the list.
	for (vector < string >::iterator iter = LoadedFiles.begin ();
		iter != LoadedFiles.end (); ++iter, count++) {

		// If this file path matches one we have loaded already, return the buffer id for it.

		if (*iter == path)
			return Buffers[count];
	}

	// If we have made it this far then this file is new and we will create a buffer for it.
	buffer = loadALBuffer (path);
	//buffer = alutCreateBufferFromFile( path );

	if (buffer) {
		// Add this new buffer to the list, and register that this file has been loaded already.
		Buffers.push_back (buffer);
		LoadedFiles.push_back (path);
	}

	return buffer;
} // xaudio::GetLoadedALBuffer

void xaudio::setSourcePosition(ALuint Source, float x, float y, float z){
	// cout << "xaudio::SetSourcePosition" << endl;
	ALfloat srcPos[] = { 0.0, 0.0, 0.0 };
	srcPos[0] = x;
	srcPos[1] = y;
	srcPos[2] = z;
	alSourcefv(Source, AL_POSITION, srcPos);
} // xaudio::SetSourcePosition

void xaudio::setListenerPosition(float x, float y, float z){
	// cout << "xaudio::SetListenerPosition" << endl;
	ListenerPos[0] = x;
	ListenerPos[1] = y;
	ListenerPos[2] = z;

        // ListenerOri[0] = cam->getTarget().X - cam->getPosition().X;
        // ListenerOri[1] = cam->getTarget().Y - cam->getPosition().Y;
        // ListenerOri[2] = -(cam->getTarget().Z - cam->getPosition().Z);
        // ListenerOri[3] = cam->getUpVector().X;
        // ListenerOri[4] = cam->getUpVector().Y;
        // ListenerOri[5] = -cam->getUpVector().Z;

	alListenerfv(AL_POSITION,    ListenerPos);
	// alListenerfv(AL_VELOCITY,    ListenerVel);
	// alListenerfv(AL_ORIENTATION, ListenerOri);
} // xaudio::SetListenerPosition

string xaudio::getLastErr (){
	return errStr;
} // xaudio::getLastErr

string xaudio::getALErrorString (ALenum err){
	switch (err) {
		case AL_NO_ERROR:
			return string ("AL_NO_ERROR");
		case AL_OUT_OF_MEMORY:
			return string ("AL_OUT_OF_MEMORY");
		case AL_INVALID_NAME:
			return string ("AL_INVALID_NAME");
		case AL_INVALID_ENUM:
			return string ("AL_INVALID_ENUM");
		case AL_INVALID_VALUE:
			return string ("AL_INVALID_VALUE");
		case AL_INVALID_OPERATION:
			return string ("AL_INVALID_OPERATION");
		default:
			return string ("AL_UNKNOWN_ERROR");
	};
} // xaudio::GetALErrorString

string xaudio::getALCErrorString (ALenum err){
	switch (err) {
		case ALC_NO_ERROR:
			return string ("AL_NO_ERROR");
		case ALC_INVALID_DEVICE:
			return string ("ALC_INVALID_DEVICE");
		case ALC_INVALID_CONTEXT:
			return string ("ALC_INVALID_CONTEXT");
		case ALC_INVALID_ENUM:
			return string ("ALC_INVALID_ENUM");
		case ALC_INVALID_VALUE:
			return string ("ALC_INVALID_VALUE");
		case ALC_OUT_OF_MEMORY:
			return string ("ALC_OUT_OF_MEMORY");
		default:
			return string ("ALC_UNKNOWN_ERROR");
		break;
	};
} // xaudio::GetALCErrorString

void xaudio::playSource (ALsource src){
	alSourcePlay (src);
} // xaudio::playSource

void xaudio::stopSource (ALsource src){
	alSourceStop (src);
} // xaudio::stopSource

void xaudio::pauseSource (ALsource src){
	alSourcePause (src);
} // xaudio::pauseSource

void xaudio::rewindSource (ALsource src){
	alSourceRewind (src);
} // xaudio::rewindSource

void xaudio::setSourceGain (ALsource src, float gain){
	alSourcef (src, AL_GAIN, gain);
} // xaudio::setSourceGain

void xaudio::playSourcev (int n, ALsource srcv[]){
	alSourcePlayv (n, srcv);
} // xaudio::playSourcev

void xaudio::stopSourcev (int n, ALsource srcv[]){
	alSourceStopv (n, srcv);
} // xaudio::stopSourcev

void xaudio::pauseSourcev (int n, ALsource srcv[]){
	alSourcePausev (n, srcv);
} // xaudio::pauseSourcev

void xaudio::rewindSourcev (int n, ALsource srcv[]){
	alSourceRewindv (n, srcv);
} // xaudio::rewindSourcev

bool xaudio::isPlaying (ALsource src){
	ALint play;

	alGetSourcei (src, AL_SOURCE_STATE, &play);
	return (play == AL_PLAYING);
} // xaudio::isPlaying

