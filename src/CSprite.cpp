#include "CSprite.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

namespace irr
{
namespace scene
{

CSprite::CSprite(ISceneNode* parent, ISceneManager* mgr, s32 id, char* base, char* arm, char* head , char* wep) : ISceneNode( parent, mgr, id )
{
	// cout << "[CSprite] create CSprite" << endl;
	setSize( core::dimension2d<f32> (10, 10) );

	indices[0] = 0;
	indices[1] = 2;
	indices[2] = 1;
	indices[3] = 0;
	indices[4] = 3;
	indices[5] = 2;

	vertices[0].TCoords.set(1.0f, 1.0f);
	vertices[0].Color = video::SColor(0xFFFFFFFF);

	vertices[1].TCoords.set(1.0f, 0.0f);
	vertices[1].Color = video::SColor(0xFFFFFFFF);

	vertices[2].TCoords.set(0.0f, 0.0f);
	vertices[2].Color = video::SColor(0xFFFFFFFF);

	vertices[3].TCoords.set(0.0f, 1.0f);
	vertices[3].Color = video::SColor(0xFFFFFFFF);

    BBox.reset(vertices[0].Pos);
    for (s32 i=1; i<4; ++i){ BBox.addInternalPoint(vertices[i].Pos); }

    // Setup the materials
    material1.Wireframe = false;
    material1.Lighting = false;
    material1.ZWriteEnable = true;
    material1.setTexture(0, mgr->getVideoDriver( )->getTexture( base ));

    material2.Wireframe = false;
    material2.Lighting = false;
    material2.ZWriteEnable = true;
    material2.setTexture(0, mgr->getVideoDriver( )->getTexture( arm ));

    material3.Wireframe = false;
    material3.Lighting = false;
    material3.ZWriteEnable = true;
    material3.setTexture(0, mgr->getVideoDriver( )->getTexture( head ));

    material4.Wireframe = false;
    material4.Lighting = false;
    material4.ZWriteEnable = true;
    material4.setTexture(0, mgr->getVideoDriver( )->getTexture( wep ));
	// material4.MaterialType = video::EMT_TRANSPARENT_ADD_COLOR;

	pos = core::vector3df(0, 0, 0);
}

CSprite::~CSprite(void)
{

}

//! pre render event
void CSprite::OnRegisterSceneNode(void)
{
    // cout << "[CSprite] OnRegisterSceneNode" << endl;
	if (IsVisible){ SceneManager->registerNodeForRendering(this); }

	ISceneNode::OnRegisterSceneNode();
}

void CSprite::OnAnimate(u32 timeMs)
{
 ISceneNode::OnAnimate(timeMs);
}

//! render
void CSprite::render(void)
{
    // cout << "[CSprite] render" << endl;
	video::IVideoDriver* driver = SceneManager->getVideoDriver();
	ICameraSceneNode* camera = SceneManager->getActiveCamera();

	if (!camera || !driver)
		return;

	// make billboard look to camera

	core::vector3df pos = getAbsolutePosition();

    // cout << "[CSprite] pos :" << pos.X << ", " << pos.Y << ", " << pos.Z << ", " << endl;


	core::vector3df campos = camera->getAbsolutePosition();
	core::vector3df target = camera->getTarget();
	core::vector3df up = camera->getUpVector();
	core::vector3df view = target - campos;
	view.normalize();

	core::vector3df horizontal = up.crossProduct(view);
	if ( horizontal.getLength() == 0 )
	{
		horizontal.set(up.Y,up.X,up.Z);
	}
	horizontal.normalize();
	horizontal *= 0.5f * Size.Width;

	core::vector3df vertical = horizontal.crossProduct(view);
	vertical.normalize();
	vertical *= 0.5f * Size.Height;

	view *= -1.0f;

	for (s32 i=0; i<4; ++i)
		vertices[i].Normal = view;

	vertices[0].Pos = pos + horizontal + vertical;
	vertices[1].Pos = pos + horizontal - vertical;
	vertices[2].Pos = pos - horizontal - vertical;
	vertices[3].Pos = pos - horizontal + vertical;

	// draw

	if ( DebugDataVisible & scene::EDS_BBOX )
	{
		driver->setTransform(video::ETS_WORLD, AbsoluteTransformation);
		video::SMaterial m;
		m.Lighting = false;
		driver->setMaterial(m);
		driver->draw3DBox(BBox, video::SColor(0,208,195,152));
	}

	driver->setTransform(video::ETS_WORLD, core::IdentityMatrix);

	driver->setMaterial(material1); // draw base
	driver->drawIndexedTriangleList(vertices, 4, indices, 2);

	driver->setMaterial(material2); // draw head
	driver->drawIndexedTriangleList(vertices, 4, indices, 2);

	driver->setMaterial(material3); // draw armor
	driver->drawIndexedTriangleList(vertices, 4, indices, 2);

	driver->setMaterial(material4); // draw weapon
	driver->drawIndexedTriangleList(vertices, 4, indices, 2);
}

//! returns the axis aligned bounding box of this node
const core::aabbox3d<f32>& CSprite::getBoundingBox() const
{
	return BBox;
}

//! sets the size of the billboard
void CSprite::setSize(const core::dimension2d<f32>& size)
{
	Size = size;

	if (Size.Width == 0.0f)
		Size.Width = 1.0f;

	if (Size.Height == 0.0f )
		Size.Height = 1.0f;

	f32 avg = (size.Width + size.Height)/6;
	BBox.MinEdge.set(-avg,-avg,-avg);
	BBox.MaxEdge.set(avg,avg,avg);
}

video::SMaterial& CSprite::getMaterial(u32 i)
{
    if (i == 0){
        return material1;
    } else if (i == 1){
        return material2;
    } else if (i == 2){
        return material3;
    } else if (i == 3){
        return material4;
    }
}

void CSprite::setTexture( char* tex, int layer ){
    if (layer == 1 ){
        material1.setTexture(0, SceneManager->getVideoDriver( )->getTexture( tex ));
    } else if (layer == 2 ){
        material2.setTexture(0, SceneManager->getVideoDriver( )->getTexture( tex ));
    } else if (layer == 3 ){
        material3.setTexture(0, SceneManager->getVideoDriver( )->getTexture( tex ));
    } else if (layer == 4 ){
        material4.setTexture(0, SceneManager->getVideoDriver( )->getTexture( tex ));
    }


}

void CSprite::setPosition( core::vector3df p ){
	//now set the beam
    RelativeTranslation = p;
	updateAbsolutePosition();

} // xbeam::setLine

core::vector3df CSprite::getPosition(){
	//now set the beam
	return pos;
} // xbeam::setLine

//! returns amount of materials used by this scene node.
u32 CSprite::getMaterialCount() const
{
	return 4;
}

//! Set the color of all vertices of the billboard
//! \param overallColor: the color to set
void CSprite::setColor(const video::SColor & overallColor)
{
	for(u32 vertex = 0; vertex < 4; ++vertex)
		vertices[vertex].Color = overallColor;
}

} // end namespace scene
} // end namespace irr
