// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XIMAGE_H
#define XIMAGE_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// ximage : this is a gui button.
///
/// Press away guys!
///

class ximage : public xelement {
	public:

		///
		/// constructor.
		///
		ximage( irr::IrrlichtDevice *dev, int id, xtexture * t, x2dpoint p, x2drect r, xcolour c );

		///
		/// destructor.
		///
		~ximage();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		void setColour( xcolour c );
		void setPosition( x2dpoint p );
		void setParent( xelement * x );
		void setVisible( bool v );
		void setRect( x2drect r );
		void setImage ( xtexture * t );
		void setSize( int w, int h );

	private:

		x2dpoint position;
		xcolour colour;
		x2drect rect;
		xtexture * tex;
		IGUIImage * image;
};

#endif // XIMAGE_H
