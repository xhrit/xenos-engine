// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xcore.cpp

#include "xcore.h"      // Core simulation system
#include "xbindings.h"      // lua bindings

xcore::xcore(){
}

xcore::~xcore(){
	cout << "[xcore] ~xcore" << endl;
#if IRR_PROFILE
	gPROFILER.PrintAll();
#endif // IRR_PROFILE
}

// initalize the irrlicht device
bool xcore::init( char * m ){
	cout << "[xcore] initializing module" << endl;
	module = m;

	std::stringstream stream;
	stream << "modules/" << module << "/" << module << ".lua";
	path = stream.str();
	// cout << "path:" << path << endl;

	// Low Level Design Stub
	// you can invoke the simulation directly here
	//

	pLuaState = lua_open();                              // Load the Lua libraries
	luaopen_base( pLuaState );
	luaopen_xcore( pLuaState );
	luaL_openlibs(pLuaState);

	int args, results, err;

	err = luaL_loadfile( pLuaState, path.c_str() );
	if ( err != 0 ){ cout << "[xcore] error loading script. " <<  lua_tostring(pLuaState, -1) << endl; }

	err = lua_pcall( pLuaState, 0, 0, 0 );
	if ( err != 0 ){ cout << "[xcore] error loading functions. " <<  lua_tostring(pLuaState, -1) << endl; }


	lua_getglobal(pLuaState, "init");	// function to be called
	err = lua_pcall( pLuaState, 0, 0, 0 );	// error handeling.
	if ( err != 0 ){
		cout << "[xcore] error running init script. " << lua_tostring(pLuaState, -1) << endl;
	}


	core_timer = new xtimer();
	step = 0.1; //666f;
	accumulator = 0.0f;

#if IRR_PROFILE
	cout << "[xcore] using profiler. " << endl;
	gPROFILER.SetTimer( core_timer ); // wtf should i do w/out access to a device?!

	// Each group has only to be added once. Adding it again will overwrite the old group.
	// groupId_: a custom group id - 0 is reserved but can be used nevertheless
	// name_: will be used in display
	PROFILE_ADD_GROUP(1, "Total");

	// Each block has only to be added once. Adding it again will overwrite the old block.
	// id: any custom id which you have to use in PROFILE_START and PROFILE_STOP
	// groupId_: to put the this profile data in a specific group (for better layout)
	// name_: used in display
	PROFILE_ADD(3, 1, "Update");
	PROFILE_ADD(4, 1, "Render");
	PROFILE_ADD(5, 1, "OnEvent"); // wtf? where does this code go?
#endif // IRR_PROFILE
	if ( err == 0 ){
		cout << "[xcore] module initialized, operational status nominal." << endl;
	} else {
		cout << "[xcore] an error has occured." << endl;
	}
	return true;
}

//engine check, returns false when shutting down
bool xcore::update(){
	// cout << "[xcore] update" << endl;

	core_timer->update();
	float delta = core_timer->deltaTime;
	delta += accumulator;
	if ( delta > step*5 ) { delta = step*5; }
	if ( delta < step/5 ) { delta = step/5; }

	// cout << "[xcore] delta:" << delta << endl;
	// cout << "[xcore] step:" << step << endl;
	for(; delta >= step; delta -= step){
		// update code here
		#if IRR_PROFILE
			PROFILE_START( 3 );
		#endif // IRR_PROFILE

		int args, results, err;
		lua_getglobal(pLuaState, "update");    // Use Lua to run a script
		err = lua_pcall( pLuaState, 0, 0, 0 );
		if ( err != 0 ){
			cout << "[xcore] Error in " << lua_tostring(pLuaState, -1) << endl;
			exit(1);
		}

		#if IRR_PROFILE
			PROFILE_STOP( 3 );
		#endif // IRR_PROFILE
	}
	accumulator = delta;
	// cout << "[xcore] accumulator:" << accumulator << endl;
    // cout << "[xcore] /update" << endl;
	return true;
}

// renders the scene according to the state
void xcore::render(){
	// cout << "[xcore] render" << endl;

#if IRR_PROFILE
	PROFILE_START( 4 );
#endif // IRR_PROFILE

	int args, results, err;
	lua_getglobal(pLuaState, "render");  // Use Lua to run a script
	err = lua_pcall( pLuaState, 0, 0, 0 );
	if ( err != 0 ){
		// cout << "[xcore] error running render script. " << lua_tostring(pLuaState, -1) << endl;
	}
	// int fps = m_driver->getFPS();  // add fps controll?
	// if (lastFPS != fps) {
	// }

#if IRR_PROFILE
	PROFILE_STOP( 4 );
#endif // IRR_PROFILE
	// cout << "[xcore] /render" << endl;
}

void xcore::destroy() {
	// cout << "[xcore] xcore destroy" << endl;
	// call the destroy script?
	lua_close( pLuaState );   // cleanup Lua
	#if IRR_PROFILE
	gPROFILER.PrintAll();
	#endif // IRR_PROFILE
}
