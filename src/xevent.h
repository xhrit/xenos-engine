// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xevent.h

#ifndef XEVENT_H
#define XEVENT_H

#include <irrlicht.h>       // 3d/Graphics

class xelement;

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

/// XEVENT_TYPE
///
/// A more detailed description.
///
enum XEVENT_TYPE{
	XGUI_EVENT,
	XMOUSE_EVENT,
	XKEY_EVENT,
	XJOYSTICK_EVENT,
	XLOG_EVENT
};

/// XKEY_EVENT_TYPE
///
/// A more detailed description.
///
enum XKEY_EVENT_TYPE{
	XKEY_LBUTTON          = 0x01,
	XKEY_RBUTTON          = 0x02,
	XKEY_CANCEL           = 0x03,
	XKEY_MBUTTON          = 0x04,
	XKEY_XBUTTON1         = 0x05,
	XKEY_XBUTTON2         = 0x06,
	XKEY_BACK             = 0x08,
	XKEY_TAB              = 0x09,
	XKEY_CLEAR            = 0x0C,
	XKEY_RETURN           = 0x0D,
	XKEY_SHIFT            = 0x10,
	XKEY_CONTROL          = 0x11,
	XKEY_MENU             = 0x12,
	XKEY_PAUSE            = 0x13,
	XKEY_CAPITAL          = 0x14,
	XKEY_KANA             = 0x15,
	XKEY_HANGUEL          = 0x15,
	XKEY_HANGUL           = 0x15,
	XKEY_JUNJA            = 0x17,
	XKEY_FINAL            = 0x18,
	XKEY_HANJA            = 0x19,
	XKEY_KANJI            = 0x19,
	XKEY_ESCAPE           = 0x1B,
	XKEY_CONVERT          = 0x1C,
	XKEY_NONCONVERT       = 0x1D,
	XKEY_ACCEPT           = 0x1E,
	XKEY_MODECHANGE       = 0x1F,
	XKEY_SPACE            = 0x20,
	XKEY_PRIOR            = 0x21,
	XKEY_NEXT             = 0x22,
	XKEY_END              = 0x23,
	XKEY_HOME             = 0x24,
	XKEY_LEFT             = 0x25,
	XKEY_UP               = 0x26,
	XKEY_RIGHT            = 0x27,
	XKEY_DOWN             = 0x28,
	XKEY_SELECT           = 0x29,
	XKEY_PRINT            = 0x2A,
	XKEY_EXECUT           = 0x2B,
	XKEY_SNAPSHOT         = 0x2C,
	XKEY_INSERT           = 0x2D,
	XKEY_DELETE           = 0x2E,
	XKEY_HELP             = 0x2F,
	XKEY_0            = 0x30,
	XKEY_1            = 0x31,
	XKEY_2            = 0x32,
	XKEY_3            = 0x33,
	XKEY_4            = 0x34,
	XKEY_5            = 0x35,
	XKEY_6            = 0x36,
	XKEY_7            = 0x37,
	XKEY_8            = 0x38,
	XKEY_9            = 0x39,
	XKEY_A            = 0x41,
	XKEY_B            = 0x42,
	XKEY_C            = 0x43,
	XKEY_D            = 0x44,
	XKEY_E            = 0x45,
	XKEY_F            = 0x46,
	XKEY_G            = 0x47,
	XKEY_H            = 0x48,
	XKEY_I            = 0x49,
	XKEY_J            = 0x4A,
	XKEY_K            = 0x4B,
	XKEY_L            = 0x4C,
	XKEY_M            = 0x4D,
	XKEY_N            = 0x4E,
	XKEY_O            = 0x4F,
	XKEY_P            = 0x50,
	XKEY_Q            = 0x51,
	XKEY_R            = 0x52,
	XKEY_S            = 0x53,
	XKEY_T            = 0x54,
	XKEY_U            = 0x55,
	XKEY_V            = 0x56,
	XKEY_W            = 0x57,
	XKEY_X            = 0x58,
	XKEY_Y            = 0x59,
	XKEY_Z            = 0x5A,
	XKEY_LWIN             = 0x5B,
	XKEY_RWIN             = 0x5C,
	XKEY_APPS             = 0x5D,
	XKEY_SLEEP            = 0x5F,
	XKEY_NUMPAD0          = 0x60,
	XKEY_NUMPAD1          = 0x61,
	XKEY_NUMPAD2          = 0x62,
	XKEY_NUMPAD3          = 0x63,
	XKEY_NUMPAD4          = 0x64,
	XKEY_NUMPAD5          = 0x65,
	XKEY_NUMPAD6          = 0x66,
	XKEY_NUMPAD7          = 0x67,
	XKEY_NUMPAD8          = 0x68,
	XKEY_NUMPAD9          = 0x69,
	XKEY_MULTIPLY         = 0x6A,
	XKEY_ADD              = 0x6B,
	XKEY_SEPARATOR        = 0x6C,
	XKEY_SUBTRACT         = 0x6D,
	XKEY_DECIMAL          = 0x6E,
	XKEY_DIVIDE           = 0x6F,
	XKEY_F1               = 0x70,
	XKEY_F2               = 0x71,
	XKEY_F3               = 0x72,
	XKEY_F4               = 0x73,
	XKEY_F5               = 0x74,
	XKEY_F6               = 0x75,
	XKEY_F7               = 0x76,
	XKEY_F8               = 0x77,
	XKEY_F9               = 0x78,
	XKEY_F10              = 0x79,
	XKEY_F11              = 0x7A,
	XKEY_F12              = 0x7B,
	XKEY_F13              = 0x7C,
	XKEY_F14              = 0x7D,
	XKEY_F15              = 0x7E,
	XKEY_F16              = 0x7F,
	XKEY_F17              = 0x80,
	XKEY_F18              = 0x81,
	XKEY_F19              = 0x82,
	XKEY_F20              = 0x83,
	XKEY_F21              = 0x84,
	XKEY_F22              = 0x85,
	XKEY_F23              = 0x86,
	XKEY_F24              = 0x87,
	XKEY_NUMLOCK          = 0x90,
	XKEY_SCROLL           = 0x91,
	XKEY_LSHIFT           = 0xA0,
	XKEY_RSHIFT           = 0xA1,
	XKEY_LCONTROL         = 0xA2,
	XKEY_RCONTROL         = 0xA3,
	XKEY_LMENU            = 0xA4,
	XKEY_RMENU            = 0xA5,
	XKEY_PLUS             = 0xBB,
	XKEY_COMMA            = 0xBC,
	XKEY_MINUS            = 0xBD,
	XKEY_PERIOD           = 0xBE,
	XKEY_ATTN             = 0xF6,
	XKEY_CRSEL            = 0xF7,
	XKEY_EXSEL            = 0xF8,
	XKEY_EREOF            = 0xF9,
	XKEY_PLAY             = 0xFA,
	XKEY_ZOOM             = 0xFB,
	XKEY_PA1              = 0xFD,
	XKEY_OEM_CLEAR        = 0xFE,
	XKEY_XKEY_CODES_COUNT  = 0xFF
};

/// XGUI_EVENT_TYPE
///
/// A more detailed description.
///

enum XGUI_EVENT_TYPE{
    XGUI_ELEMENT_FOCUS_LOST = 0,
    XGUI_ELEMENT_FOCUSED,
    XGUI_ELEMENT_HOVERED,
    XGUI_ELEMENT_LEFT,
    XGUI_ELEMENT_CLOSED,
    XGUI_BUTTON_CLICKED,
    XGUI_SCROLL_BAR_CHANGED,
    XGUI_CHECKBOX_CHANGED,
    XGUI_LISTBOX_CHANGED,
    XGUI_LISTBOX_SELECTED_AGAIN,
    XGUI_FILE_SELECTED,
    XGUI_DIRECTORY_SELECTED,
    XGUI_FILE_CHOOSE_DIALOG_CANCELLED,
    XGUI_MESSAGEBOX_YES,
    XGUI_MESSAGEBOX_NO,
    XGUI_MESSAGEBOX_OK,
    XGUI_MESSAGEBOX_CANCEL,
    XGUI_EDITBOX_ENTER,
    XGUI_EDITBOX_CHANGED,
    XGUI_EDITBOX_MARKING_CHANGED,
    XGUI_TAB_CHANGED,
    XGUI_MENU_ITEM_SELECTED,
    XGUI_COMBO_BOX_CHANGED,
    XGUI_SPINBOX_CHANGED,
    XGUI_TABLE_CHANGED,
    XGUI_TABLE_HEADER_CHANGED,
    XGUI_TABLE_SELECTED_AGAIN,
    XGUI_TREEVIEW_NODE_DESELECT,
    XGUI_TREEVIEW_NODE_SELECT,
    XGUI_TREEVIEW_NODE_EXPAND,
    XGUI_TREEVIEW_NODE_COLLAPS,
    XGUI_COUNT
};

/// XLOG_EVENT_TYPE
///
/// A more detailed description.
///

enum XLOG_EVENT_TYPE{
	XLOG_INFORMATION,
	XLOG_WARNING,
	XLOG_ERROR,
	XLOG_NONE
};

/// XMOUSE_EVENT_TYPE
///
/// A more detailed description.
///
enum XMOUSE_EVENT_TYPE{
	XMOUSE_L_PRESSED_DOWN,
	XMOUSE_R_PRESSED_DOWN,
	XMOUSE_M_PRESSED_DOWN,
	XMOUSE_L_LEFT_UP,
	XMOUSE_R_LEFT_UP,
	XMOUSE_M_LEFT_UP,
	XMOUSE_MOVED,
	XMOUSE_WHEEL
};

///
/// xevent : event
///
/// This is an event object.
///
class xevent {
	public:

		///
		/// constructor.
		///
		xevent();

		///
		/// destructor.
		///
		~xevent();

		XEVENT_TYPE getEventType            () { return EventType; };

		XKEY_EVENT_TYPE getKeyEventType     () { return KeyEventType; };
		bool isKeyPressedDown               () { return PressedDown; };
		bool isKeyShift                     () { return Shift; };
		bool isKeyControl                   () { return Control; };

		XGUI_EVENT_TYPE getGUIEventType        () { return GUIEventType; };
		xelement* getCaller                 () { return Caller; };

		XLOG_EVENT_TYPE getLogEventType     () { return LogEventType; };

		XMOUSE_EVENT_TYPE getMouseEventType () { return MouseEventType; };
		float getMouseX                     () { return X; };
		float getMouseY                     () { return Y; };
		float getMouseWheel                 () { return Wheel; };

		void setKeyEventType ( EKEY_CODE key ); // translate irrkey to xkey
		void setGUIEventType ( EGUI_EVENT_TYPE gui ); // translate
		void setMouseEventType ( EMOUSE_INPUT_EVENT mouse ); // translate

	// private:
		XEVENT_TYPE EventType; // the type ov event

		bool   Shift; // true if shift was also pressed
		bool   Control; // true if ctrl was also pressed
		bool   PressedDown; // if not pressed, then the key was left up
		XKEY_EVENT_TYPE   KeyEventType; // Key which has been pressed or released.

		// GUI event
		XGUI_EVENT_TYPE   GUIEventType; // Type of GUI Event.
		xelement * Caller; // who called the event.

		// log event
		XLOG_EVENT_TYPE LogEventType; // the log level
		const char * Text;  // pointer to text which has been logged

		// mouse event
		XMOUSE_EVENT_TYPE MouseEventType; // type of mouse event
		float   Wheel; // mouse wheel delta, usually 1.0 or -1.0.
		float   X; // X position of mouse cursor.
		float   Y; //Y position of mouse cursor.

		float getAxis( int i ){ return Axis[ i ]; }

		// joystick event
		float   Axis [6];	// the axes.
		int   ButtonStates;	// Bitflag button states
		int   Joystick;		// ID

};

#endif // XEVENT_H
