// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "ximage.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

ximage::ximage( irr::IrrlichtDevice *dev, int id, xtexture * t, x2dpoint p, x2drect r, xcolour c ) : xelement( dev, id ){
	// cout << "[ximage] create ximage" << endl;
	image = irr_env->addImage ( t->texture, core::position2d< s32 >(p.X, p.Y) );
	image->setColor( c.getSColour() );

	tex = t;
	position = p;
	rect = r;
	colour = c;
	visible = true;
	setType( XELEMENT_TYPE_IMAGE );
}

ximage::~ximage(){
	// cout << "[ximage] delete ximage" << endl;
}

int ximage::update(){
	// cout << "[ximage] ximage::update" << endl;
	return 1;
}

int ximage::render(){
	// cout << "[ximage] ximage::render" << endl;
	// if ( visible == true ){
	//	// irr_driver draw image using irrdriver
	//	irr_driver->draw2DImage( tex->texture, core::position2d<s32>( position.X, position.Y ),
	//		core::rect< s32 >(rect.X1, rect.Y1, rect.X2, rect.Y2 ),
	//		0, video::SColor( colour.A, colour.R, colour.G, colour.B ), true);
	//}
	return 1;
}

bool ximage::event(){
	// cout << "[ximage] ximage::event" << endl;
	return false;
}

void ximage::destroy(){
	// cout << "[ximage] ximage::destroy" << endl;
	image->remove();
	setType( XELEMENT_TYPE_NULL );
}

void ximage::setColour( xcolour c ){
	colour = c;
	image->setColor( c.getSColour() );
}

void ximage::setPosition( x2dpoint p ){
	position = p;
	// image->setRelativePosition( rect<s32>(p.X, p.Y, p.X+100, p.Y+100 ) );
	image->setRelativePosition( core::position2di(p.X, p.Y)  );

}

void ximage::setRect( x2drect r ){
	rect = r;
}

void ximage::setSize( int w, int h ){
	image->setMinSize( core::dimension2du(w, h) );
	image->setMaxSize( core::dimension2du(w, h) );
} // xwindow::setSize

void ximage::setVisible( bool v ){
	// cout << "[ximage] ximage::setVisible" << endl;
	visible = v;
	image->setVisible( v );
}

void ximage::setImage ( xtexture * t ){
	// cout << "[ximage] ximage::setImage" << endl;
	tex = t;
	image->setImage( t->texture );
}

void ximage::setParent( xelement * x ){
	// cout << "[xtext" << element_id <<"] setParent" << endl;
	parent = x;
	image->setParent( x->getGfxElement() );
} // xtext::setParent
