// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xbillboard.cpp

#include "xbillboard.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xbillboard::xbillboard( irr::IrrlichtDevice *dev, int id, x2dpoint p ) : xelement( dev, id ){
	// cout << "[xbillboard] create xbillboard" << endl;
	node = irr_smgr->addBillboardSceneNode(0, dimension2d< float >(p.X, p.Y), vector3df(0,0,0), id );
	node->setID( id );
	// node->setIsDebugObject( true );
	// node->setDebugDataVisible ( EDS_BBOX );

	setPosition( x3dpoint( 0, 0, 0 ) );
	setType( XELEMENT_TYPE_BILLBOARD );
	node_reference = node;
}

xbillboard::~xbillboard(){
	// cout << "[xbillboard] delete xbillboard" << endl;
}

int xbillboard::update(){
	// cout << "[xbillboard] xbillboard::update" << endl;
	return 1;
}

int xbillboard::render(){
	// cout << "[xbillboard] xbillboard::render" << endl;
	return 1;
} // xbillboard::render

bool xbillboard::event(){
	// cout << "[xbillboard] xbillboard::event" << endl;
	return false;
} // xbillboard::event

void xbillboard::destroy(){
	// cout << "[xbillboard] xbillboard::destroy" << endl;
	node->remove();
} // xbillboard::destroy

x3dpoint xbillboard::getPosition(){
	vector3df p = node->getPosition();
	return x3dpoint ( p.X, p.Y, p.Z );
}

x3dpoint xbillboard::getRotation(){
	vector3df p = node->getRotation();
	return x3dpoint ( p.X, p.Y, p.Z );
}

x3dpoint xbillboard::getScale(){
	vector3df p = node->getScale();
	return x3dpoint ( p.X, p.Y, p.Z );
}

void xbillboard::setPosition( x3dpoint p){
	// cout << "[xbillboard] setPosition" << endl;
	position = p;
	node->setPosition ( p.getv3df() );
} // xbillboard::setPosition

void xbillboard::setRotation( x3dpoint p){
	// cout << "[xbillboard] setRotation" << endl;
	rotation = p;
	node->setRotation ( p.getv3df() );
} // xbillboard::setRotation

void xbillboard::setScale(x3dpoint p) {
	scale = p;
	node->setScale ( p.getv3df() );
} // xbillboard::setScale

void xbillboard::setVisible( bool val ){
	node->setVisible ( val );
} // xbillboard::setVisible

void xbillboard::setMaterialFlag( XMATERIAL_FLAG mf, bool val ){
	node->setMaterialFlag ( (E_MATERIAL_FLAG)mf, val );

} // xbillboard::setMaterialFlag

// TODO: fixme? do something about this :
// node->getMaterial(0).AmbientColor = ambient.getSColour();
// node->getMaterial(0).DiffuseColor = diffuse.getSColour();
// node->getMaterial(0).SpecularColor = specular.getSColour();

void xbillboard::setMaterialType( XMATERIAL_TYPE mt ){ //
	// cout << "[xbillboard] xbillboard::setMaterialType" << endl;
	video::IMaterialRenderer* renderer = irr_driver->getMaterialRenderer( (E_MATERIAL_TYPE)mt );
	// display some problem text when problem
	if (!renderer || renderer->getRenderCapability() != 0){
		// cout << "[xbillboard] unable to render effect: " << mt << endl;;
	} else {
		node->setMaterialType( (E_MATERIAL_TYPE)mt );
	}

} // xbillboard::setMaterialType

void xbillboard::addTexture( char * fn, int index ){
	// cout << "[xbillboard] addTexture: " << tex_fn << endl;
	node->setMaterialTexture( index, irr_driver->getTexture( fn ) );
} // xbillboard::addTexture

void xbillboard::setTextureMatrix( xmatrix4 m ){
	// cout << "[xbillboard] setTextureMatrix" << endl;
    node->getMaterial(0).setTextureMatrix(0, m.m);
} // xbillboard::setTextureMatrix
