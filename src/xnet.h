// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XNET_H
#define XNET_H

#include <irrNet.h>
#include <vector>
#include <iostream>

using namespace irr;
using namespace std;

enum XOP_TYPE {
    XOP_CONN,
    XOP_DISCO,
    XOP_NICK,
    XOP_AUTH,
    XOP_ALLOW,
    XOP_DENY,
    XOP_VERSION,
    XOP_BADVER,
    XOP_VALID,
    XOP_DATA,
    XOP_ADD,
    XOP_DEL,
    XOP_POS,
    XOP_ROT,
    XOP_STAT,
    XOP_ACT,
    XOP_ZERO,
    XOP_CLEAR,
    XOP_ZONE,
    XOP_JOIN,
    XOP_PART,
    XOP_UPDATE,
    XOP_MSG,
    XOP_MSGTO,
    XOP_BOT,
    XOP_FREQ,
    XOP_TRANS,
    XOP_EQUIP,
    XOP_MANIFEST,
    XOP_KILL,
    XOP_SPAWN,
    XOP_GIVE,
    XOP_KICK,
    XOP_BAN,
    XOP_MODE,
    XOP_END,
    XOP_DBG
};

///
/// xpacket
///
/// This is a data packet.
///

class xpacket {
	public:
		xpacket(){};
		~xpacket(){};
		char * getMessage(){ return message; };
		int getID(){ return uid; };
		int getType(){
			// cout << "[xpacket] getType: " << ptype << endl;
			return ptype;
		};

		void setMessage( char * m){ message = m; };
		void setID( int i ){ uid = i; };
		void setType( int i ){
			// cout << "[xpacket] setType: " << i << endl;
			ptype = i;
		};
	private:
		char * message;		// packet data
		int uid;		// user who created this packet
		int ptype;		// packet type id
};

class ServerNetCallback : public net::INetCallback {
	public:
		ServerNetCallback(net::INetManager* netManagerIn) : netManager(netManagerIn) {}

		// Override the "onConnect" function, don't forget to get the method
		// signature correct. Whenever a fresh client connects, their brand
		// spanking new player id will be passed to this function. A "u16"
		// is a typedef for an unsigned short, incase you were wondering.
		virtual void onConnect(const u16 playerId);

		// Similar to the onConnect function, except it happens when a
		// player disconnects. When this happens we will just report
		// which player has disconnected.
		virtual void onDisconnect(const u16 playerId);

		// Handle the packets, as usual.
		virtual void handlePacket(net::SInPacket& packet);

		xpacket * get();
		int getSize();

	private:
		core::array<u32> banList;	// use std::vector
		net::INetManager* netManager;	// A pointer to the INetManager.

		// save messages in que to be fetched from scripting
		std::vector< xpacket * > message_que;
};

// The client callback.
class ClientNetCallback : public net::INetCallback {
	public:
		// Our handlePacket function.
		virtual void handlePacket(net::SInPacket& packet);
		xpacket * get();
		int getSize();
	private:
		// save messages in que to be fetched from scripting
		std::vector< xpacket * > message_que;
};

///
/// xnet : static text
///
/// This is the network interface
///

class xnet{
	public:
		xnet();
		~xnet();

		///
		/// create server
		///
		void server( int p );

		///
		/// create client
		///
		void client( char * ip, int p );

		///
		/// update network engine
		///
		int update();

		///
		/// Send packet to all users, or server
		///
		void send( int t, char * m ); // type, message

		///
		/// Send packet to specific user (server only)
		///
		void sendTo( int t, char * m, int uid ); // type, message

		///
		/// get
		/// @return oldest packet
		///
		xpacket * get();

		///
		/// debugging functions
		///
		int getPing();
        int getPacketsSent();
        int getPacketsReceived();
        void setVerbose( bool b );

	private:
		net::INetManager* netManager;
		ServerNetCallback* serverCallback;
		ClientNetCallback* clientCallback;
		int net_state; // 0 = nothing, 1 = connected client, 2 = listen server
		int	packets_sent;
		int	packets_received;

};

#endif // XNET_H
