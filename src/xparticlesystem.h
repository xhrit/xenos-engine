// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xparticlesystem.h

#ifndef XPARTICLESYSTEM_H
#define XPARTICLESYSTEM_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xparticlesystem
///
///

class xparticlesystem : public xelement {
	public:

		///
		/// constructor.
		///
		xparticlesystem( irr::IrrlichtDevice *dev, int id, char * tex_fn  );

		///
		/// destructor.
		///
		~xparticlesystem();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint p );
		void setScale( x3dpoint p );

		void setMaterialFlag( XMATERIAL_TYPE mt, bool val );
		void setMaterialType( XMATERIAL_TYPE mt );
		void addTexture( char * fn, int index );

		x3dpoint getPosition(){ return position; };
		x3dpoint getRotation(){ return rotation; };
		x3dpoint getScale(){ return scale; };

		void addBoxEmitter( x3dpoint direction, int minpps, int maxpps, xcolour mincolour, xcolour maxcolour,
			int minlife, int maxlife, float angle, float minsize, float maxsize );
		void addRingEmitter(x3dpoint center, float radius, float thickness, x3dpoint direction, int minpps,
			int maxpps, xcolour mincolour, xcolour maxcolour, int minlife, int maxlife, float angle, float minsize, float maxsize);
		void addSphereEmitter(x3dpoint center, float radius, x3dpoint direction, int minpps, int maxpps,
			xcolour mincolour, xcolour maxcolour, int minlife, int maxlife, float angle, float minsize, float maxsize);
		void createPointEmitter( x3dpoint direction, int minpps, int maxpps, xcolour mincolour, xcolour maxcolour,
			int minlife, int maxlife, float angle, float minsize, float maxsize );

		void addFadeOutAffector(xcolour colour, float time);
		void addGravityAffector( x3dpoint gravity, int force );
		void addRotationAffector( x3dpoint speed, x3dpoint pivot );
		void addAttractionAffector( x3dpoint point, float speed, bool attract );
        void addScaleAffector( x2dpoint scaleTo );

		IParticleEmitter* emitter;
		IParticleSystemSceneNode* node;

		void setMinParticlesPerSecond( int n );
		void setMaxParticlesPerSecond( int n );


	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;      // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		x3dpoint scale;
};

#endif // XPARTICLESYSTEM_H
