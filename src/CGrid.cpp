/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						**
 *********************************************************************************/

#include "CGrid.h"

CGrid::force::force(irr::core::vector2df pos, irr::f32 size, irr::f32 radius, irr::f32 time) : Pos(pos), Size(size), Radius(radius*radius), Time(time)
{
}

void CGrid::force::affect(irr::core::vector2d<irr::f32> pos, irr::core::vector2d<irr::f32>& speed, irr::f32 diff)
{
	irr::core::vector2df p = pos;
	irr::core::vector2df dir = p-Pos;
	irr::f32 dSQ = dir.getLengthSQ();
	if (dSQ > Radius)
		return;
	dir.normalize();
	if (dSQ == 0)
		speed+=dir*Size*diff;
	else
		speed+=dir*Size*(1.0f-dSQ/Radius)*diff;
	//Time -= diff;
}

CGrid::CGrid(irr::u32 a, irr::u32 b, irr::scene::ISceneManager* manager, irr::core::dimension2d<irr::s32> size) : irr::scene::ISceneNode(manager->getRootSceneNode(), manager, -1), Driver(manager->getVideoDriver()), A(a), B(b){
SpecialGrid = true;
	Points = new irr::core::vector2d<irr::f32>[(a)*(b)];
	PointOffset = new irr::core::vector2d<irr::f32>[(a)*(b)];
	PointSpeed = new irr::core::vector2d<irr::f32>[(a)*(b)];


	irr::f32 Width = size.Width;//Driver->getScreenSize().Width;
	irr::f32 Height = size.Height;//Driver->getScreenSize().Height;

	Border = irr::core::vector2df(Width/a*(a-1), Height/b*(b-1));
	for (irr::u32 x = 0;x<a;x++)
	{
	for (irr::u32 y = 0;y<b;y++)
	{
		Points[x+y*A] = irr::core::vector2d<irr::f32>(Width/a*x, Height/b*y);
	}
	}
	Material.DiffuseColor = irr::video::SColor(200, 255, 0,0);
	Material.AmbientColor = irr::video::SColor(255, 255, 0,0);
	Material.Lighting = false;
	Material.MaterialType = irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL;
	// Material.ZWriteEnable = false;

	// grid shouldnt screw w/ teh camera.
	// SceneManager->getActiveCamera()->setPosition(irr::core::vector3df(Border.X/2,Border.Y/2,-300));
	// SceneManager->getActiveCamera()->setTarget(irr::core::vector3df(Border.X/2,Border.Y/2,0));
}

CGrid::~CGrid(void) {
	delete [] Points;
	delete [] PointOffset;
	delete [] PointSpeed;
	Forces.clear();
}

void CGrid::setGridCalc(bool special) {
	SpecialGrid = special;
}

void CGrid::render(void) {
	irr::video::IVideoDriver* driver = SceneManager->getVideoDriver();
	driver->setTransform(irr::video::ETS_WORLD, AbsoluteTransformation);
	driver->setMaterial(Material);

	//printf("%i active Forces\n", Forces.size());

	irr::u32 x = 0;
	while (x<A)
	{
		irr::u32 y = 0;
		while (y<B)
		{
		if (x != 0 && y != 0 && x != A-1 && y != B-1)
		{
			if (SpecialGrid)
			{
			irr::core::vector2df o1 = (Points[x+y*A]+PointOffset[x+y*A])-(Points[x+1+y*A]+PointOffset[x+1+y*A]);
			irr::core::vector2df o2 = (Points[x+y*A]+PointOffset[x+y*A])-(Points[x-1+y*A]+PointOffset[x-1+y*A]);
			irr::core::vector2df o3 = (Points[x+y*A]+PointOffset[x+y*A])-(Points[x+(y+1)*A]+PointOffset[x+(y+1)*A]);
			irr::core::vector2df o4 = (Points[x+y*A]+PointOffset[x+y*A])-(Points[x+(y-1)*A]+PointOffset[x+(y-1)*A]);

			PointSpeed[x+y*A] += -o1*diff*20-o2*diff*20-o3*diff*20-o4*diff*20-PointOffset[x+y*A]*diff-PointSpeed[x+y*A]*diff*2;
			}
			else
			PointSpeed[x+y*A] += -PointOffset[x+y*A]*diff*20-PointSpeed[x+y*A]*diff*2;

			irr::u32 i = 0;

			while (i<Forces.size())
			{
			Forces[i].affect(Points[x+y*A]+PointOffset[x+y*A], PointSpeed[x+y*A], diff);
			/*if (Forces[i].Time <= 0.0f)
			{
				Forces.erase(i);
			}
			else*/
			i++;
			}

			PointOffset[x+y*A] += PointSpeed[x+y*A]*diff;

			//irr::u32 is = 0

			if (abs(PointOffset[x+y*A].X) <= 1 && abs(PointSpeed[x+y*A].X) <= 1)
			{
			PointOffset[x+y*A].X = 0;
			}
			if (abs(PointOffset[x+y*A].Y) <= 1 && abs(PointSpeed[x+y*A].Y) <= 1)
			{
			PointOffset[x+y*A].Y = 0;
			}
		}

		//actual draw
		if (x+y*A+1 < A*B && x+1 < A)
		{
			irr::core::vector3df s(Points[x+y*A].X+PointOffset[x+y*A].X, Points[x+y*A].Y+PointOffset[x+y*A].Y, 0 );
			irr::core::vector3df d(Points[x+y*A+1].X+PointOffset[x+y*A+1].X, Points[x+y*A+1].Y+PointOffset[x+y*A+1].Y, 0 );
			Driver->draw3DLine(s, d, Material.DiffuseColor);
		}
		if (x+(y+1)*A < A*B && y+1 < B)
		{
			irr::core::vector3df s(Points[x+y*A].X+PointOffset[x+y*A].X, Points[x+y*A].Y+PointOffset[x+y*A].Y, 0 );
			irr::core::vector3df d(Points[x+(y+1)*A].X+PointOffset[x+(y+1)*A].X, Points[x+(y+1)*A].Y+PointOffset[x+(y+1)*A].Y, 0 );
			Driver->draw3DLine(s, d, Material.DiffuseColor);
		}
		y++;
		}
		x++;
	}
	//delete forces
	Forces.clear();
}
void CGrid::addForce(irr::core::vector2df pos, irr::f32 size, irr::f32 radius, irr::f32 time) {
	//AppFrame::ILogger::log("add Force");
	Forces.push_back(force(pos, size, radius, time));
}

void CGrid::OnRegisterSceneNode(void) {
	if ( IsVisible ) {
		SceneManager->registerNodeForRendering( this );
	}
}

void CGrid::OnAnimate(irr::u32 timeMs) {
    if ( !IsVisible )
        return;
    if (last_time == 0)
        last_time = timeMs;
    diff = timeMs-last_time;
    diff /= 1000.0f;
    last_time = timeMs;

    irr::u32 i = 0;

    /*for (irr::u32 x = 0;x<A;x++)
    {
        for (irr::u32 y = 0;y<B;y++)
        {
            if (x == 0 || y == 0 || x == A-1 || y == B-1)
                continue;
            irr::u32 s = (PointOffset[x+y*A].getLengthSQ())*diff/1000;

            PointSpeed[x+y*A] += -PointOffset[x+y*A]/3-PointSpeed[x+y*A]/50;

            for (irr::u32 i = 0;i<Forces.size();i++)
                Forces[i].affect(Points[x+y*A]+PointOffset[x+y*A], PointSpeed[x+y*A]);

            PointOffset[x+y*A] += PointSpeed[x+y*A]*diff;

            //irr::u32 is = 0

            if (abs(PointOffset[x+y*A].X) <= 1 && abs(PointSpeed[x+y*A].X) <= 1)
            {
                PointOffset[x+y*A].X = 0;
            }
            if (abs(PointOffset[x+y*A].Y) <= 1 && abs(PointSpeed[x+y*A].Y) <= 1)
            {
                PointOffset[x+y*A].Y = 0;
            }
        }
    }
    Forces.clear();
    */
}

const irr::core::aabbox3d<irr::f32>& CGrid::getBoundingBox() const {
    return Box;
}

irr::u32 CGrid::getMaterialCount() const {
    return 1;
}

irr::video::SMaterial& CGrid::getMaterial(irr::u32 i) {
    return Material;
}

irr::core::vector2df CGrid::getBorder(void) {
    return Border;
}

void CGrid::setColor(irr::video::SColor color) {
    Material.DiffuseColor = color;
}
