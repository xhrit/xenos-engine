// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xcontextmenu.h

#ifndef XCONTEXTMENU_H
#define XCONTEXTMENU_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xcontextmenu : static text
///
/// This is a static text element.
///


class xcontextmenu : public xelement {
	public:

		///
		/// constructor.
		///
		xcontextmenu(  irr::IrrlichtDevice *dev, int id, x2drect r );
		xcontextmenu(  irr::IrrlichtDevice *dev, int id );
		xcontextmenu( irr::IrrlichtDevice *dev, int id, IGUIContextMenu * m );

		///
		/// destructor.
		///
		~xcontextmenu();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();
		void setVisible( bool v );
		void setParent( xelement * x );

		void addItem( int id, char * c, bool sub );
		char * getItem( int i );
		int getSelected();
		xcontextmenu * getSubMenu( int id, int nid  );
		void addSeparator();

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;      // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		IGUIContextMenu * contextmenu;
};

#endif // XCONTEXTMENU_H
