// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xevent.cpp

#include "xevent.h"

xevent::xevent(){
}

xevent::~xevent(){
}

void xevent::setKeyEventType ( EKEY_CODE key ){
	KeyEventType = (XKEY_EVENT_TYPE) key;
}

void xevent::setGUIEventType ( EGUI_EVENT_TYPE gui ){
	GUIEventType = (XGUI_EVENT_TYPE) gui;
}

void xevent::setMouseEventType ( EMOUSE_INPUT_EVENT mouse ){
	MouseEventType = (XMOUSE_EVENT_TYPE) mouse;
}
