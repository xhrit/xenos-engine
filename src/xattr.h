// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XATTR_H
#define XATTR_H

/// XATTR_TYPE
///
/// A more detailed description.
///

enum XATTR_TYPE{
	XATTR_TYPE_INT,
	XATTR_TYPE_FLOAT,
	XATTR_TYPE_STRING,
	XATTR_TYPE_BOOL
};

///
/// xattr
///
/// This is an attribute.
///

class xattr {
	public:
		///
		/// constructor.
		///
		xattr();

		///
		/// destructor.
		///
		~xattr();

		void setType( XATTR_TYPE t );
		void setName( char * name );
		void setValue( char * value ); // , XATTR_TYPE type
		char * getName();
		char * getValue();

		// wtf?
		XATTR_TYPE type;
		char * name;
		char * value;
	private:
};

#endif // XATTR_H
