// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xlight.cpp

#include "xlight.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xlight::xlight( irr::IrrlichtDevice *dev, int id, x3dpoint pos, xcolour c, float radius  ) : xelement( dev, id ){
	// cout << "[xlight] create xlight" << endl;
	light = irr_smgr->addLightSceneNode( 0, pos.getv3df(), video::SColorf( c.A, c.R, c.G, c.B), radius );

	setType( XELEMENT_TYPE_LIGHT );
	node_reference = light;
}

xlight::~xlight(){
	// cout << "[xlight] delete xlight" << endl;
}

int xlight::update(){
	// cout << "[xlight] xlight::update" << endl;
	return 1;
}

int xlight::render(){
	// cout << "[xlight] xlight::render" << endl;
	return 1;
} // xlight::render

bool xlight::event(){
	// cout << "[xlight] xlight::event" << endl;
	return false;
} // xlight::event

void xlight::destroy(){
	// cout << "[xlight] xlight::destroy" << endl;
	light->remove();
} // xlight::destroy

void xlight::setPosition( x3dpoint p){
	// cout << "[xlight] setPosition" << endl;
	position = p;
	light->setPosition ( p.getv3df() );
} // xlight::setPosition
