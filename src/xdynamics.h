// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XDYNAMICS_H
#define XDYNAMICS_H

#define GEOMSPERBODY   32   // maximum number of geometries per body
#define JOINTSPERBODY 10   // maximum number of joints per body
#define JOINTGROUPSPERBODY 10   // maximum number of joints per body
#define MAX_CONTACTS  10   // maximum number of ODE contacts

#include "xbody.h"
#include "xelement.h"
#include "xmesh.h"
#include "xterrainmesh.h"

#include <ode/ode.h>

class xcollision {
	public:
		xcollision(){ a = 0; b = 0; };
		~xcollision(){};

		void seto1( xbody * o ){
                o1 = o; a = 1;
		    };
		void seto2( xbody * o ){
                o2 = o; b = 1;
            };
		void setContactPoint( x3dpoint p ){ contactPoint = p; };

		xbody * geto1(){
            if (a == 1) {
                return o1;
            }else{
                return NULL;
            }
		};

		xbody * geto2(){
            if (b == 1) {
                return o2;
            }else{
                return NULL;
            }
        };
		x3dpoint getContactPoint(){ return contactPoint; };

	private:
		xbody * o1;
		xbody * o2;
		int a, b;
		x3dpoint contactPoint;
		// todo : set contact point data in collision callback
};

enum XDYNAMICS_TYPE {
	XDYNAMICS_STEP,
	XDYNAMICS_FAST,
	XDYNAMICS_QUICK
};

///
/// xdynamics : physics
///
class xdynamics {
	public:

		///
		/// constructor.
		///
		xdynamics();

		///
		/// destructor.
		///
		~xdynamics();

		void update();
		static void nearCollision (void *data, dGeomID o1, dGeomID o2);
		void collision(dGeomID o1, dGeomID o2);
		xcollision * getCollision();
        void removeCollision();
		int getCollisionCount();
        void clearCollisionQue();
        xcollision * getCollisionFromLine( x3dpoint pos, x3dpoint rot, float len );

		void createBody( xbody *b );
		void destroyBody( xbody *b );

		void createBoxGeom( xbody *b, x3dpoint size, x3dpoint pos, x3dpoint rot );
		void createSphereGeom( xbody *b, float radius, x3dpoint pos, x3dpoint rot );
		void createTrimeshGeom( xbody *b, xmesh * m, x3dpoint pos, x3dpoint rot, bool immobile );
		void createTrimeshGeomFromTerrain( xbody *b, xterrainmesh * t, x3dpoint pos, x3dpoint rot, bool mobile );
		void destroyGeom( xbody *b );

		void setPlane2d( xbody * o1 );
		void destroyJoints( xbody *b );

		// void createCapsuleGeom();	// TODO: finish coding this
		// void createHinge2Joint();
		// void createAMotor();

		void setGravity( x3dpoint g );
		void setERP( float erp );
		void setCFM( float cfm );
		void setContactSurface( float f );
		void setCorrectingVel( float f );
		void setAutoDisable( bool b );
		void setTimeStep( float f );
		void setMode( XDYNAMICS_TYPE m );

		void eulerToQuaternion(const irr::core::vector3df &euler, dQuaternion quaternion);
		x3dpoint quaternionToEuler( const dQuaternion quaternion );


	private:
		void addReference(xbody* e );
        void removeReference(xbody* e );
		xbody * getxbodyByGeomID( dGeomID gid );

		dWorldID world;			// dynamics world
		dSpaceID space;			// collision space
		dJointGroupID contactgroup;	// active collisions
		int body_count;
		vector < xbody * > references;
		float step;
		XDYNAMICS_TYPE dmode;

		vector< xcollision * > collision_que;
};

#endif // XDYNAMICS_H
