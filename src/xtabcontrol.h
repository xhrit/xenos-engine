// xengine Lukas Irwin [ sysop@xhrit.com ]
// copyright (c) 2007
// This program is distributed under the terms of the GNU General Public License
// xtabcontrol.h

#ifndef XTABCONTROL_H
#define XTABCONTROL_H

#include "xelement.h"       // Simulation objects
#include "xtab.h"           // a tab page, that gui elements can be added to.

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xtabcontrol
///
/// worthless comment
///

class xtabcontrol : public xelement {
	public:

		///
		/// constructor.
		///
		xtabcontrol( irr::IrrlichtDevice *dev, int id, x2drect r );

		///
		/// destructor.
		///
		~xtabcontrol();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		char * getText();
		void setText( char * c );
		void setParent( xelement * x );

		xtab* addTab( char * c, int id );
		int getActiveTab ();
		void setActiveTab ( int id);

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;            // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
};

#endif // XTABCONTROL_H
