// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xmesh.cpp

#include "xmesh.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xmesh::xmesh( irr::IrrlichtDevice *dev, int id, char * mesh_fn) : xelement( dev, id ){
	// cout << "[xmesh] create xmesh" << endl;
	mesh = irr_smgr->getMesh( mesh_fn );
	node = irr_smgr->addAnimatedMeshSceneNode( mesh );
	node->setID( id );

	hasShadow = 0;

	setPosition( x3dpoint( 0, 0, 0 ) );
	setType( XELEMENT_TYPE_MESH );
	node_reference = node;
}

xmesh::~xmesh(){
	// cout << "[xmesh] delete xmesh" << endl;
}

int xmesh::update(){
	// cout << "[xmesh] xmesh::update" << endl;
	return 1;
}

int xmesh::render(){
	// cout << "[xmesh] xmesh::render" << endl;
	return 1;
} // xmesh::render

bool xmesh::event(){
	// cout << "[xmesh] xmesh::event" << endl;
	return false;
} // xmesh::event

void xmesh::destroy(){
	// cout << "[xmesh] xmesh::destroy" << endl;
	if ( hasShadow == 1 ){
		cout << "[xmesh] remove shadow volume" << endl;
		shadow->remove();
	}
	node->remove();
	setType( XELEMENT_TYPE_NULL );

} // xmesh::destroy

void xmesh::setDebug( bool b){
	node->setIsDebugObject( b );
	// node->setDebugDataVisible ( EDS_BBOX );
}

x3dpoint xmesh::getPosition(){
	vector3df p = node->getPosition();
	return x3dpoint ( p.X, p.Y, p.Z );
}

x3dpoint xmesh::getRotation(){
	vector3df p = node->getRotation();
	return x3dpoint ( p.X, p.Y, p.Z );
}

x3dpoint xmesh::getScale(){
	vector3df p = node->getScale();
	return x3dpoint ( p.X, p.Y, p.Z );
}

void xmesh::setPosition( x3dpoint p){
	// cout << "[xmesh] setPosition" << endl;
	position = p;
	node->setPosition ( p.getv3df() );
} // xmesh::setPosition

void xmesh::setRotation( x3dpoint p){
	// cout << "[xmesh] setRotation" << endl;
	rotation = p;
	node->setRotation ( p.getv3df() );
} // xmesh::setRotation

void xmesh::setScale(x3dpoint p) {
	scale = p;
	node->setScale ( p.getv3df() );
} // xmesh::setScale

void xmesh::setMaterialFlag( XMATERIAL_FLAG mf, bool val ){
	node->setMaterialFlag ( (E_MATERIAL_FLAG)mf, val );

} // xmesh::setMaterialFlag

// TODO: fixme? do something about this :
// node->getMaterial(0).AmbientColor = ambient.getSColour();
// node->getMaterial(0).DiffuseColor = diffuse.getSColour();
// node->getMaterial(0).SpecularColor = specular.getSColour();

void xmesh::setMaterialType( XMATERIAL_TYPE mt ){ //
	// cout << "[xmesh] xmesh::setMaterialType" << endl;
	video::IMaterialRenderer* renderer = irr_driver->getMaterialRenderer( (E_MATERIAL_TYPE)mt );

	// display some problem text when problem
	if (!renderer || renderer->getRenderCapability() != 0){
		cout << "[xmesh] unable to render effect: " << mt << endl;;
	} else {
		node->setMaterialType( (E_MATERIAL_TYPE)mt );
	}

} // xmesh::setMaterialType

void xmesh::addNormalMap( char * bump, float factor ){
	// cout << "[xmesh] xmesh::addNormalMap" << endl;
	video::ITexture* normalMap = irr_driver->getTexture( bump );
	irr_driver->makeNormalMapTexture( normalMap, factor );
	node->setMaterialTexture( 1, normalMap );
	// node->setMaterialType( EMT_NORMAL_MAP_SOLID);

} // xmesh::addNormalMap

void xmesh::addTexture( char * fn, int index ){
	// cout << "[xmesh] addTexture: " << tex_fn << endl;
	node->setMaterialTexture( index, irr_driver->getTexture( fn ) );
} // xmesh::addTexture

void xmesh::setMD2Animation( int i ){
	node->setMD2Animation( (EMD2_ANIMATION_TYPE)i );
} // xmesh::setMD2Animation

void xmesh::setFrameLoop( int begin, int end ){
	node->setFrameLoop( begin, end );
} // xmesh::setFrameLoop

void xmesh::createTriangleSelector(){
	selector = irr_smgr->createTriangleSelector(mesh, node);
        node->setTriangleSelector(selector);
} // xmesh::createTriangleSelector

int xmesh::getStartFrame(){
	return node->getStartFrame();
} // xmesh::getStartFrame

int xmesh::getEndFrame (){
	return node->getEndFrame();
} // xmesh::getEndFrame

void xmesh::setAnimationSpeed( int fps ){
	node->setAnimationSpeed ( fps );
} // xmesh::setAnimationSpeed

void xmesh::setCurrentFrame( int frame ){
	node->setCurrentFrame ( frame );
} // xmesh::setCurrentFrame

void xmesh::addShadowVolume( bool zfail ){
	cout << "[xmesh] addShadowVolume " << endl;
	hasShadow = 1;
	node->addShadowVolumeSceneNode ( 0, -1, zfail );
} // xmesh::addShadowVolume

void xmesh::setLoopMode( bool loop ){
	node->setLoopMode ( loop );
} // xmesh::setLoopMode

xbone * xmesh::getBone (  char * jointName ){
	// cout << "[xmesh] getJointNode: " << jointName << endl;
	if ( node->getJointNode( jointName ) == NULL ){
		cout << "[xmesh] error: no joint named " << jointName << endl;
		return NULL;
	}
	xbone * b = new xbone(irr_device, -1, node->getJointNode( jointName ) );
	// addReference( e ); // should keep track ov this, to delete cleanly.
	// cout << "[xmesh] /getJointNode" << endl;
	return b;
} // xmesh::getJointNode

void xmesh::setParent( xelement * x ){
	// cout << "[xmesh] setParent" << endl;
	node->setParent( x->getGfxNode() );
	// cout << "[xmesh] /setParent" << endl;
} // xmesh::setParent

void xmesh::setHardwareMapping( X_HARDWARE_MAPPING hint, X_BUFFER_TYPE buffer ){
	cout << "[xmesh] setHardwareMapping" << endl;
	node->getMesh()->setHardwareMappingHint( (E_HARDWARE_MAPPING) hint, (E_BUFFER_TYPE) buffer );
	cout << "[xmesh] /setHardwareMapping" << endl;
} // xmesh::setHardwareMappingHint

