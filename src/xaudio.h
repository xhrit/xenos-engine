// zp3 c 2006 xhrit.com
// xaudio Header
// Change log

#ifndef XAUDIO_H
#define XAUDIO_H

// #include <vector.h>

#include <stdlib.h>
#include <stdio.h>

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>
#include <ogg/ogg.h>
#include <vorbis/vorbisfile.h>
#include <vector>
#include <string>
#include <iostream>

// #define BUFFER_SIZE (4096 * 4)
#define BUFFER_SIZE     32768       // 32 KB buffers

using namespace std;

typedef ALuint ALsource;

class xaudio{
	public:
		xaudio ();
		~xaudio ();

		void update ();
		void clear ();
		unsigned int addWAVSample ( char *path, bool loop, float gain = 1.0 );
		unsigned int addOGGSample ( char *fileName, float gain = 1.0 );
		void playSource ( unsigned int src );
		void setSourcePosition ( unsigned int src, float x, float y, float z );
		void setListenerPosition ( float x, float y, float z );

		// not bound to scripting
		std::string getLastErr ();
		bool isValid ();
		bool isPlaying ( unsigned int src );
		void stopSource ( unsigned int src );
		void pauseSource ( unsigned int src );
		void rewindSource ( unsigned int src );
		void setSourceGain ( unsigned int src, float gain );
		void playSourcev ( int n, unsigned int srcv[] );
		void stopSourcev ( int n, unsigned int srcv[] );
		void pauseSourcev ( int n, unsigned int srcv[] );
		void rewindSourcev ( int n, unsigned int srcv[] );
		bool initOpenAL ();
		void closeDriver();
		void removeSource ( unsigned int src );

		int getFreeSource ();
		unsigned int getSource ();

		unsigned int loadALBuffer ( char *path );
		unsigned int getLoadedALBuffer (char *path );
		std::string getALErrorString ( ALenum err );
		std::string getALCErrorString ( ALenum err );

		// void setSourceVelocity();
		// void setListenerVelocity();
		// void setListenerOrientation();
	private:
		bool valid;
		std::string errStr;

		vector < std::string > LoadedFiles;

		vector < ALuint > Buffers;
		vector < ALuint > Sources;

		// vector < Buffer_Data > Buffers;
		// typedef Buffer_Data {
		//	ALuint buffer;
		//	int time_played;
		//	var other_info;
		// };
		// vector < Source_Data > Sources;
		// typedef Buffer_Data {
		//	ALuint Source;
		//	int time_played;
		//	int distance from listiner;
		//	var other_info;
		// };

		vector < bool > SourcesActive;

		// Positions of the source sounds.
		vector < ALfloat[3] > SourcesPos;

		// Velocity of the source sounds.
		vector < ALfloat[3] > SourcesVel;
		int maxSources;
};

#endif // XAUDIO_H
