// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// text.cpp

#include "xcubemesh.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xcubemesh::xcubemesh( irr::IrrlichtDevice *dev, int id ) : xelement( dev, id ){
	// cout << "[xcubemesh] create xcubemesh; id " << id << endl;
	node = irr_smgr->addCubeSceneNode();
	node->setID( id );
	node_reference = node;
	setType( XELEMENT_TYPE_MESH );
	position = x3dpoint(0, 0, 0);
	rotation = x3dpoint(0, 0, 0);
}

xcubemesh::~xcubemesh(){
	// cout << "[xcubemesh] delete xcubemesh" << endl;
}

int xcubemesh::update(){
	// cout << "[xcubemesh] xcubemesh::update" << endl;
	return 1;
}

int xcubemesh::render(){
	// cout << "[xcubemesh] xcubemesh::render" << endl;
	return 1;
} // xcubemesh::render

bool xcubemesh::event(){
	// cout << "[xcubemesh] xcubemesh::event" << endl;
	return false;
} // xcubemesh::event

void xcubemesh::destroy(){
	// cout << "[xcubemesh] xcubemesh::destroy" << endl;
	node->remove();
	setType( XELEMENT_TYPE_NULL );
} // xcubemesh::destroy

void xcubemesh::setPosition( x3dpoint p){
	// cout << "[xcubemesh] setPosition" << endl;
	position = p;
	node->setPosition ( p.getv3df() );
} // xcubemesh::setPosition

void xcubemesh::setRotation( x3dpoint p){
	// cout << "[xcubemesh] setRotation" << endl;
	rotation = p;
	node->setRotation ( p.getv3df() );
} // xcubemesh::setRotation

void xcubemesh::setScale(x3dpoint p) {
	scale = p;
	node->setScale ( p.getv3df() );
} // xcubemesh::setScale

void xcubemesh::setVisible( bool val ){
	node->setVisible ( val );
} // xcubemesh::setVisible

void xcubemesh::setMaterialFlag( XMATERIAL_FLAG mf, bool val ){
	node->setMaterialFlag ( (E_MATERIAL_FLAG)mf, val );
} // xcubemesh::setMaterialFlag

void xcubemesh::setMaterialType( XMATERIAL_TYPE mt ){
	node->setMaterialType( (E_MATERIAL_TYPE)mt );
} // xcubemesh::setMaterialType

void xcubemesh::addNormalMap( char * bump, float factor ){
	video::ITexture* normalMap = irr_driver->getTexture( bump );
	irr_driver->makeNormalMapTexture( normalMap, factor );
	node->setMaterialTexture( 1, normalMap );
} // xcubemesh::addNormalMap

void xcubemesh::addTexture( char * fn, int index ){
	// cout << "[xmesh] addTexture: " << tex_fn << endl;
	node->setMaterialTexture( index, irr_driver->getTexture( fn ) );
} // xcubemesh::addTexture

void xcubemesh::createTriangleSelector(){
	selector = irr_smgr->createTriangleSelector( node->getMesh(), node);
	node->setTriangleSelector( selector );
	// s->drop();
}
