// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "x3dline.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

x3dline::x3dline( irr::IrrlichtDevice *dev, int id, x3dpoint s, x3dpoint e ) : xelement( dev, id ){
	// cout << "[x3dline] create x3dline" << endl;
	startpoint = s;
	endpoint = e;
	visible = true;
	setType( XELEMENT_TYPE_LINE );
}

x3dline::~x3dline(){
	// cout << "[x3dline] delete x3dline" << endl;
}

int x3dline::update(){
	// cout << "[x3dline] x3dline::update" << endl;
	return 1;
}

int x3dline::render(){
	// cout << "[x3dline] x3dline::render" << endl;
	if ( visible == true ){
		// irr_driver draw line using irrdriver
		SMaterial mat;
		mat.Lighting = false;
		irr_smgr->getVideoDriver()->setMaterial(mat);
		irr_smgr->getVideoDriver()->setTransform(ETS_WORLD, matrix4());
		irr_smgr->getVideoDriver()->draw3DLine( startpoint.getv3df() , endpoint.getv3df(), colour.getSColour() );
	}
	return 1;
}

bool x3dline::event(){
	cout << "[x3dline] x3dline::event" << endl;
	return false;
}

void x3dline::destroy(){
	// cout << "[x3dline] x3dline::destroy" << endl;
	visible = false;
}

void x3dline::setColour( xcolour c ){
	colour = c;
}

void x3dline::setStart( x3dpoint p ){
	startpoint = p;
}

void x3dline::setEnd( x3dpoint p ){
	endpoint = p;
}

x3dpoint x3dline::getStart(){
	return startpoint;
}

x3dpoint x3dline::getEnd(){
	return endpoint;
}

x3dpoint x3dline::getClosestPoint( x3dpoint p ){
    core::line3d<f32> ray;
    ray.start = startpoint.getv3df();
    ray.end = endpoint.getv3df();
    vector3df np = ray.getClosestPoint( p.getv3df() );
    return x3dpoint( np.X, np.Y, np.Z );
}
