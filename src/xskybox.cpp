// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xskybox.cpp

#include "xskybox.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xskybox::xskybox( irr::IrrlichtDevice *dev, int id,
			char * filename1,
			char * filename2,
			char * filename3,
			char * filename4,
			char * filename5,
			char * filename6) : xelement( dev, id ){
	// cout << "[xcore] -xskybox" << endl;
	// images.clear();

	irr_driver->setTextureCreationFlag(video::ETCF_CREATE_MIP_MAPS, false);
	skybox_node =  irr_smgr->addSkyBoxSceneNode(
		irr_driver->getTexture(filename1),
		irr_driver->getTexture(filename2),
		irr_driver->getTexture(filename3),
		irr_driver->getTexture(filename4),
		irr_driver->getTexture(filename5),
		irr_driver->getTexture(filename6) );

	/*
		irr_driver->getTexture("modules/tutorial1/resources/sky_aqua_05.tga"),
		irr_driver->getTexture("modules/tutorial1/resources/sky_aqua_04.tga"),
		irr_driver->getTexture("modules/tutorial1/resources/sky_aqua_02.tga"),
		irr_driver->getTexture("modules/tutorial1/resources/sky_aqua_06.tga"),
		irr_driver->getTexture("modules/tutorial1/resources/sky_aqua_03.tga"),
		irr_driver->getTexture("modules/tutorial1/resources/sky_aqua_01.tga") );
	*/
/*
1 up 5
2 dn 4
3 ri 2
4 lf 6
5 bk 3
6 ft 1
*/

	// light
	// irr_driver->setAmbientLight   ( SColorf(255, 255, 255, 255) );

	// images.push_back(sb1);
	// images.push_back(sb2);
	// images.push_back(sb3);
	// images.push_back(sb4);
	// images.push_back(sb5);
	// images.push_back(sb6);
	// irr_driver->setTextureCreationFlag(video::ETCF_CREATE_MIP_MAPS, true);

	setType( XELEMENT_TYPE_SKYBOX );
}

xskybox::~xskybox(){
	cout << "[xcore] delete xskybox" << endl;
}

void xskybox::destroy(){
	skybox_node->remove();
	setType( XELEMENT_TYPE_NULL );
}
