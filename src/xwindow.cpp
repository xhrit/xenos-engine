// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xwindow.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xwindow::xwindow( irr::IrrlichtDevice *dev, int id, x2drect r, char * c  ) : xelement( dev, id ){
	// cout << "[xwindow] create xwindow" << endl;

	stream.str("");
	stream.clear();
	swdata  = c;
	wsdata  = swdata.c_str();

	window = irr_env->addWindow( rect<s32>(r.X1, r.Y1, r.X2, r.Y2), false, wsdata );
	window->getCloseButton()->setVisible(false);

	element_reference = window;

	setType( XELEMENT_TYPE_WINDOW );
}

xwindow::~xwindow(){
	// cout << "[xwindow] delete xwindow" << endl;

}

int xwindow::update(){
	// cout << "[xwindow] xwindow::update" << endl;
	return 1;
}

int xwindow::render(){
	// cout << "[xwindow] xwindow::render" << endl;
	return 1;
}

bool xwindow::event(){
	// cout << "[xwindow] xwindow::event" << endl;
	return false;
}

void xwindow::destroy(){
	// cout << "[xwindow] xwindow::destroy" << endl;
	window->remove();
}

void xwindow::setVisible( bool v ){
	// cout << "[xwindow] setVisible" << endl;
	window->setVisible( v );
} // xwindow::setVisible

void xwindow::setParent( xelement * x ){
	parent = x;
	window->setParent( x->getGfxElement() );
} // xwindow::setParent

void xwindow::setSize( int w, int h ){
	window->setMinSize( core::dimension2du(w, h) );
	window->setMaxSize( core::dimension2du(w, h) );
} // xwindow::setSize

void xwindow::setText( char * c ){
	// cout << "[xwindow" << element_id <<"] xwindow::setText" << endl;
	swdata  = c;
	wsdata  = swdata.c_str();
	window->setText( wsdata );
} // xwindow::setText

void xwindow::setTitleBarVisible( bool b ){
    // todo : fixme (port irr hack to windows)
	window->setDrawTitlebar( b );

} // xwindow::setTitleBarVisible

