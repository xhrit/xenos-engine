// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xcombobox.h

#ifndef XCOMBOBOX_H
#define XCOMBOBOX_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xcombobox : static text
///
/// This is a static text element.
///


class xcombobox : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		///
		xcombobox(  irr::IrrlichtDevice *dev, int id, x2drect r );

		///
		/// destructor.
		///
		~xcombobox();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();
		void setVisible( bool v );
		void setParent( xelement * x );

		void addItem( char * c );
		char * getItem( int i );
		int getSelected();
		void setSelected( int n );
		void clear();

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;      // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		IGUIComboBox * combobox;
};

#endif // XCOMBOBOX_H
