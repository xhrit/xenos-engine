// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XBEAM_H
#define XBEAM_H

#include "xelement.h"       // Simulation objects
#include "CBeamSceneNode.h" // ...

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xbeam : quad node
///
///

class xbeam : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param mesh character array ov mesh to load.
		/// \param tex texture to load.
		///
		xbeam( irr::IrrlichtDevice *dev, int id, char * tx_fn1, char * tx_fn2 );

		///
		/// destructor.
		///
		~xbeam();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		void setMaterialFlag( XMATERIAL_FLAG mf, bool val );
		void setMaterialType( XMATERIAL_TYPE mt);
		void addNormalMap( char * bump, float factor );
		void addTexture( char * fn, int index );

		void setLine(x3dpoint start, x3dpoint end, float scale );
        void setColour(xcolour c );

		CBeamNode* beam;

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;      // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		x3dpoint scale;
};

#endif // XBEAM_H
