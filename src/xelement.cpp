// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

// node.h
#include "xelement.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

xelement::xelement( irr::IrrlichtDevice *dev, int id ){
	// cout << "[xelement] new xelement; id " << id << endl;
	element_id = id;
	irr_device = dev;                                    // collect irrlicht driver pointer
	irr_driver = irr_device->getVideoDriver();           // collect irrlicht driver pointer
	irr_smgr = irr_device->getSceneManager();            // irrlicht scene manager pointer
	irr_env = irr_device->getGUIEnvironment();           // irrlicht gui pointer

}

xelement::~xelement(){
	// cout << "[xcore] xelement destructor" << endl;
	// delete children
}

int xelement::update(){
	// cout << "[xelement] xelement::update" << endl;
	return 1;
}

int xelement::render(){
	// cout << "[xelement] xelement::render" << endl;
	return 1;
}

void xelement::destroy(){
	// cout << "[xelement] xelement::destroy" << endl;
}

bool xelement::event(){
	// cout << "[xelement] xelement::event" << endl;
	return false;
}

void xelement::setAttr( char * n, char * v ){
	// test to see if attr exists
	int doesexist = 0;
	for ( int i = 0; i < attributes.size(); i++ ) {
		if ( attributes[ i ].name == n ){
			doesexist = i;
		}
	}
	if ( doesexist != 0 ){
		// if it does, set to new value
		attributes[ doesexist ].value = v;
	} else {
		// else create it
		xattr new_attr = xattr();
		new_attr.name = n;
		new_attr.value = v;
		attributes.push_back( new_attr );
	}
} // xelement::setAttr

char * xelement::getAttr( char * n ){
	// find attr
	for ( int i = 0; i < attributes.size(); i++ ) {
		if ( attributes[ i ].name == n ){
			return attributes[ i ].value;
		}
	}
	return NULL;
} // xelement::getAttr

void xelement::setParent( xelement * x ){
	cout << "[xelement] xelement::setParent" << endl;
	parent = x;
} // xelement::setParent

xelement * xelement::getParent(){
	cout << "[xelement] xelement::getParent" << endl;
	return parent;
} // xelement::getParent

void xelement::printType(){
	if (xelement_type == XELEMENT_TYPE_NULL){ cout << " XELEMENT_TYPE_NULL (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_AREA){ cout << " XELEMENT_TYPE_AREA (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_DIALOG){ cout << " XELEMENT_TYPE_DIALOG (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_SELECT){ cout << " XELEMENT_TYPE_SELECT (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_OPTION){ cout << " XELEMENT_TYPE_OPTION (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_INPUT){ cout << " XELEMENT_TYPE_INPUT (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_TEXT){ cout << " XELEMENT_TYPE_TEXT (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_IMAGE){ cout << " XELEMENT_TYPE_IMAGE (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_BUTTON){ cout << " XELEMENT_TYPE_BUTTON (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_WINDOW){ cout << " XELEMENT_TYPE_WINDOW (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_SCROLLBAR){ cout << " XELEMENT_TYPE_SCROLLBAR (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_LISTBOX){ cout << " XELEMENT_TYPE_LISTBOX (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_TEXTBOX){ cout << " XELEMENT_TYPE_TEXTBOX (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_EDITBOX){ cout << " XELEMENT_TYPE_EDITBOX (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_CONTEXTMENU){ cout << " XELEMENT_TYPE_CONTEXTMENU (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_ENVIRONMENT){ cout << " XELEMENT_TYPE_ENVIRONMENT (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_CAMERA){ cout << " XELEMENT_TYPE_CAMERA (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_LIGHT){ cout << " XELEMENT_TYPE_LIGHT (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_FIELD){ cout << " XELEMENT_TYPE_FIELD (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_MESH){ cout << " XELEMENT_TYPE_MESH (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_ANIMATEDMESH){ cout << " XELEMENT_TYPE_ANIMATEDMESH (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_TERRAIN){ cout << " XELEMENT_TYPE_TERRAIN (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_PARTICLESYSTEM){ cout << " XELEMENT_TYPE_PARTICLESYSTEM (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_SKYBOX){ cout << " XELEMENT_TYPE_SKYBOX (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_SCRIPT){ cout << " XELEMENT_TYPE_SCRIPT (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_BODY){ cout << " XELEMENT_TYPE_BODY (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_TABLE){ cout << " XELEMENT_TYPE_TABLE (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_COMBOBOX){ cout << " XELEMENT_TYPE_COMBOBOX (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_GRID){ cout << " XELEMENT_TYPE_GRID (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_TEXTNODE){ cout << " XELEMENT_TYPE_TEXTNODE (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_BEAM){ cout << " XELEMENT_TYPE_BEAM (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_BILLBOARD){ cout << " XELEMENT_TYPE_BILLBOARD (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_BONE){ cout << " XELEMENT_TYPE_BONE (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_CHECKBOX){ cout << " XELEMENT_TYPE_CHECKBOX (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_TAB){ cout << " XELEMENT_TYPE_TAB (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_LINE){ cout << " XELEMENT_TYPE_LINE (" << element_id << ")" << endl; }
	else if (xelement_type == XELEMENT_TYPE_CIRCLE){ cout << " XELEMENT_TYPE_CIRCLE (" << element_id << ")" << endl; }
    else {
        cout << " XELEMENT_TYPE_UNKNOWN (" << element_id << ")" << endl;
    }

} // xelement::printType

