// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xlight.h

#ifndef XLIGHT_H
#define XLIGHT_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xlight : it's a light, duh.
///
///

class xlight : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param mesh character array ov mesh to load.
		/// \param tex texture to load.
		///
		xlight( irr::IrrlichtDevice *dev, int id, x3dpoint pos, xcolour c, float radius );

		///
		/// destructor.
		///
		~xlight();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		void setPosition( x3dpoint p );
		x3dpoint getPosition(){ return position; };

		scene::ILightSceneNode* light;


	private:
		x3dpoint scale;
};

#endif // XLIGHT_H
