// xengine Lukas Irwin [ sysop@xhrit.com ]
// copyright (c) 2007
// This program is distributed under the terms of the GNU General Public License

#include "xtab.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xtab::xtab( irr::IrrlichtDevice *dev, int id, x2drect r ) : xelement( dev, id ){
	cout << "[xtab] create xtab" << endl;

	setType( XELEMENT_TYPE_TAB );
}

xtab::~xtab(){
	cout << "[xtab] delete xtab" << endl;
}

int xtab::update(){
	// cout << "[xtab] xtab::update" << endl;
	return 1;
}

int xtab::render(){
	// cout << "[xtab] xtab::render" << endl;
	return 1;
}

bool xtab::event(){
	// cout << "[xtab] xtab::event" << endl;
	return false;
}

void xtab::destroy(){
	// cout << "[xtab] xtab::destroy" << endl;
	// textbox->remove();
}

