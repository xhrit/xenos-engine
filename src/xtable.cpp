// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xtable.cpp

#include "xtable.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xtable::xtable( irr::IrrlichtDevice *dev, int id, bool b, x2drect r ) : xelement( dev, id ){
	// cout << "[xtable" << id <<"] create xtable" << endl;

	table = irr_env->addTable( rect<s32>(r.X1, r.Y1, r.X2, r.Y2), 0, id, b);
	element_reference = table;

	setType( XELEMENT_TYPE_TABLE );
}

xtable::~xtable(){
	// cout << "[xtable" << element_id <<"] delete xtable" << endl;
}

int xtable::update(){
	// cout << "[xtable" << element_id <<"] xtable::update" << endl;
	return 1;
} // xtable::setVisible

int xtable::render(){
	// cout << "[xtable" << element_id <<"] xtable::render" << endl;
	return 1;
} // xtable::setVisible

bool xtable::event(){
	// cout << "[xtable" << element_id <<"] xtable::event" << endl;
	return false;
} // xtable::setVisible

void xtable::destroy(){
	// cout << "[xtable" << element_id <<"] xtable::destroy" << endl;
	table->remove();
} // xtable::setVisible

void xtable::clear(){
	// cout << "[xtable" << element_id <<"] xtable::clear" << endl;
	table->clear();
} // xtable::setVisible

void xtable::setVisible( bool v ){
	// cout << "[xtable" << element_id <<"] setVisible" << endl;
	table->setVisible( v );
} // xtable::setVisible

void xtable::addColumn( char * caption, int index ){
	stream.str("");
	stream.clear();
	swdata  = caption;
	wsdata  = swdata.c_str();
	table->addColumn(wsdata, index);
} // xtable::addColumn

void xtable::addRow( int index ){
	table->addRow(index);
} // xtable::addRow

char * xtable::getCellText ( int i, int n ){
	core::stringc s = table->getCellText(i, n);
	return strdup( s.c_str() );
} // xtable::getCellText

void xtable::setCellText ( int row, int col, char * t ){
	// cout << "[xtable" << element_id <<"] setCellText" << endl;
	stream.str("");
	stream.clear();
	swdata  = t;
	wsdata  = swdata.c_str();
	table->setCellText( row, col, wsdata );

} // xtable::setCellText

int xtable::getSelected(){
	// cout << "[xtable" << element_id <<"] getSelected" << endl;
	return table->getSelected();
} // xtable::getSelected

void xtable::setSelected( int n ){
	// cout << "[xtable" << element_id <<"] setSelected" << endl;
	table->setSelected( n );
} // xtable::setSelected

void xtable::setParent( xelement * x ){
	parent = x;
	table->setParent( x->getGfxElement() );
} // xtable::setParent
