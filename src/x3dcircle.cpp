// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "x3dcircle.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

x3dcircle::x3dcircle( irr::IrrlichtDevice *dev, int id, x3dpoint p, float r ) : xelement( dev, id ){
	// cout << "[x3dcircle] create x3dcircle" << endl;
    position = p;
    radius = r;

    // creation
    node = new CCircle( position.getv3df(), radius, 40, core::vector3df(0,1,0), irr_smgr->getRootSceneNode(), irr_smgr);
	visible = true;
	setType( XELEMENT_TYPE_CIRCLE );
}

x3dcircle::~x3dcircle(){
	// cout << "[x3dcircle] delete x3dcircle" << endl;
}

int x3dcircle::update(){
	// cout << "[x3dcircle] x3dcircle::update" << endl;
	return 1;
}

int x3dcircle::render(){
}

bool x3dcircle::event(){
	cout << "[x3dcircle] x3dcircle::event" << endl;
	return false;
}

void x3dcircle::destroy(){
	// cout << "[x3dcircle] x3dcircle::destroy" << endl;
	node->remove();
}

void x3dcircle::setColour( xcolour c ){
	colour = c;
	node->setColour( c.getSColour() );
}

void x3dcircle::setPosition( x3dpoint p ){
    position = p;
    node->createCircle( position.getv3df(), radius, 40, core::vector3df(0,1,0) );
}

void x3dcircle::setRotation( x3dpoint r ){
    rotation = r;
    node->createCircle( position.getv3df(), radius, 40, rotation.getv3df() );
}

x3dpoint x3dcircle::getPosition(){
    return position;
}

x3dpoint x3dcircle::getRotation(){
    return rotation;
}

