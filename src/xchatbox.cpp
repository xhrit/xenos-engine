// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xchatbox.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xchatbox::xchatbox( irr::IrrlichtDevice *dev, int id, x2drect r ) : xelement( dev, id ){
	// cout << "[xchatbox] create xchatbox" << endl;

	// irr::gui::IGUIFont *font;
	chatbox = irr_env->addChatQueue( rect<s32>(r.X1, r.Y1, r.X2, r.Y2), 0, id ); // hmm, what one is id?

	// chatbox->setScrollModeLines(false); // ??

	element_reference = chatbox;

	setType( XELEMENT_TYPE_CHATBOX );
}

xchatbox::~xchatbox(){
	cout << "[xchatbox] delete xchatbox" << endl;
}

int xchatbox::update(){
	// cout << "[xchatbox] xchatbox::update" << endl;
	return 1;
}

int xchatbox::render(){
	// cout << "[xchatbox] xchatbox::render" << endl;
	return 1;
}

bool xchatbox::event(){
	// cout << "[xchatbox] xchatbox::event" << endl;
	return false;
}

void xchatbox::destroy(){
	// cout << "[xchatbox] xchatbox::destroy" << endl;
	chatbox->remove();
}

void xchatbox::addMessage( char * txt, xcolour c ){
	swdata  = txt;
	wsdata  = swdata.c_str();
	chatbox->addMessage( wsdata, CQS_USEDEFAULT, c.getSColour() );
} // xchatbox::addMessage

void xchatbox::setLife( int i ){
    chatbox->setLife(i);
} // xchatbox::setLife

void xchatbox::setFont( xfont * f ){
	chatbox->setFont( f->getFont() );
}// xchatbox::setFont

void xchatbox::setVisible( bool v ){
	// cout << "[xtext" << element_id <<"] setVisible" << endl;
	chatbox->setVisible( v );
} // xchatbox::setVisible

void xchatbox::setParent( xelement * x ){
	parent = x;
	chatbox->setParent( x->getGfxElement() );
} // xchatbox::setParent
