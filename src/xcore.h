// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xcore.h

#ifndef XCORE_H
#define XCORE_H

extern "C" {
#include "lua.h"            // Scripting
#include "lualib.h"         // ''
#include "lauxlib.h"        // ''
}

#include <irrlicht.h>       // 3d/Graphics
#include "xtimer.h"         // timer
#include "profiler.h"       // for benchmarking
#include "xsimulation.h"    // root simulation object
#include "xsql.h"           // SQL subsystem
#include "xnet.h"           // networking subsystem
#include "xaudio.h"         // sound subsystem
#include "xdynamics.h"      // physics subsystem


#include <iostream>         //
#include <cstdlib>          //
#include <fstream>          //

using namespace std;
using namespace irr;

/** \mainpage

xenjine.

*/

///
/// xcore : component interface
///
/// This object interfaces the simulation with 3rd party libs.
///

class xcore {
	protected:
		irr::u32 m_time ;                     ///< current time, updated every Run()
		lua_State *pLuaState;                 ///< lua state pointer

	public:
		///
		/// constructor.
		///
		xcore();

		///
		/// destructor.
		///
		~xcore();

		///
		/// init seq
		/// @return success
		///
		bool init(char * m);

		///
		/// init seq
		/// @return success
		///
		bool update();

		///
		/// init seq
		/// @return success
		///
		void render();

		///
		/// update seq
		/// @return the results
		///
		bool event ( irr::SEvent event );
		int state;

	private:
		void destroy();
		int lastFPS;
		int gamestate;
		std::string module;
		std::string path;

		float delta, accumulator, step; //666f;
		xtimer *core_timer;
};

#endif // XCORE_H
