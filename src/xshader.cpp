// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xshader.h"

#include <iostream>
#include <cstdlib>

xshader::xshader( irr::IrrlichtDevice *dev ) {
	cout << "[xshader] new xshader" << endl;
	irr_device = dev;                                    // collect irrlicht driver pointer
	irr_driver = irr_device->getVideoDriver();           // collect irrlicht driver pointer
}

xshader::~xshader() {}

int xshader::addMaterial( char * psFile, char * vsFile, int baseMaterial ){
	// cout << "[xshader] addMaterial" << endl;
	// create materials
	video::IGPUProgrammingServices* gpu = irr_driver->getGPUProgrammingServices();
	s32 r;

	if (gpu) {

		shaderCallBack* sc = new shaderCallBack();
		sc->init( irr_device );

		// create material from high level shaders (hlsl or glsl)
		 r = gpu->addHighLevelShaderMaterialFromFiles(
			vsFile, "vertexMain", video::EVST_VS_1_1,
			psFile, "pixelMain", video::EPST_PS_1_1,
			sc, (E_MATERIAL_TYPE)baseMaterial );

		sc->drop();
		return (int)r;
	} else {
		cout << "[xshader] error, no gpu programming services" << endl;
	}
}
