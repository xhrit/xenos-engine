// xengine Lukas Irwin [ sysop@xhrit.com ]
// copyright (c) 2007
// This program is distributed under the terms of the GNU General Public License
// xtab.h

#ifndef XTAB_H
#define XTAB_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xtab : static text
///
/// This is a static text element.
///

class xtab : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param txt string.
		/// \param x1 an integer argument.
		/// \param x2 an integer argument.
		/// \param y1 an integer argument.
		/// \param y21 an integer argument.
		///
		xtab( irr::IrrlichtDevice *dev, int id, x2drect r );

		///
		/// destructor.
		///
		~xtab();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();



	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;            // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
};

#endif // XTAB_H
