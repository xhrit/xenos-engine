// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XBODY_H
#define XBODY_H

#define GEOMSPERBODY   32   // maximum number of geometries per body
#define JOINTSPERBODY 10   // maximum number of joints per body
#define JOINTGROUPSPERBODY 10   // maximum number of joints per body
#define MAX_CONTACTS  10   // maximum number of ODE contacts

#include <ode/ode.h>
#include "xelement.h"

///
/// xbody : physics body
///
class xbody {
	public:

		///
		/// constructor.
		///
		xbody();

		///
		/// destructor.
		///
		~xbody();

		void addForce( x3dpoint f );
		void addRelForce( x3dpoint f );
		void addRelForceAtRelPos( x3dpoint f, x3dpoint p );

		void setLinearVel( x3dpoint v );
		void setAngularVel( x3dpoint v);
		x3dpoint getLinearVel();
		x3dpoint getAngularVel();


		void setID(int i);
		int getID();

		dBodyID getBody();
		dMass * getMass();
		dGeomID getGeom( int i );
		dJointID getJoint( int i );
		dJointGroupID getJointGroup( int i );

		void setBody(dBodyID b);
		void setMass( dMass m );
		void setGeom( dGeomID g );
		void setJoint( dJointID j );
		void setJointGroup( dJointGroupID j );

        int getStatic();
        void setStatic( int i );

		int getGeomCount();
		int getJointCount();
		void setGeomCount(int i );
		void setJointCount(int i );

		x3dpoint getPosition();
		x3dpoint getRotation();
		void setPosition( x3dpoint p );
		int getEnabled();
		void setEnabled(int i);

		void setMU( float f );
		void setBounce( float f );
		void setSoftERP( float f );
		void setSoftCFM( float f );

		float getMU();
		float getBounce();
		float getSoftCFM();
		float getSoftERP();

		void alignToAxis();
		void damp( float f );

        void setCategoryBits( float bits );
        void setCollideBits( float bits );

		// private ?
		x3dpoint quaternionToEuler( const dQuaternion quaternion );
		void eulerToQuaternion(const irr::core::vector3df &euler, dQuaternion quaternion);

	private:
		int bodyID;                                   // id ov this body
		dBodyID body;                                 // the dynamics body
		dMass mass;                                   // mass ov dynamics body
		dGeomID geom[GEOMSPERBODY];                   // geometries representing this body
		dReal offset[GEOMSPERBODY][3];                // offset?
		dJointGroupID jointGroup[JOINTGROUPSPERBODY]; // joint groups
		dJointID joint[JOINTSPERBODY];                // I like joints
		dMass compositeMass, componentMass;           // Accumulator for masses ov encapsulated geometries
		int geomCount, jointCount, jointGroupCount;

		// object data for material interaction (wood, rubber, cement, etc)
		float mu, bounce, softERP, softCFM;
        int is_static;
};

#endif // XBODY_H
