/*********************************************************************************
 **Copyright (C) 2008 by Daniel Sudmann						**
 *********************************************************************************/

#ifndef GRID_H_INCLUDED
#define GRID_H_INCLUDED

#include <ILogger.h>
#include <irrlicht.h>

class CGrid : public irr::scene::ISceneNode
{
public:
    class force
    {
    public:
        force(irr::core::vector2df pos, irr::f32 size, irr::f32 radius, irr::f32 time);
        void affect(irr::core::vector2d<irr::f32> pos, irr::core::vector2d<irr::f32>& speed, irr::f32 diff);
        irr::f32 Time;
    protected:
        irr::core::vector2df Pos;
        irr::f32 Size;
        irr::f32 Radius;
    };

    CGrid(irr::u32 a, irr::u32 b, irr::scene::ISceneManager* manager, irr::core::dimension2d<irr::s32> size = irr::core::dimension2d<irr::s32>(1000, 1000));

    ~CGrid(void);

    void render(void);

    void addForce(irr::core::vector2df pos, irr::f32 strenght, irr::f32 radius, irr::f32 time = 0.1f);

    virtual void OnRegisterSceneNode(void);

    virtual void OnAnimate(irr::u32 timeMs);

    virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const;

    virtual irr::u32 getMaterialCount() const;

    virtual irr::video::SMaterial& getMaterial(irr::u32 i);

    irr::core::vector2df getBorder(void);

    void setColor(irr::video::SColor color);

    void setGridCalc(bool special);
protected:
    irr::u32 A;
    irr::u32 B;
    irr::f32 diff;
    irr::core::vector2d<irr::f32>* Points;
    irr::core::vector2d<irr::f32>* PointOffset;
    irr::core::vector2d<irr::f32>* PointSpeed;
    irr::core::array<force> Forces;
    irr::video::IVideoDriver* Driver;
    irr::u32 last_time;
    irr::video::SMaterial Material;
    irr::core::aabbox3d<irr::f32> Box;
	irr::core::vector2df Border;

	bool SpecialGrid;
};

#endif // GRID_H_INCLUDED
