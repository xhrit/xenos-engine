// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xtext.cpp

#include "xtext.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

// todo: fix this
// xtext::xtext( irr::IrrlichtDevice *dev, int id, char * c x2drect r ) : xelement( dev, id ){
xtext::xtext( irr::IrrlichtDevice *dev, int id, char * c, int x1, int x2, int y1, int y2 ) : xelement( dev, id ){
	// cout << "[xtext" << id <<"] create xtext" << endl;

	stream.str("");
	stream.clear();
	swdata  = c;
	wsdata  = swdata.c_str();

	text  = irr_env->addStaticText(wsdata, rect<int>( x1, x2, y1, y2 ), true);
	text->setOverrideColor   ( video::SColor(255, 255, 255, 255) );
	text->setDrawBorder( false );
	element_reference = text;

	setType( XELEMENT_TYPE_TEXT );

	w = y1 - x1;
	h = y2 - x2;
    // cout << "[xtext] w:" << w << endl;
    // cout << "[xtext] h:" << h << endl;

}

xtext::~xtext(){
	cout << "[xtext" << element_id <<"] delete xtext" << endl;
}

int xtext::update(){
	// cout << "[xtext" << element_id <<"] xtext::update" << endl;
	return 1;
}

int xtext::render(){
	// cout << "[xtext" << element_id <<"] xtext::render" << endl;
	return 1;
}

bool xtext::event(){
	// cout << "[xtext" << element_id <<"] xtext::event" << endl;
	return false;
}

void xtext::destroy(){
	// cout << "[xtext" << element_id <<"] xtext::destroy" << endl;
	text->remove();
}

void xtext::setVisible( bool v ){
	// cout << "[xtext" << element_id <<"] setVisible" << endl;
	text->setVisible( v );
} // xtext::setVisible

void xtext::setParent( xelement * x ){
	// cout << "[xtext" << element_id <<"] setParent" << endl;
	parent = x;
	text->setParent( x->getGfxElement() );
} // xtext::setParent

char * xtext::getText(){
	// cout << "[xtext" << element_id <<"] xtext::getText" << endl;
	core::stringc s = text->getText();
	return strdup( s.c_str() );
} // xtext::getText

void xtext::setText( char * c ){
	// cout << "[xtext" << element_id <<"] xtext::setText" << endl;
	swdata  = c;
	wsdata  = swdata.c_str();
	text->setText( wsdata );
} // xtext::setText

void xtext::setColour( xcolour c ){
	text->setOverrideColor   ( c.getSColour() );
}

void xtext::setFont( xfont * f ){
	text->setOverrideFont( f->getFont() );
}

void xtext::setAlignment( int h, int v ){
	text->setTextAlignment( (EGUI_ALIGNMENT)h, (EGUI_ALIGNMENT)v );
}

void xtext::setPosition( x2dpoint p ){
	// cout << "[xtext] setPosition" << endl;
	// position = p;
	text->setRelativePosition ( irr::core::rect<int>( p.X, p.Y, p.X+w, p.Y+h ) );
} // xtext::setPosition

