// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xtimer.h

#ifndef XTIMER_H
#define XTIMER_H

#ifdef _WIN32
	#include <time.h>
	#include <windows.h>
#endif

// #ifdef  _POSIX
	#include <sys/time.h> // for gettimeofday()
// #endif

#include <iostream>         //
#include <cstdlib>          //
#include <fstream>          //

using namespace std;

class xtimer {
	 public:
		xtimer(){
			reset(); // init timer
		};

		struct timeval t1, t2, t3; //start time, current time, lat update.
		double elapsedTime, deltaTime; // time since start, time since last update

		float getTime(){
			return (float)elapsedTime;
		};

		float getFactor(){
			return (float)deltaTime;
		};

		void reset() {
			// cout << "Resetting timer" << endl;
			gettimeofday( &t1, 0);
			gettimeofday( &t2, 0);
			gettimeofday( &t3, 0);
			elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
			elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
			// cout << "time since start :" << elapsedTime << endl;
		};

		void update(){
			gettimeofday(&t3, 0);
			deltaTime = (t3.tv_sec - t2.tv_sec) * 1000.0;      // sec to ms
			deltaTime += (t3.tv_usec - t2.tv_usec) / 1000.0;   // us to ms
			// cout << "time since last frame :" << deltaTime << endl;

			gettimeofday(&t2, 0);
			elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
			elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
			// cout << "time since start :" << elapsedTime << endl;
		};
};

#endif // XTIMER_H
