// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xsimulation.h

#ifndef XSIMULATION_H
#define XSIMULATION_H

#include "xelement.h"       // Simulation object element
#include "xtext.h"          // Static text element
#include "xlight.h"         // light element
#include "xcamera.h"        // Camera element
#include "xbone.h"          // skeletal bone element
#include "xmesh.h"          // Mesh element
#include "xcubemesh.h"      // Cube element
#include "xspheremesh.h"    // Sphere element
#include "xterrainmesh.h"   // Terrain Mesh element
#include "xevent.h"         // event object ( ? )
#include "xparticlesystem.h"// particles
#include "xskybox.h"        // skybox element
#include "xgrid.h"          // grid element
#include "xbeam.h"          // quad node
#include "xbillboard.h"     // ...
#include "xsprite.h"        // multi-layer sprite
#include "xtextnode.h"      // 3d text in 3d space
#include "xcheckbox.h"      // a box. to check.
#include "xtabcontrol.h"    // a tab bar, that tabs can be added to.

#include "xbutton.h"        // button element
#include "xwindow.h"        // window element
#include "xscrollbar.h"     // scrollbar element
#include "xlistbox.h"       // listbox element
#include "xcombobox.h"      // combo element
#include "xcontextmenu.h"   // menu bar element
#include "xtable.h"         // table element
#include "xtextbox.h"       // textbox element
#include "xchatbox.h"       // chatbox element
#include "xeditbox.h"       // editbox element
#include "x3dline.h"        // 3d line element
#include "x3dcircle.h"      // 3d circle element
#include "ximage.h"         // gui image element
#include "xshader.h"        // shader loader

#include "XEffects.h"

#include "PostProscessBloom.h"        // shader loader
#include "PostProscessMotionBlur.h"        // shader loader

#include <irrlicht.h>       // 3d/Graphics
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

enum XDRIVER_TYPE {
	XDT_NULL,
	XDT_SOFTWARE,
	XDT_BURNINGSVIDEO,
	XDT_DIRECT3D8,
	XDT_DIRECT3D9,
	XDT_OPENGL
};

class xkeybind {
	public:
		xkeybind(){};
		~xkeybind ();
		const char * action;
		XKEY_EVENT_TYPE key_event;
};

class xguibind {
	public:
		xguibind(){};
		~xguibind ();
		const char * action;
		int id;
		XGUI_EVENT_TYPE gui_event;
};

///
/// simulation object
///
/// This is the root node.
///

class xsimulation {
	protected:
		int val1; ///<  value val1.
		irr::IrrlichtDevice       * irr_device;    ///< irrlicht device pointer
		irr::video::IVideoDriver  * irr_driver;    ///< irrlicht video driver pointer
		irr::scene::ISceneManager * irr_smgr;      ///< irrlicht scene manager pointer
		irr::gui::IGUIEnvironment * irr_env;       ///< irrlicht gui environment pointer

	public:
		///
		/// constructor.
		///
		xsimulation ( XDRIVER_TYPE dtype = XDT_SOFTWARE, int width = 512, int height = 384,
			int depth = 16, bool fs = false, bool sbuff = false, bool vsynch = false );

		///
		/// destructor.
		///
		~xsimulation ();

		///
		/// getDevice
		/// @return IrrlichtDevice
		///
		irr::IrrlichtDevice       * getDevice () {return irr_device; }

		///
		/// getDriver
		/// @return IVideoDriver
		///
		irr::video::IVideoDriver  * getDriver () {return irr_driver; }

		///
		/// getSceneMgr
		/// @return ISceneManager
		///
		irr::scene::ISceneManager * getSceneMgr () { return irr_smgr; }

		///
		/// getEnv
		/// @return IGUIEnvironment
		///
		irr::gui::IGUIEnvironment * getEnv (){ return irr_env; }
		//start the irrlicht engine

		///
		/// update seq
		/// @return the results
		///
		int update ();

		///
		/// update seq
		/// @return the results
		///
		int prerender ();
		int render ();
		int postrender ();

		void setColour ( xcolour c );

		///
		/// update seq
		/// @return the results
		///
		bool event ( irr::SEvent sevent );

		///
		/// destroy simulation
		///
		void destroy ();

		void bindkey ( const char * a, XKEY_EVENT_TYPE e );
		bool isKey( const char * a );

		bool isKeyDown( const char * a );
		bool isKeyPressed( const char * a );
		bool isKeyUp( const char * a );

        void setKeyDelay( int d );
        void ignoreKeyRepeat( bool r );

		void bindGUI ( const char * a, int i, XGUI_EVENT_TYPE e );
		bool isGUI ( const char * a ); // event, id

		// bindMouse ( const char *a, XMOUSE_EVENT_TYPE e );
		// bool isMouse();

		void setWindowCaption(char * text);
		int getFPS();
		int getTris();
		void addArchive(char * text){
			irr_device->getFileSystem()->addZipFileArchive(text);
		}

		void addEvent(xevent * e );
		int getEventCount();
		void clearEventQue();
		xevent* getEvent();
        void removeEvent();
		xelement * getCaller( irr::gui::IGUIElement * c );

		///
		/// Methods for adding gui elements
		///
		xtext * addText(int id, char * c, int x1, int x2, int y1, int y2 ); // x2drect r
		xbutton * addButton( int id, x2drect r, char * c );
		xwindow * addWindow( int id, x2drect r, char * c );
		xtextnode * addTextNode( int id,  xfont * f, char * txt, xcolour c );
		xscrollbar * addScrollbar( int id, bool o, x2drect r );
		xlistbox * addListbox( int id, bool b, x2drect r );
		xcombobox * addCombobox( int id, x2drect r );
		xtable * addTable( int id, bool b, x2drect r );
		xeditbox * addEditbox( int id, x2drect r );
		xtextbox * addTextbox( int id, x2drect r );
		ximage * addImage( int id, xtexture * t, x2dpoint p, x2drect r, xcolour c );
		xcontextmenu * addContextMenu( int id, x2drect r );
		xcontextmenu * addMenu( int id );
		xcheckbox * addCheckbox(int id, bool b, char * c, int x1, int x2, int y1, int y2 ); // x2drect r
        xchatbox * addChatbox( int id, x2drect r );

		///
		/// Methods for adding simulation elements
		///
		xlight * addLight(  int id, x3dpoint pos, xcolour c, float radius );
		xcamera * addCamera(int id, XCAMERA_TYPE camtype,
			int x1, int y1, int z1, int x2, int y2, int z2 ); // x3dpoint pos, x3dpoint rot
		xmesh * addMesh(int id, char * mesh_file);
		xcubemesh * addCubeMesh(int id);
		xspheremesh * addSphereMesh(int id);
		xterrainmesh * addTerrainMesh(int id, char * mesh_file);
		xparticlesystem * addParticleSystem(int id, char * tex_file);
		xskybox * addSkybox(int id, char * filename1,
					char * filename2,
					char * filename3,
					char * filename4,
					char * filename5,
					char * filename6);
		x3dline * add3dLine( int id, x3dpoint s, x3dpoint e );
		x3dcircle * add3dCircle( int id, x3dpoint c, float r );
		xgrid * addGrid( int id, int sz, int num );
		xbeam * addBeam( int id, char * tx_fn1, char * tx_fn2  );
		xbillboard * addBillboard( int id, x2dpoint p );
		xsprite * addSprite( int id, char * tx_fn1, char * tx_fn2, char * tx_fn3, char * tx_fn4  );

		xshader * addShader();

		int getNodeIDFromScreenCoordinates( x2dpoint p );
		x3dpoint getIntersection( x2dpoint p,  int nid );
        x2dpoint getScreenCoordinatesFrom3DPoint( x3dpoint p ); // TODO / fixme : return a pointer?
        x3dline * get3dlineFromScreenCoordinates( x2dpoint p );

		///
		/// Methods for dealing with GUI
		///
		void setSkinColor ( XGUI_DEFAULT_COLOR which, xcolour c );
		xcolour * getSkinColor ( XGUI_DEFAULT_COLOR which );
		xtexture * getTexture(char * file);
		xfont * getFont(  char * file );
		void setFont( xfont  *f, int which );
		xfont * getBuiltInFont();
		void makeColorKeyTexture(xtexture * t, x2dpoint);

		void draw2DImage(xtexture * t, x2dpoint p, x2drect r, xcolour c);

		///
		/// sets timestep
		///
		void setTimestep( float f );

		void setAmbientLight( xcolour c );
		void setShadowColour( xcolour c );
		void setFog ( xcolour colour,  bool linear, float start, float end, float density, bool pixel, bool range );

		///
		/// Adds a pointer to an xelement object to the reference vector
		/// @param xelement* e
		///
		void addReference(xelement * e );

		///
		/// Prints a tree to the console.
		///
		void printTree();
		void printElement( xelement * e );

		// remove this after i get it sorted out.
		int renderWithBloom();
		int renderWithBlur();
		int initBloom( float dist, float str, float mult);
		int initBlur( float str);

		// xeffects
		void initShadows();
		void addShadowLight( int id, x3dpoint pos, x3dpoint tgt, xcolour c, float nv, float fv, float radius, bool directional );
		void setShadowLightPosition(int i, x3dpoint pos );
		void setShadowLightTarget(int i, x3dpoint pos );
		void addShadowToElement( xelement * e );
		void removeShadowFromElement( xelement * e );
		int renderWithShadows();
		void addPostProcessingEffect( char * filename );

	private:
		int val2;
		vector < xelement * > references;
		vector < xevent * > event_que;
		vector < xkeybind * > keys;
		vector < xguibind * > gui; // TODO: should be renamed?

		bool keybuffer[ XKEY_XKEY_CODES_COUNT ];
		bool keybuffer_old[ XKEY_XKEY_CODES_COUNT ];
		int keybuffer_delay[ XKEY_XKEY_CODES_COUNT ];

		int keyDelay;
        bool keyRepeat;

		void clearKeyBuffer();
        void updateKeyBuffer();          //

		vector <bool> guibuffer;         // GUI
		void clearGUIBuffer();           //

		// text conversion function?
		ostringstream stream;            // conversion variables (?)
		core::stringw swdata;            // conversion variables (?)
		const wchar_t * wsdata;           // conversion variables (?)
		float timestep;

		xcolour scene_colour;

		// TEST!!!!!!!!!
		IPostProcessBloom *Bloom;
		IPostProcessMotionBlur *Blur;
		EffectHandler* effect;
};

class eventReceiver : public IEventReceiver {
	protected:
		xsimulation * xsim;
	public:
		bool OnEvent ( const irr::SEvent& event );
		void setSim( xsimulation* sim ){ xsim = sim; }; //set simulation
};

#endif // XSIMULATION_H
