// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XSCRLLBAR_H
#define XSCRLLBAR_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xscrollbar : slide option element
///
/// This is a scrollbar. it can go up and down or side to side.
///

class xscrollbar : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param txt string.
		/// \param x1 an integer argument.
		/// \param x2 an integer argument.
		/// \param y1 an integer argument.
		/// \param y21 an integer argument.
		///
		xscrollbar( irr::IrrlichtDevice *dev, int id, bool o, x2drect r );

		///
		/// destructor.
		///
		~xscrollbar();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();
		void setParent( xelement * x );

        void setVisible( bool v );

		void setMax(int n){ scrollbar->setMax( n ); }
		float getMax(){ return scrollbar->getMax(); }

		void setMin(int n){ scrollbar->setMin( n ); }
		float getMin(){ return scrollbar->getMin(); }

		void setPos(int n){  scrollbar->setPos( n ); }
		float getPos(){ return scrollbar->getPos(); }

		void setSmallStep(int n){  scrollbar->setSmallStep( n ); }
		float getSmallStep(){ return scrollbar->getSmallStep(); }

		void setLargeStep(int n){  scrollbar->setLargeStep( n ); }
		float getLargeStep(){ return scrollbar->getLargeStep(); }

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;            // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		IGUIScrollBar* scrollbar;

};

#endif // XSCRLLBAR_H

