// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xcamera.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xcamera::xcamera(  irr::IrrlichtDevice *dev, int id, XCAMERA_TYPE camtype,
			int x1, int y1, int z1, int x2, int y2, int z2 ) : xelement( dev, id ){
	// cout << "[xcamera] create xcamera" << endl;
	type = camtype;
	if (type == CAMERA_STATIC){
		camera_static = irr_smgr->addCameraSceneNode( 0, vector3df( x1, y1, z1 ), vector3df( x2, y2, z2 ) );
	} else if (type == CAMERA_FPS){
		camera_fps = irr_smgr->addCameraSceneNodeFPS();
	} else if (type == CAMERA_RTS){
		cout << "[xcamera] CAMERA_RTS" << endl;
		// camera_rts = new scene::xrtscam( irr_smgr->getRootSceneNode(), irr_smgr, -1 );
		// if ( (*m_v)[focused_unit].proto.strMeshtype == "animated" ){
		// 			(*m_sim).rtscamera->setParent( (*m_v)[focused_unit].proto.anim_node );
		// } else {
		// 				(*m_sim).rtscamera->setParent( (*m_v)[focused_unit].proto.node );
		// 			}
		// camera_rts->setTarget( vector3df( x2, y2, z2 ) );
	} else if (camtype == CAMERA_RPG){
		cout << "[xcamera] CAMERA_RPG not implemented" << endl;
	} else if (camtype == CAMERA_COCKPIT){
		cout << "[xcamera] CAMERA_COCKPIT not implemented" << endl;
	}
	setType( XELEMENT_TYPE_CAMERA );
}

xcamera::~xcamera(){
	// cout << "[xcamera] xcamera destructor" << endl;
}

int xcamera::update(){
	// cout << "[xcamera] xcamera::update" << endl;
	return 1;
}

int xcamera::render(){
	// cout << "[xcamera] xcamera::render" << endl;
	return 1;
}

bool xcamera::event(){
	// cout << "[xcamera] xcamera::event" << endl;
	return false;
}

void xcamera::destroy(){
	// cout << "[xcamera] xelement::destroy" << endl;
	// TODO: remove node
	if (type == CAMERA_STATIC){
        camera_static->remove();
    } else if (type == CAMERA_FPS){
		camera_fps->remove();
	} else if (type == CAMERA_RTS){
		cout << "[xcamera] CAMERA_RTS" << endl;
		// camera_rts = new scene::xrtscam( irr_smgr->getRootSceneNode(), irr_smgr, -1 );
		// if ( (*m_v)[focused_unit].proto.strMeshtype == "animated" ){
		// 			(*m_sim).rtscamera->setParent( (*m_v)[focused_unit].proto.anim_node );
		// } else {
		// 				(*m_sim).rtscamera->setParent( (*m_v)[focused_unit].proto.node );
		// 			}
		// camera_rts->setTarget( vector3df( x2, y2, z2 ) );
	} else if (type == CAMERA_RPG){
		cout << "[xcamera] CAMERA_RPG not implemented" << endl;
	} else if (type == CAMERA_COCKPIT){
		cout << "[xcamera] CAMERA_COCKPIT not implemented" << endl;
	}

	setType( XELEMENT_TYPE_NULL );
}

void xcamera::setTarget( x3dpoint p ){
	// cout << "[xcamera] xcamera::setTarget" << endl;
	if (type == CAMERA_STATIC){
		camera_static->setTarget( p.getv3df() );
	}
}

void xcamera::setPosition( x3dpoint p ){
	// cout << "[xcamera] xcamera::setPosition" << endl;
	if (type == CAMERA_STATIC){
		camera_static->setPosition( p.getv3df() );
	} else if (type == CAMERA_FPS){
		camera_fps->setPosition( p.getv3df() );
	}
	position = p;
}

x3dpoint xcamera::getPosition(){
	vector3df p;
	if (type == CAMERA_STATIC){
		p = camera_static->getPosition();
	} else if (type == CAMERA_FPS){
		p = camera_fps->getPosition();
	}
	return x3dpoint( p.X, p.Y, p.Z );
}

void xcamera::setFOV( float fov ){
	if (type == CAMERA_STATIC){
		camera_static->setFOV( fov );
	} else if (type == CAMERA_FPS){
		camera_fps->setFOV( fov );
	}
}

void xcamera::setAspectRatio( float a ){
	if (type == CAMERA_STATIC){
		camera_static->setAspectRatio( a );
	} else if (type == CAMERA_FPS){
		camera_fps->setAspectRatio( a );
	}
}

void xcamera::setFarValue( float v){
	if (type == CAMERA_STATIC){
		camera_static->setFarValue( v );
	} else if (type == CAMERA_FPS){
		camera_fps->setFarValue( v );
	}
}

void xcamera::setOrthoMode( float a, float b, float c, float d){
    // cout << "[xcamera] setOrthoMode" << endl;
    // Add the camera and set the Orthogonal Matrix
    irr::core::matrix4 m;
    m.buildProjectionMatrixOrthoLH( a, b, c, d );
    if (type == CAMERA_STATIC){
        camera_static->setProjectionMatrix( m );
    } else if (type == CAMERA_FPS){
        camera_fps->setProjectionMatrix( m );
    }
}
