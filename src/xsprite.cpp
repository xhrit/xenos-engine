// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xsprite.cpp

#include "xsprite.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xsprite::xsprite( irr::IrrlichtDevice *dev, int id, char * tx_fn1, char * tx_fn2, char * tx_fn3, char * tx_fn4 ) : xelement( dev, id ){
	// cout << "[xsprite] create xsprite" << endl;
	node = new scene::CSprite( 0, irr_smgr, id, tx_fn1, tx_fn2, tx_fn3, tx_fn4 );

	node->setID( id );
	// node->setIsDebugObject( true );
	// node->setDebugDataVisible ( EDS_BBOX );

	// setPosition( x3dpoint( 0, 0, 0 ) );
	setType( XELEMENT_TYPE_BILLBOARD ); // hmm.
	node_reference = node;
	vis = true;
}

xsprite::~xsprite(){
	// cout << "[xsprite] delete xsprite" << endl;
}

int xsprite::update(){
	// cout << "[xsprite] xsprite::update" << endl;
	return 1;
}

int xsprite::render(){
	// cout << "[xsprite] xsprite::render" << endl;
	if ( getType() == XELEMENT_TYPE_BILLBOARD ){
	    if ( vis == true){
            node->OnRegisterSceneNode();
	    }

    }
	return 1;
} // xsprite::render

bool xsprite::event(){
	// cout << "[xsprite] xsprite::event" << endl;
	return false;
} // xsprite::event

void xsprite::destroy(){
	// cout << "[xsprite] xsprite::destroy" << endl;
	node->remove();
    // delete node;
    setType( XELEMENT_TYPE_NULL );
} // xsprite::destroy

x3dpoint xsprite::getPosition(){
	vector3df p = node->getPosition();
	return x3dpoint ( p.X, p.Y, p.Z );
}

x2dpoint xsprite::getSize(){
	// vector2df p = node->getSize();
	return x2dpoint ( 0, 0 );
}

void xsprite::setPosition( x3dpoint p){
	// cout << "[xsprite] setPosition" << endl;
	position = p;
	node->setPosition ( p.getv3df() );
} // xsprite::setPosition

void xsprite::setSize(x2dpoint p) {
	// scale = p;
	node->setSize ( core::dimension2d<f32> ( p.X, p.Y) );
} // xsprite::setScale

void xsprite::setVisible( bool val ){
	node->setVisible ( val );
	vis = val;
} // xsprite::setVisible


void xsprite::setMaterialFlag( XMATERIAL_FLAG mf, bool val ){
	node->setMaterialFlag ( (E_MATERIAL_FLAG)mf, val );

} // xsprite::setMaterialFlag

// TODO: fixme? do something about this :
// node->getMaterial(0).AmbientColor = ambient.getSColour();
// node->getMaterial(0).DiffuseColor = diffuse.getSColour();
// node->getMaterial(0).SpecularColor = specular.getSColour();

void xsprite::setMaterialType( XMATERIAL_TYPE mt ){ //
	// cout << "[xsprite] xsprite::setMaterialType" << endl;
	video::IMaterialRenderer* renderer = irr_driver->getMaterialRenderer( (E_MATERIAL_TYPE)mt );
	// display some problem text when problem
	if (!renderer || renderer->getRenderCapability() != 0){
		// cout << "[xsprite] unable to render effect: " << mt << endl;;
	} else {
		node->setMaterialType( (E_MATERIAL_TYPE)mt );
	}

} // xsprite::setMaterialType

void xsprite::setTexture( char * tex, int layer ){
    node->setTexture( tex, layer );
}

void xsprite::setTextureMatrix( xmatrix4 m ){
	// cout << "[xsprite] setTextureMatrix" << endl;
    node->getMaterial(0).setTextureMatrix(0, m.m);
    node->getMaterial(1).setTextureMatrix(0, m.m);
    node->getMaterial(2).setTextureMatrix(0, m.m);
    node->getMaterial(3).setTextureMatrix(0, m.m);

} // xsprite::setTextureMatrix

