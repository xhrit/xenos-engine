// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xtextnode.h

#ifndef XTEXTNODE_H
#define XTEXTNODE_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xtextnode : 3d mesh
///
/// This is a static 3d mesh element. It supports many file formats.
///

class xtextnode : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param mesh character array ov mesh to load.
		/// \param tex texture to load.
		///
		xtextnode( irr::IrrlichtDevice *dev, int id, xfont * f, char* txt, xcolour c  );

		///
		/// destructor.
		///
		~xtextnode();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint p );
		void setScale( x3dpoint p );

		void setText( char * c );
        void setColour( xcolour c  );

		x3dpoint getPosition();
		x3dpoint getRotation();
		x3dpoint getScale();

		ITextSceneNode* node;

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;      // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		x3dpoint scale;
};

#endif // XTEXTNODE_H
