// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xparticlesystem.cpp

#include "xparticlesystem.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xparticlesystem::xparticlesystem( irr::IrrlichtDevice *dev, int id, char * tex_fn ) : xelement( dev, id ){
	// cout << "[xparticlesystem] create xparticlesystem" << endl;

	node = irr_smgr->addParticleSystemSceneNode( false, 0 , id );
	node->setMaterialTexture(0, irr_driver->getTexture( tex_fn ) );

	node_reference = node;
	setType( XELEMENT_TYPE_PARTICLESYSTEM );
}

xparticlesystem::~xparticlesystem(){
	cout << "[xparticlesystem] delete xparticlesystem" << endl;
}

int xparticlesystem::update(){
	// cout << "[xparticlesystem] xparticlesystem::update" << endl;
	return 1;
}

int xparticlesystem::render(){
	// cout << "[xparticlesystem] xparticlesystem::render" << endl;
	return 1;
} // xparticlesystem::render

bool xparticlesystem::event(){
	// cout << "[xparticlesystem] xparticlesystem::event" << endl;
	return false;
} // xparticlesystem::event

void xparticlesystem::destroy(){
	// cout << "[xparticlesystem] xparticlesystem::destroy" << endl;
	node->remove();
	setType( XELEMENT_TYPE_NULL );
} // xparticlesystem::destroy

void xparticlesystem::setPosition( x3dpoint p){
	// cout << "[xparticlesystem] setPosition" << endl;
	position = p;
	node->setPosition ( p.getv3df() );
} // xparticlesystem::setPosition

void xparticlesystem::setRotation( x3dpoint p){
	// cout << "[xparticlesystem] setRotation" << endl;
	rotation = p;
	node->setRotation ( p.getv3df() );
} // xparticlesystem::setRotation

void xparticlesystem::setScale(x3dpoint p) {
	scale = p;
	node->setScale ( p.getv3df() );
} // xparticlesystem::setScale

void xparticlesystem::setMaterialFlag( XMATERIAL_TYPE mt, bool val ){
	node->setMaterialFlag ( (E_MATERIAL_FLAG)mt, val );
} // xparticlesystem::setMaterialFlag

void xparticlesystem::setMaterialType( XMATERIAL_TYPE mt ){ //
	// cout << "[xmesh] xmesh::setMaterialType" << endl;
	video::IMaterialRenderer* renderer = irr_driver->getMaterialRenderer( (E_MATERIAL_TYPE)mt );

	// display some problem text when problem
	if (!renderer || renderer->getRenderCapability() != 0){
		cout << "[xmesh] unable to render effect: " << mt << endl;;
	} else {
		node->setMaterialType( (E_MATERIAL_TYPE)mt );
	}

} // xparticlesystem::setMaterialType

void xparticlesystem::addTexture( char * fn, int index ){
	// cout << "[xmesh] addTexture: " << tex_fn << endl;
	node->setMaterialTexture( index, irr_driver->getTexture( fn ) );
} // xparticlesystem::addTexture

void xparticlesystem::createPointEmitter( x3dpoint direction, int minpps, int maxpps, xcolour mincolour, xcolour maxcolour, int minlife, int maxlife, float angle, float minsize, float maxsize ){
	emitter = node->createPointEmitter(
		direction.getv3df(),
		minpps, maxpps,
		mincolour.getSColour(),
		maxcolour.getSColour(),
		minlife, maxlife, angle,
		core::dimension2df(minsize, minsize),
		core::dimension2df(maxsize, maxsize));
	node->setEmitter(emitter); // this grabs the emitter
	emitter->drop(); // so we can drop it here without deleting it
} // xparticlesystem::createPointEmitter

void xparticlesystem::addSphereEmitter(x3dpoint center, float radius, x3dpoint direction, int minpps, int maxpps, xcolour mincolour, xcolour maxcolour, int minlife, int maxlife, float angle, float minsize, float maxsize){
	emitter = node->createSphereEmitter(
		center.getv3df(), radius,
		direction.getv3df(),
		minpps, maxpps,
		mincolour.getSColour(),
		maxcolour.getSColour(),
		minlife, maxlife, angle,
		core::dimension2df(minsize, minsize),
		core::dimension2df(maxsize, maxsize));
	node->setEmitter(emitter); // this grabs the emitter
	emitter->drop(); // so we can drop it here without deleting it
} // xparticlesystem::addSphereEmitter

void xparticlesystem::addRingEmitter(x3dpoint center, float radius, float thickness, x3dpoint direction, int minpps, int maxpps, xcolour mincolour, xcolour maxcolour, int minlife, int maxlife, float angle, float minsize, float maxsize){
	emitter = node->createRingEmitter(
		center.getv3df(), radius, thickness,
		direction.getv3df(),
		minpps, maxpps,
		mincolour.getSColour(),
		maxcolour.getSColour(),
		minlife, maxlife, angle,
		core::dimension2df(minsize, minsize),
		core::dimension2df(maxsize, maxsize));
	node->setEmitter(emitter); // this grabs the emitter
	emitter->drop(); // so we can drop it here without deleting it
} // xparticlesystem::addRingEmitter

void xparticlesystem::addBoxEmitter( x3dpoint direction, int minpps, int maxpps, xcolour mincolour, xcolour maxcolour, int minlife, int maxlife, float angle, float minsize, float maxsize ){
	emitter = node->createBoxEmitter(
		core::aabbox3d<f32>(-1,0,-1, 1,1,1), // emitter size
		direction.getv3df(),
		minpps, maxpps,
		mincolour.getSColour(),
		maxcolour.getSColour(),
		minlife, maxlife, angle,
		core::dimension2df(minsize, minsize),
		core::dimension2df(maxsize, maxsize));

	node->setEmitter(emitter); // this grabs the emitter
	emitter->drop(); // so we can drop it here without deleting it
} // xparticlesystem::addBoxEmitter

void xparticlesystem::addFadeOutAffector(xcolour colour, float time){
	scene::IParticleAffector* paf = node->createFadeOutParticleAffector( colour.getSColour(), time );
	node->addAffector(paf); // same goes for the affector
	paf->drop();
} // xparticlesystem::addFadeOutAffector

void xparticlesystem::addAttractionAffector( x3dpoint point, float speed, bool attract ){
	scene::IParticleAffector* paf = node->createAttractionAffector( point.getv3df(), speed, attract );
	node->addAffector(paf); // same goes for the affector
	paf->drop();
} // xparticlesystem::addAttractionAffector

void xparticlesystem::addRotationAffector( x3dpoint speed, x3dpoint pivot ){
	scene::IParticleAffector* paf = node->createRotationAffector( speed.getv3df(), pivot.getv3df() );
	node->addAffector(paf); // same goes for the affector
	paf->drop();
} // xparticlesystem::RotationAffector

void xparticlesystem::addGravityAffector( x3dpoint gravity, int force ){
	scene::IParticleAffector* paf = node->createGravityAffector (gravity.getv3df(), (u32)force);
	node->addAffector(paf); // same goes for the affector
	paf->drop();
} // xparticlesystem::addGravityAffector

void xparticlesystem::addScaleAffector( x2dpoint scaleTo ){
    scene::IParticleAffector* paf = node->createScaleParticleAffector( core::dimension2df( scaleTo.X, scaleTo.Y ));
	node->addAffector(paf); // same goes for the affector
	paf->drop();
} // xparticlesystem::addScaleAffector


void xparticlesystem::setMinParticlesPerSecond( int n ){
	// cout << "[xparticlesystem] setMinParticlesPerSecond :" << n << endl;
	node->getEmitter()->setMinParticlesPerSecond( n );
} // xparticlesystem::setMinParticlesPerSecond

void xparticlesystem::setMaxParticlesPerSecond( int n ){
	// cout << "[xparticlesystem] setMinParticlesPerSecond:" << n << endl;
	node->getEmitter()->setMaxParticlesPerSecond( n );
} // xparticlesystem::setMaxParticlesPerSecond



