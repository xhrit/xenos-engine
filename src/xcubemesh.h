// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xcubemesh.h

#ifndef XCUBEMESH_H
#define XCUBEMESH_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xcubemesh : 3d mesh
///
/// This is a static 3d mesh element. It supports many file formats.
///

class xcubemesh : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param mesh character array ov mesh to load.
		/// \param tex texture to load.
		///
		xcubemesh( irr::IrrlichtDevice *dev, int id );

		///
		/// destructor.
		///
		~xcubemesh();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		void setPosition( x3dpoint p );
		void setRotation( x3dpoint p );
		void setScale( x3dpoint p );

        void setVisible( bool val );

		void setMaterialFlag( XMATERIAL_FLAG mf, bool val );
		void setMaterialType( XMATERIAL_TYPE mt );
		void addNormalMap( char * bump, float factor );
		void addTexture( char * fn, int index );
		void createTriangleSelector();

		x3dpoint getPosition(){ return position; };
		x3dpoint getRotation(){ return rotation; };
		x3dpoint getScale(){ return scale; };

		IMeshSceneNode* node;
		ITriangleSelector* selector;

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;      // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		x3dpoint scale;
		x3dpoint rotation;
		x3dpoint position;
};

#endif // XCUBEMESH_H
