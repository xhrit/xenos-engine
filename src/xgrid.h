// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xgrid.h

#ifndef XGRID_H
#define XGRID_H

#include "xelement.h"       // Simulation objects
#include "CGrid.h"          // Sudi's grid

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xgrid : 3d mesh
///
/// This is a static 3d mesh element. It supports many file formats.
///

class xgrid : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param mesh character array ov mesh to load.
		/// \param tex texture to load.
		///
		xgrid( irr::IrrlichtDevice *dev, int id, int sz, int num );

		///
		/// destructor.
		///
		~xgrid();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		void addForce( x2dpoint p, float str, float radius, float time );
        void setColour( xcolour c );

        void setPosition( x3dpoint p );

	private:
		CGrid * grid;

};

#endif // XGRID_H
