// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// text.h

#ifndef XCAMERA_H
#define XCAMERA_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace irr;

enum XCAMERA_TYPE {
	CAMERA_STATIC,
	CAMERA_FPS,
	CAMERA_RTS,
	CAMERA_RPG,
	CAMERA_COCKPIT
};

///
/// xcamera : camera
///
/// This is a camera element.
///

class xcamera : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param ID this node's id
		/// \param x1 vector 1 x?
		/// \param y1 vector 1 y?
		/// \param z1 vector 1 z?
		/// \param x2 vector 2 x?
		/// \param y2 vector 2 y?
		/// \param z2 vector 2 z?
		/// - replace with pos and rot
		///
		xcamera( irr::IrrlichtDevice *dev, int id, XCAMERA_TYPE camtype,
			int x1, int y1, int z1, int x2, int y2, int z2 );
		// xcamera_type camtype
		// eular pos (x, y, z);
		// eular rot (x, y, z);

		///
		/// destructor.
		///
		~xcamera();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		x3dpoint getPosition();
		void setPosition ( x3dpoint p );
		void setTarget ( x3dpoint p );
		void setFOV( float fov );
		void setAspectRatio( float a );
		void setFarValue( float v);
        void setOrthoMode( float a, float b, float c, float d);

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;            // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)

		XCAMERA_TYPE type;
		// look at my camera collection.
		scene::ICameraSceneNode* camera_static;
		scene::ICameraSceneNode* camera_fps;
		// scene::xrtscam*          camera_rts;
		// followingCamera*	 followcam;



};

#endif // XCAMERA_H
