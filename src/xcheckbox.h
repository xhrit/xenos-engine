// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xcheckbox.h

#ifndef XCHECKBOX_H
#define XCHECKBOX_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xcheckbox : static text
///
/// This is a static text element.
///


class xcheckbox : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		///
		xcheckbox( irr::IrrlichtDevice *dev, int id, bool b, char * c, int x1, int x2, int y1, int y2 );

		///
		/// destructor.
		///
		~xcheckbox();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();
		void setVisible( bool v );
		void setParent( xelement * x );
		bool isChecked();
		void setChecked( bool b );

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;      // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		IGUICheckBox * checkbox;
};

#endif // XCHECKBOX_H
