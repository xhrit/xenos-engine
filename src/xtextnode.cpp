// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xtextnode.cpp

#include "xtextnode.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xtextnode::xtextnode( irr::IrrlichtDevice *dev, int id, xfont * f, char* txt, xcolour c ) : xelement( dev, id ){
	// cout << "[xtextnode] create xtextnode" << endl;

	swdata  = txt;
	wsdata  = swdata.c_str();

	node = irr_smgr->addTextSceneNode( f->getFont(), wsdata, c.getSColour(), 0, vector3df( 0, 0, 0 ), id );

	setType( XELEMENT_TYPE_TEXTNODE );
	node_reference = node;
}

xtextnode::~xtextnode(){
	// cout << "[xtextnode] delete xtextnode" << endl;
}

int xtextnode::update(){
	// cout << "[xtextnode] xtextnode::update" << endl;
	return 1;
}

int xtextnode::render(){
	// cout << "[xtextnode] xtextnode::render" << endl;
	return 1;
} // xtextnode::render

bool xtextnode::event(){
	// cout << "[xtextnode] xtextnode::event" << endl;
	return false;
} // xtextnode::event

void xtextnode::destroy(){
	// cout << "[xtextnode] xtextnode::destroy" << endl;
	node->remove();
	setType( XELEMENT_TYPE_NULL );
} // xtextnode::destroy

x3dpoint xtextnode::getPosition(){
	vector3df p = node->getPosition();
	return x3dpoint ( p.X, p.Y, p.Z );
}

x3dpoint xtextnode::getRotation(){
	vector3df p = node->getRotation();
	return x3dpoint ( p.X, p.Y, p.Z );
}

x3dpoint xtextnode::getScale(){
	vector3df p = node->getScale();
	return x3dpoint ( p.X, p.Y, p.Z );
}

void xtextnode::setPosition( x3dpoint p){
	// cout << "[xtextnode] setPosition" << endl;
	position = p;
	node->setPosition ( p.getv3df() );
} // xtextnode::setPosition

void xtextnode::setRotation( x3dpoint p){
	// cout << "[xtextnode] setRotation" << endl;
	rotation = p;
	node->setRotation ( p.getv3df() );
} // xtextnode::setRotation

void xtextnode::setScale(x3dpoint p) {
	scale = p;
	node->setScale ( p.getv3df() );
} // xtextnode::setScale

void xtextnode::setText( char * c ){
	// cout << "[xtext" << element_id <<"] xtext::setText" << endl;
	swdata  = c;
	wsdata  = swdata.c_str();
	node->setText( wsdata );
} // xtext::setText

void xtextnode::setColour( xcolour c ){
	node->setTextColor   ( c.getSColour() );
} // xtext::setColour
