// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XELEMENT_H
#define XELEMENT_H

#include "xattr.h"

#include <irrlicht.h>       // 3d/Graphics lib
#include <vector>           // container class
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <sstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

/// \enum XELEMENT_TYPE
/// \brief xelement type.
///
/// A more detailed class description.
///
enum XELEMENT_TYPE{
	XELEMENT_TYPE_NULL,
	XELEMENT_TYPE_AREA,
	XELEMENT_TYPE_DIALOG,
	XELEMENT_TYPE_SELECT,
	XELEMENT_TYPE_OPTION,
	XELEMENT_TYPE_INPUT,
	XELEMENT_TYPE_TEXT,
	XELEMENT_TYPE_IMAGE,
	XELEMENT_TYPE_BUTTON,
	XELEMENT_TYPE_WINDOW,
	XELEMENT_TYPE_SCROLLBAR,
	XELEMENT_TYPE_LISTBOX,
	XELEMENT_TYPE_TEXTBOX,
	XELEMENT_TYPE_EDITBOX,
	XELEMENT_TYPE_CONTEXTMENU,
	XELEMENT_TYPE_ENVIRONMENT,
	XELEMENT_TYPE_CAMERA,
	XELEMENT_TYPE_LIGHT,
	XELEMENT_TYPE_FIELD,
	XELEMENT_TYPE_MESH,
	XELEMENT_TYPE_ANIMATEDMESH,
	XELEMENT_TYPE_TERRAIN,
	XELEMENT_TYPE_PARTICLESYSTEM,
	XELEMENT_TYPE_SKYBOX,
	XELEMENT_TYPE_SCRIPT,
	XELEMENT_TYPE_BODY,
	XELEMENT_TYPE_TABLE,
	XELEMENT_TYPE_COMBOBOX,
	XELEMENT_TYPE_GRID,
	XELEMENT_TYPE_TEXTNODE,
	XELEMENT_TYPE_BEAM,
	XELEMENT_TYPE_BILLBOARD,
	XELEMENT_TYPE_BONE,
	XELEMENT_TYPE_CHECKBOX,
	XELEMENT_TYPE_TAB,
	XELEMENT_TYPE_LINE,
	XELEMENT_TYPE_CIRCLE,
	XELEMENT_TYPE_CHATBOX
};

///
/// x3dpoint
///
/// A point in 3d space
///

class x3dpoint{
	public:
		x3dpoint(){};
		x3dpoint(float x, float y, float z){X = x; Y = y; Z = z;};
		~x3dpoint(){};
		float X;
		float Y;
		float Z;
		vector3df getv3df(){ return vector3df(X, Y, Z); }
		// dReal * getDReal()()
		void trace(){
			cout << "[x3dpoint]" << X << "," << Y << "," << Z  << endl;
		}

		void normalize() {
            vector3df v = vector3df(X, Y, Z);
            v.normalize();
			X = v.X;
			Y = v.Y;
			Z = v.Z;
		}

};

///
/// x2dpoint
///
/// A point in 2d space
///

class x2dpoint{
	public:
		x2dpoint(){};
		x2dpoint (float x, float y){X = x; Y = y; };
		~x2dpoint (){};
		float X;
		float Y;
};

///
/// x2drect
///
/// A rectangle in 2d space
///

class x2drect{
	public:
		x2drect(){};
		x2drect (float x1, float y1, float x2, float y2){
			X1 = x1; Y1 = y1; X2 = x2; Y2 = y2;
		};
		x2drect (x2dpoint p1, x2dpoint p2){
			X1 = p1.X; Y1 = p1.Y; X2 = p2.X; Y2 = p2.Y;
		};
		~x2drect (){};
		float X1;
		float Y1;
		float X2;
		float Y2;
};


///
/// xmatrix4
///
/// A 4 dimensonal rotation matrix
///

class xmatrix4{
	public:
		xmatrix4(){};
		~xmatrix4 (){};
		void setRotationDegrees( x3dpoint *p ){
			m.setRotationDegrees( p->getv3df() );
		};
		void transformVect( x3dpoint *p ){
			vector3df v = p->getv3df();
			m.transformVect( v );
			p->X = v.X;
			p->Y = v.Y;
			p->Z = v.Z;
		};
		void buildTextureTransform( float r, x2dpoint * c, x2dpoint * t, x2dpoint * s){ // rotate, center, translate, scale
            m.buildTextureTransform(r, vector2df(c->X, c->Y), vector2df(t->X, t->Y), vector2df(s->X, s->Y) );
		}
		matrix4 m;
};

// x2drect(x2dpoint1, x2dpoint2)
// x2drect(x1, y1, x2, y2)
// x3drect(x1, y1, z1, x2, y2, z2)
// x3drect(x3dpoint1, x3dpoint2)

enum XGUI_DEFAULT_COLOR{
	XGDC_3D_DARK_SHADOW,
	XGDC_3D_SHADOW,
	XGDC_3D_FACE,
	XGDC_3D_LIGHT,
	XGDC_ACTIVE_BORDER,
	XGDC_ACTIVE_CAPTION,
	XGDC_APP_WORKSPACE,
	XGDC_BUTTON_TEXT,
	XGDC_GRAY_TEXT,
	XGDC_HIGH_LIGHT,
	XGDC_HIGH_LIGHT_TEXT,
	XGDC_INACTIVE_BORDER,
	XGDC_INACTIVE_CAPTION,
	XGDC_TOOLTIP,
	XGDC_TOOLTIP_BACKGROUND,
	XGDC_SCROLLBAR,
	XGDC_WINDOW,
	XGDC_WINDOW_SYMBOL,
	XGDC_ICON,
	XGDC_ICON_HIGH_LIGHT,
	XGDC_COUNT
};

enum XMATERIAL_FLAG{
	XMF_WIREFRAME = 0x1,
	XMF_POINTCLOUD = 0x2,
	XMF_GOURAUD_SHADING = 0x4,
	XMF_LIGHTING = 0x8,
	XMF_ZBUFFER = 0x10,
	XMF_ZWRITE_ENABLE = 0x20,
	XMF_BACK_FACE_CULLING = 0x40,
	XMF_FRONT_FACE_CULLING = 0x80,
	XMF_BILINEAR_FILTER = 0x100,
	XMF_TRILINEAR_FILTER = 0x200,
	XMF_ANISOTROPIC_FILTER = 0x400,
	XMF_FOG_ENABLE = 0x800,
	XMF_NORMALIZE_NORMALS = 0x1000,
	XMF_TEXTURE_WRAP = 0x2000,
	XMF_ANTI_ALIASING = 0x4000,
	XMF_COLOR_MASK = 0x8000
};

enum XMATERIAL_TYPE{
	XMT_SOLID,
	XMT_SOLID_2_LAYER,
	XMT_LIGHTMAP,
	XMT_LIGHTMAP_ADD,
	XMT_LIGHTMAP_M2,
	XMT_LIGHTMAP_M4,
	XMT_LIGHTMAP_LIGHTING,
	XMT_LIGHTMAP_LIGHTING_M2,
	XMT_LIGHTMAP_LIGHTING_M4,
	XMT_DETAIL_MAP,
	XMT_SPHERE_MAP,
	XMT_REFLECTION_2_LAYER,
	XMT_TRANSPARENT_ADD_COLOR,
	XMT_TRANSPARENT_ALPHA_CHANNEL,
	XMT_TRANSPARENT_ALPHA_CHANNEL_REF,
	XMT_TRANSPARENT_VERTEX_ALPHA,
	XMT_TRANSPARENT_REFLECTION_2_LAYER,
	XMT_NORMAL_MAP_SOLID,
	XMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR,
	XMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA,
	XMT_PARALLAX_MAP_SOLID,
	XMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR,
	XMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA,
	XMT_ONETEXTURE_BLEND,
	XMT_FORCE_32BIT
};

enum X_HARDWARE_MAPPING{
	XHM_NEVER=0,
	XHM_STATIC,
	XHM_DYNAMIC,
	XHM_STREAM
};

enum X_BUFFER_TYPE{
	XBT_NONE=0,
	XBT_VERTEX,
	XBT_INDEX,
	XBT_VERTEX_AND_INDEX
};

///
/// xcolour
///
/// An ARGB colour
///

class xcolour {
	public:
		xcolour(){};
		xcolour (float a, float r, float g, float b){
			A = a; R = r; G = g; B = b;
		};
		~xcolour (){};
		float A;
		float R;
		float G;
		float B;
 		SColor getSColour(){ return SColor(A, R, G, B); }
};

class xfont {
	public:
		xfont( irr::IrrlichtDevice * dev, char * filename ){
			// cout << "[xcore] xfont file" << endl;
			font = dev->getGUIEnvironment()->getFont( filename );
		};
		xfont( irr::IrrlichtDevice * dev ){
			// cout << "[xcore] xfont BuiltInFont" << endl;
			font = dev->getGUIEnvironment()->getBuiltInFont();
		};
		~xfont(){};
		void draw( char * t, x2drect r, xcolour c ){
			// cout << "[xcore] xfont draw" << endl;
			ostringstream stream;      // conversion variables (?)
			core::stringw swdata;      // conversion variables (?)
			const wchar_t * wsdata;     // conversion variables (?)

			stream.str("");
			stream.clear();
			swdata  = t;
			wsdata  = swdata.c_str();

		        font->draw( wsdata,
				core::rect<s32>( r.X1, r.Y1, r.X2, r.Y2 ),
				video::SColor( c.A, c.R, c.G, c.B ));
		};
		irr::gui::IGUIFont * getFont(){ return font; }
	// private
		irr::gui::IGUIFont * font;
};

class xtexture {
	public:
		xtexture( irr::IrrlichtDevice * dev, char * filename ){
			texture = dev->getVideoDriver()->getTexture( filename );
		};
		~xtexture (){};
		irr::video::ITexture * texture;
};

///
/// xelement
///
/// This is the root node.
///

class xelement {
	protected:
		irr::IrrlichtDevice       *irr_device;    ///< irrlicht device pointer
		irr::video::IVideoDriver  *irr_driver;    ///< irrlicht video driver pointer
		irr::scene::ISceneManager *irr_smgr;      ///< irrlicht scene manager pointer
		irr::gui::IGUIEnvironment *irr_env;       ///< irrlicht gui environment pointer

	public:
		///
		/// constructor.
		///
		xelement( irr::IrrlichtDevice *dev, int id );

		///
		/// destructor.
		///
		~xelement();

		///
		/// update seq
		/// @return  error code
		///
		virtual int update();

		///
		/// render seq
		/// @return  error code
		///
		virtual int render();

		///
		/// event seq
		/// @return success
		///
		virtual bool event();
        virtual void destroy();


		virtual void setPosition( x3dpoint p){ position = p; };
		virtual x3dpoint getPosition(){ return position; };

		virtual void setRotation( x3dpoint p){ rotation = p; };
		virtual x3dpoint getRotation(){ return rotation; };

		virtual void setLocation( x2dpoint p){ location = p; };
		virtual x2dpoint getLocation(){ return location; };

		///
		/// sets element type
		/// @param enum
		void setType( XELEMENT_TYPE t ){ xelement_type = t; };
		XELEMENT_TYPE getType(){ return xelement_type; };

		///
		/// sets element attribute
		/// @param enum
		void setAttr( char * name, char * value ); // , XATTR_TYPE type
		char * getAttr( char * name );

		virtual void setVisible( bool v ){ visible = v; };
		virtual bool getVisible(){ return visible; };

		///
		/// sets parent
		///
		virtual void setParent( xelement * x );
		virtual xelement * getParent();

		virtual ISceneNode * getGfxNode(){ return node_reference; };
		virtual IGUIElement * getGfxElement(){
			// cout << "[xelement] getGfxElement" << endl;
			return element_reference;
		};

		void printType();
        int getID(){
			// cout << "[xelement] getID" << element_id << endl;
            return element_id;
        };

		// why are'nt these private?!
		x3dpoint position;
		x3dpoint rotation;
		x2dpoint location;
		int element_id;
		bool visible;
		vector < xattr > attributes;
		IGUIElement * element_reference;
		ISceneNode * node_reference;
		xelement * parent;

	private:
		XELEMENT_TYPE xelement_type;

};

#endif // XELEMENT_H
