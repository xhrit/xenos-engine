// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xskybox.h

#ifndef XSKYBOX_H
#define XSKYBOX_H

#include "xelement.h"       // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xskybox
///
/// This is a skybox.
///

class xskybox : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		///
		xskybox( irr::IrrlichtDevice *dev, int id,
			char * filename1,
			char * filename2,
			char * filename3,
			char * filename4,
			char * filename5,
			char * filename6);

		///
		/// destructor.
		///
		~xskybox();
		void destroy();

		void setRotation( x3dpoint p){
			// cout << "rotate sky" << endl;
			rotation = p;
			skybox_node->setRotation ( vector3df(p.X, p.Y, p.Z) );
		};

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;            // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		scene::ISceneNode* skybox_node;

};

#endif // XSKYBOX_H
