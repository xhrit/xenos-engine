
#include "xrtscam.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"

#include <iostream>
#include <cstdlib>
#include <sstream>
#include <stdio.h>

namespace irr
{
namespace scene
{

using namespace std;

//! constructor
xrtscam::xrtscam(ISceneNode* parent, ISceneManager* mgr, s32 id, 
	const core::vector3df& position, const core::vector3df& lookat)
	: ICameraSceneNode(parent, mgr, id, position, core::vector3df(-40.0f, 0.0f, -40.0f),
			core::vector3df(0.0f, 0.0f, 0.0f)), InputReceiverEnabled(true)
{
	cout << "[xrtscam] new xrtscam" << endl;
	Parent = parent;
	wheelSpeed = 5;
	radius = 20;
	alpha = 20;
	theta = 180;
	Ttheta = theta;
	LeftMouseClick.mouse_key = RELEASED;
	RightMouseClick.mouse_key =  RELEASED;

	ekey = RELEASED;
	wkey =  RELEASED;
	dkey =  RELEASED;
	fkey =  RELEASED;
	skey =  RELEASED;
	rkey =  RELEASED;


	xMovementMult = 0.1;
	yMovementMult = 0.1;

	BBox.reset(0,0,0);

// set default view

	
	UpVector.set(0.0f, 1.0f, 0.0f);
	calculateCameraPosition();

	// set default projection

	fovy = core::PI / 2.5f;	// Field of view, in radians. 
	aspect = 4.0f / 3.0f;	// Aspect ratio. 
	zn = 1.0f;				// value of the near view-plane. 
	zf = 3000.0f;			// Z-value of the far view-plane. 

	video::IVideoDriver* d = mgr->getVideoDriver();
	if (d)
	{
		screenDim.Width = (f32)d->getScreenSize().Width;
		screenDim.Height = (f32)d->getScreenSize().Height;
		aspect = screenDim.Width / screenDim.Height;
	}

	recalculateProjectionMatrix();
}



//! destructor
xrtscam::~xrtscam()
{
}

void xrtscam::setParent(ISceneNode* parent){
	Parent = parent;
}

//! Disables or enables the camera to get key or mouse inputs.
void xrtscam::setInputReceiverEnabled(bool enabled)
{
	InputReceiverEnabled = enabled;
}


//! Returns if the input receiver of the camera is currently enabled.
bool xrtscam::isInputReceiverEnabled() const
{
	_IRR_IMPLEMENT_MANAGED_MARSHALLING_BUGFIX;
	return InputReceiverEnabled;
}


//! Sets the projection matrix of the camera. The core::matrix4 class has some methods
//! to build a projection matrix. e.g: core::matrix4::buildProjectionMatrixPerspectiveFovLH
//! \param projection: The new projection matrix of the camera. 
void xrtscam::setProjectionMatrix(const core::matrix4& projection)
{
	Projection = projection;
}



//! Gets the current projection matrix of the camera
//! \return Returns the current projection matrix of the camera.
const core::matrix4& xrtscam::getProjectionMatrix() const {
	return Projection;
}



//! Gets the current view matrix of the camera
//! \return Returns the current view matrix of the camera.
const core::matrix4& xrtscam::getViewMatrix() const {
	return View;
}


void xrtscam::zoomIn(){
	cout << "[xrtscam] zoomIn" << endl;
	radius -= wheelSpeed;
	if (radius < 5) 
		radius = 5;
}

void xrtscam::zoomOut(){
	cout << "[xrtscam] zoomOut" << endl;
	radius += wheelSpeed;
	if (radius > 400)
		radius = 400;
}

void xrtscam::moveCam(int x, int y){
	cout << "[xrtscam] movecam" << endl;
	float xMovement = x;

	theta -= xMovement*yMovementMult;

	if(theta < 0) 
		theta =  360 + theta;
	if (theta>360)
		theta = theta - 360;

	float yMovement = y;
	alpha += yMovement*yMovementMult;

	if (alpha < -45)
		alpha = -45;
	if (alpha > 90)
		alpha = 90;
}


//! It is possible to send mouse and key events to the camera. Most cameras
//! may ignore this input, but camera scene nodes which are created for 
//! example with scene::ISceneManager::addMayaCameraSceneNode or
//! scene::ISceneManager::addMeshViewerCameraSceneNode, or scene::ISceneManager::xrtscam may want to get this input
//! for changing their position, look at target or whatever. 
bool xrtscam::OnEvent(const SEvent& event) {
	cout << "[xrtscam] OnEvent" << endl;
	core::vector3df vect;
	float PI = 3.1415;

	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		switch(event.MouseInput.Event) 
		{
	        case EMIE_MOUSE_WHEEL:
	        {
				if (event.MouseInput.Wheel > 0) {
					zoomIn();
				} else {
					zoomOut();
				}
				calculateCameraPosition(); 
	        }
	        break;
			case EMIE_MOUSE_MOVED:
	        {
				if (RightMouseClick.mouse_key == PRESSED_DOWN)
				{
	
					float xMovement = event.MouseInput.X - RightMouseClick.x;
	
					theta -= xMovement*yMovementMult;
		
					if(theta < 0) 
						theta =  360 + theta;
					if (theta>360)
						theta = theta - 360;
	
					float yMovement = event.MouseInput.Y - RightMouseClick.y;
					alpha += yMovement*yMovementMult;
		                
					if (alpha < -45)
						alpha = -45;
					if (alpha > 90)
						alpha = 90;
		                                                      
					RightMouseClick.x = event.MouseInput.X;
					RightMouseClick.y = event.MouseInput.Y;                          

					// calculateCameraPosition();
	
				}	
	
			}
			break;
			/*
			case EMIE_RMOUSE_PRESSED_DOWN:
			{
				if (RightMouseClick.mouse_key == RELEASING)			
				{
					RightMouseClick.x = event.MouseInput.X;
					RightMouseClick.y = event.MouseInput.Y;                          
					RightMouseClick.mouse_key = PRESSING_DOWN;
				}
	
				if (RightMouseClick.mouse_key == RELEASED)
				{	
					RightMouseClick.x = event.MouseInput.X;
					RightMouseClick.y = event.MouseInput.Y;                          
					RightMouseClick.mouse_key = PRESSING_DOWN;
				}
	
				if (RightMouseClick.mouse_key == PRESSING_DOWN)
					RightMouseClick.mouse_key = PRESSED_DOWN;
		
				if (RightMouseClick.mouse_key == PRESSED_DOWN){}
	
			}
			break;

	        case EMIE_RMOUSE_LEFT_UP:
			{
				if (RightMouseClick.mouse_key == RELEASING)			
					RightMouseClick.mouse_key = RELEASED;
	
				if (RightMouseClick.mouse_key == RELEASED){}			
	
				if (RightMouseClick.mouse_key == PRESSING_DOWN)
					RightMouseClick.mouse_key = RELEASING;
		
				if (RightMouseClick.mouse_key == PRESSED_DOWN)
					RightMouseClick.mouse_key = RELEASING;
				
			}
			break;
			*/
	        case EMIE_RMOUSE_PRESSED_DOWN:
				if (RightMouseClick.mouse_key == RELEASING)
				{
					RightMouseClick.x = event.MouseInput.X;
					RightMouseClick.y = event.MouseInput.Y;
					RightMouseClick.mouse_key = PRESSING_DOWN;
				}
				if (RightMouseClick.mouse_key == RELEASED)
				
				{
					RightMouseClick.x = event.MouseInput.X;
					RightMouseClick.y = event.MouseInput.Y;
					RightMouseClick.mouse_key = PRESSING_DOWN;
				}
				if (RightMouseClick.mouse_key == PRESSING_DOWN)
					RightMouseClick.mouse_key = PRESSED_DOWN;
	
				if (RightMouseClick.mouse_key == PRESSED_DOWN){}
	
			break;
	        case EMIE_RMOUSE_LEFT_UP:
				if (RightMouseClick.mouse_key == RELEASING)
					RightMouseClick.mouse_key = RELEASED;
	
				if (RightMouseClick.mouse_key == RELEASED){}
	
				if (RightMouseClick.mouse_key == PRESSING_DOWN)
					RightMouseClick.mouse_key = RELEASING;
	
				if (RightMouseClick.mouse_key == PRESSED_DOWN)
					RightMouseClick.mouse_key = RELEASING;
	
			break;
	
			default: break;
		}
	}

	return false;
}



//! sets the look at target of the camera
//! \param pos: Look at target of the camera.
void xrtscam::setTarget(const core::vector3df& pos)
{
	Target = pos;
}



//! Gets the current look at target of the camera
//! \return Returns the current look at target of the camera
core::vector3df xrtscam::getTarget() const
{
	return Target;
}



//! sets the up vector of the camera
//! \param pos: New upvector of the camera.
void xrtscam::setUpVector(const core::vector3df& pos)
{
	UpVector = pos;
}



//! Gets the up vector of the camera.
//! \return Returns the up vector of the camera.
core::vector3df xrtscam::getUpVector() const
{
	return UpVector;
}


f32 xrtscam::getNearValue() const
{
	return zn;
}

f32 xrtscam::getFarValue() const
{
	return zf;
}

f32 xrtscam::getAspectRatio() const
{
	return aspect;
}

f32 xrtscam::getFOV() const
{
	return fovy;
}

void xrtscam::setNearValue(f32 f)
{
	zn = f;
	recalculateProjectionMatrix();
}

void xrtscam::setFarValue(f32 f)
{
	zf = f;
	recalculateProjectionMatrix();
}

void xrtscam::setAspectRatio(f32 f)
{
	aspect = f;
	recalculateProjectionMatrix();
}

void xrtscam::setFOV(f32 f)
{
	fovy = f;
	recalculateProjectionMatrix();
}

void xrtscam::recalculateProjectionMatrix()
{
	Projection.buildProjectionMatrixPerspectiveFovLH(fovy, aspect, zn, zf);
	//recalculateViewArea();
}


//! prerender
void xrtscam::OnRegisterSceneNode()
{
	float xTrans, yTrans;
	float PI = 3.1415;
	calculateCameraPosition();
	
	video::IVideoDriver* driver = SceneManager->getVideoDriver();
	if (!driver)
		return;

	if (SceneManager->getActiveCamera() == this)
	{
		screenDim.Width = (f32)driver->getScreenSize().Width;
		screenDim.Height = (f32)driver->getScreenSize().Height;

		driver->setTransform(video::ETS_PROJECTION, Projection);

		// if upvector and vector to the target are the same, we have a
		// problem. so solve this problem:

		core::vector3df pos = getAbsolutePosition();
		core::vector3df tgtv = Target - pos;
		tgtv.normalize();

		core::vector3df up = UpVector;
		up.normalize();

		f32 dp = tgtv.dotProduct(up);
		if ((dp > -1.0001f && dp < -0.9999f) ||
			(dp < 1.0001f && dp > 0.9999f))
			up.X += 1.0f;

		View.buildCameraLookAtMatrixLH(pos, Target, up);
		recalculateViewArea();

		SceneManager->registerNodeForRendering(this, ESNRP_CAMERA); // ESNRP_LIGHT_AND_CAMERA
	}

	ISceneNode::OnRegisterSceneNode();

}


//! render
void xrtscam::render()
{	
	video::IVideoDriver* driver = SceneManager->getVideoDriver();
	if (!driver)
		return;

	driver->setTransform(video::ETS_VIEW, View);
}


//! returns the axis aligned bounding box of this node
const core::aabbox3d<f32>& xrtscam::getBoundingBox() const
{
	return BBox;
}



//! returns the view frustrum. needed sometimes by bsp or lod render nodes.
const SViewFrustum* xrtscam::getViewFrustum() const
{
	return &ViewArea;
}

void xrtscam::recalculateViewArea()
{
	core::matrix4 mat = Projection * View;
	ViewArea = SViewFrustum(mat);

	ViewArea.cameraPosition = getAbsolutePosition();
	ViewArea.recalculateBoundingBox();
}

void xrtscam::calculateCameraPosition()
{
	float x,y,z;
	float PI = 3.1415;


	x = radius*cos(alpha*PI/180)*cos(theta*PI/180);
	y = radius*sin(alpha*PI/180);
	z = radius*cos(alpha*PI/180)*sin(theta*PI/180);
	
	core::vector3df pos(x, y, z);
	core::vector3df character_pos = Parent->getPosition();
	// character_pos.Y = character_pos.Y + 30;

//    printf("cx %f\n", character_pos.X);
//    printf("cy %f\n",character_pos.Y);
//    printf("cz %f\n", character_pos.Z);
//
//    printf("x %f\n",x);
//    printf("y %f\n",y);
//    printf("z %f\n",z);
//
    pos = pos+character_pos;
	setPosition(pos);
	setTarget(character_pos);
	updateAbsolutePosition();
//	updateAbsolutePosition();
}

int xrtscam::keyUpdate( SEvent event, key_state key )
{
	if (event.EventType == EET_KEY_INPUT_EVENT)
	{
			if(event.KeyInput.PressedDown)
			{
				
				if (key == RELEASING)
					key = PRESSING_DOWN;
	
				if (key == RELEASED)
					key = PRESSING_DOWN;
	
				if (key == PRESSING_DOWN)
					key = PRESSED_DOWN;
	
				if (key == PRESSED_DOWN){}
	
			}
			if(!event.KeyInput.PressedDown)
			{
					
				if (key == RELEASING)
					key = RELEASED;
	
				if (key == RELEASED){}
	
				if (key == PRESSING_DOWN)
					key = RELEASING;
	
				if (key == PRESSED_DOWN)
					key = RELEASING;

			}

	}

	return (int)key;

}

} // end namespace
} // end namespace





