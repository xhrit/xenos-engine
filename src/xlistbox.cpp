// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xlistbox.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xlistbox::xlistbox( irr::IrrlichtDevice *dev, int id, bool b, x2drect r ) : xelement( dev, id ){
	// cout << "[xlistbox" << id <<"] create xlistbox" << endl;

	listbox = irr_env->addListBox( rect<s32>(r.X1, r.Y1, r.X2, r.Y2), 0, id, b);
	element_reference = listbox;

	w = r.X2 - r.X1;
	h = r.Y2 - r.Y1;
    // cout << "[xlistbox] w:" << w << endl;
    // cout << "[xlistbox] h:" << h << endl;

	setType( XELEMENT_TYPE_LISTBOX );
}

xlistbox::~xlistbox(){
	// cout << "[xlistbox" << element_id <<"] delete xlistbox" << endl;
}

int xlistbox::update(){
	// cout << "[xlistbox" << element_id <<"] xlistbox::update" << endl;
	return 1;
} // xlistbox::setVisible

int xlistbox::render(){
	// cout << "[xlistbox" << element_id <<"] xlistbox::render" << endl;
	return 1;
} // xlistbox::setVisible

bool xlistbox::event(){
	// cout << "[xlistbox" << element_id <<"] xlistbox::event" << endl;
	return false;
} // xlistbox::setVisible

void xlistbox::destroy(){
	// cout << "[xlistbox" << element_id <<"] xlistbox::destroy" << endl;
	listbox->remove();
} // xlistbox::setVisible

void xlistbox::clear(){
	// cout << "[xlistbox" << element_id <<"] xlistbox::clear" << endl;
	listbox->clear();
} // xlistbox::setVisible

void xlistbox::setVisible( bool v ){
	// cout << "[xlistbox" << element_id <<"] setVisible" << endl;
	listbox->setVisible( v );
} // xlistbox::setVisible

void xlistbox::addItem( char * c ){
	stream.str("");
	stream.clear();
	swdata  = c;
	wsdata  = swdata.c_str();
	listbox->addItem( wsdata );
} // xlistbox::setVisible

char * xlistbox::getItem( int i ){
	core::stringc s = listbox->getListItem( i );
	return strdup( s.c_str() );
} // xlistbox::getItem

int xlistbox::getSelected(){
	return listbox->getSelected();
} // xlistbox::getSelected

void xlistbox::setSelected( int n ){
	listbox->setSelected( n );
} // xlistbox::getSelected

void xlistbox::setParent( xelement * x ){
	parent = x;
	listbox->setParent( x->getGfxElement() );
} // xlistbox::setParent

int xlistbox::getItemCount(){
    return listbox->getItemCount();
} // xlistbox::getItemCount

void xlistbox::setItemColor( int i, int n, xcolour c ){
    listbox->setItemOverrideColor ( i, (EGUI_LISTBOX_COLOR)n, c.getSColour() );
} // xlistbox::setItemColor

int xlistbox::setAutoScroll( bool b ){
    listbox->setAutoScrollEnabled( b );
} // xlistbox::setAutoScroll

int xlistbox::removeItem( int n ){
    listbox->removeItem( n );
} // xlistbox::setAutoScroll

void xlistbox::setPosition( x2dpoint p ){
	// cout << "[xtext] setPosition" << endl;
	// position = p;
	listbox->setRelativePosition ( irr::core::rect<int>( p.X, p.Y, p.X+w, p.Y+h ) );
} // xlistbox::setPosition
