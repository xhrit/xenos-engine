// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xtextbox.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xtextbox::xtextbox( irr::IrrlichtDevice *dev, int id, x2drect r ) : xelement( dev, id ){
	cout << "[xtextbox] create xtextbox" << endl;

	// irr::gui::IGUIFont *font;
	textbox = new irr::gui::CGUITextBox( irr_env->getBuiltInFont() ,L"Text", irr_env,
		irr::core::rect<irr::s32>(r.X1, r.Y1, r.X2, r.Y2), irr_env->getRootGUIElement(), id );
	textbox->setScrollModeLines(false); // ??

	// textbox->setVisible(false);
	// textbox->hideScrollbar();

	/*
EGDC_3D_DARK_SHADOW    Dark shadow for three-dimensional display elements.
  EGDC_3D_SHADOW    Shadow color for three-dimensional display elements (for edges facing away from the light source).
  EGDC_3D_FACE    Face color for three-dimensional display elements and for dialog box backgrounds.
  EGDC_3D_HIGH_LIGHT    Highlight color for three-dimensional display elements (for edges facing the light source.).
  EGDC_3D_LIGHT    Light color for three-dimensional display elements (for edges facing the light source.).
  EGDC_ACTIVE_BORDER    Active window border.
  EGDC_ACTIVE_CAPTION    Active window title bar text.
  EGDC_APP_WORKSPACE    Background color of multiple document interface (MDI) applications.
  EGDC_BUTTON_TEXT    Text on a button.
  EGDC_GRAY_TEXT    Grayed (disabled) text.
  EGDC_HIGH_LIGHT    Item(s) selected in a control.
  EGDC_HIGH_LIGHT_TEXT    Text of item(s) selected in a control.
  EGDC_INACTIVE_BORDER    Inactive window border.
  EGDC_INACTIVE_CAPTION    Inactive window caption.
  EGDC_TOOLTIP    Tool tip text color.
  EGDC_TOOLTIP_BACKGROUND    Tool tip background color.
  EGDC_SCROLLBAR    Scrollbar gray area.
  EGDC_WINDOW    Window background.
  EGDC_WINDOW_SYMBOL    Window symbols like on close buttons, scroll bars and check boxes.
  EGDC_ICON    Icons in a list or tree.
  EGDC_ICON_HIGH_LIGHT    Selected icons in a list or tree.
*/

	IGUISkin * s = irr_env->getSkin();
	s->setColor ( EGDC_3D_FACE , video::SColor(255,255,255,255) );
	irr_env->setSkin( s );

	element_reference = textbox;

	setType( XELEMENT_TYPE_TEXTBOX );
}

xtextbox::~xtextbox(){
	cout << "[xtextbox] delete xtextbox" << endl;
}

int xtextbox::update(){
	// cout << "[xtextbox] xtextbox::update" << endl;
	return 1;
}

int xtextbox::render(){
	// cout << "[xtextbox] xtextbox::render" << endl;
	return 1;
}

bool xtextbox::event(){
	// cout << "[xtextbox] xtextbox::event" << endl;
	return false;
}

void xtextbox::destroy(){
	// cout << "[xtextbox] xtextbox::destroy" << endl;
	textbox->remove();
}

char * xtextbox::getText(){
	// cout << "[xtextbox] xtextbox::getText" << endl;
	core::stringc s = textbox->getText();
	// cout << "[xtextbox] text :" << s.c_str() << endl;
	return strdup( s.c_str() );
} // xtextbox::getText

void xtextbox::setText( char * c ){
	// cout << "[xtextbox] xtextbox::setText" << endl;
	swdata  = c;
	wsdata  = swdata.c_str();
	textbox->setText( wsdata );
} // xtextbox::setText

void xtextbox::setParent( xelement * x ){
	parent = x;
	textbox->setParent( x->getGfxElement() );
} // xtextbox::setParent
