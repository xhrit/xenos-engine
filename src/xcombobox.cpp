// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xcombobox.cpp

#include "xcombobox.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xcombobox::xcombobox( irr::IrrlichtDevice *dev, int id, x2drect r ) : xelement( dev, id ){
	// cout << "[xcombobox" << id <<"] create xcombobox" << endl;

	combobox  = irr_env->addComboBox( rect<int>( r.X1, r.Y1, r.X2, r.Y2 ), 0, id );
	element_reference = combobox;

	setType( XELEMENT_TYPE_COMBOBOX );
}

xcombobox::~xcombobox(){
	cout << "[xcombobox" << element_id <<"] delete xcombobox" << endl;
	// TODO: finish this.
}

int xcombobox::update(){
	// cout << "[xcombobox" << element_id <<"] xcombobox::update" << endl;
	return 1;
}

int xcombobox::render(){
	// cout << "[xcombobox" << element_id <<"] xcombobox::render" << endl;
	return 1;
}

bool xcombobox::event(){
	// cout << "[xcombobox" << element_id <<"] xcombobox::event" << endl;
	return false;
}

void xcombobox::destroy(){
	// cout << "[xcombobox" << element_id <<"] xcombobox::destroy" << endl;
	combobox->remove();
}

void xcombobox::clear(){
	// cout << "[xcombobox" << element_id <<"] xcombobox::clear" << endl;
	combobox->clear();
} // xcombobox::setVisible

void xcombobox::setVisible( bool v ){
	// cout << "[xcombobox" << element_id <<"] setVisible" << endl;
	combobox->setVisible( v );
} // xcombobox::setVisible

void xcombobox::setParent( xelement * x ){
	// cout << "[xcombobox" << element_id <<"] setParent" << endl;
	parent = x;
	combobox->setParent( x->getGfxElement() );
} // xcombobox::setParent

void xcombobox::addItem( char * c ){
	stream.str("");
	stream.clear();
	swdata  = c;
	wsdata  = swdata.c_str();
	combobox->addItem( wsdata );
} // xcombobox::setVisible

char * xcombobox::getItem( int i ){
	core::stringc s = combobox->getItem( i );
	return strdup( s.c_str() );
} // xcombobox::getItem

int xcombobox::getSelected(){
	return combobox->getSelected();
} // xcombobox::getSelected

void xcombobox::setSelected( int n ){
	combobox->setSelected( n );
} // xcombobox::getSelected
