// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// text.cpp

#include "xspheremesh.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xspheremesh::xspheremesh( irr::IrrlichtDevice *dev, int id ) : xelement( dev, id ){
	// cout << "[xspheremesh] create xspheremesh" << endl;
	node = irr_smgr->addSphereSceneNode();
	node_reference = node;
	setType( XELEMENT_TYPE_MESH );
}

xspheremesh::~xspheremesh(){
	// cout << "[xspheremesh] delete xspheremesh" << endl;
}

int xspheremesh::update(){
	// cout << "[xspheremesh] xspheremesh::update" << endl;
	return 1;
}

int xspheremesh::render(){
	// cout << "[xspheremesh] xspheremesh::render" << endl;
	return 1;
} // xspheremesh::render

bool xspheremesh::event(){
	// cout << "[xspheremesh] xspheremesh::event" << endl;
	return false;
} // xspheremesh::event

void xspheremesh::destroy(){
	// cout << "[xspheremesh] xspheremesh::destroy" << endl;
	node->remove();
	setType( XELEMENT_TYPE_NULL );
} // xspheremesh::destroy


x3dpoint xspheremesh::getPosition(){
	vector3df p = node->getPosition();
	return x3dpoint ( p.X, p.Y, p.Z );
}

x3dpoint xspheremesh::getRotation(){
	vector3df p = node->getRotation();
	return x3dpoint ( p.X, p.Y, p.Z );
}

x3dpoint xspheremesh::getScale(){
	vector3df p = node->getScale();
	return x3dpoint ( p.X, p.Y, p.Z );
}

void xspheremesh::setPosition( x3dpoint p){
	// cout << "[xspheremesh] setPosition" << endl;
	position = p;
	node->setPosition ( p.getv3df() );
} // xspheremesh::setPosition

void xspheremesh::setRotation( x3dpoint p){
	// cout << "[xspheremesh] setRotation" << endl;
	rotation = p;
	node->setRotation ( p.getv3df() );
} // xspheremesh::setRotation

void xspheremesh::setScale(x3dpoint p) {
	scale = p;
	node->setScale ( p.getv3df() );
} // xspheremesh::setScale

void xspheremesh::setMaterialFlag( XMATERIAL_FLAG mf, bool val ){
	node->setMaterialFlag ( (E_MATERIAL_FLAG)mf, val );
} // xmesh::setMaterialFlag

void xspheremesh::setMaterialType( XMATERIAL_TYPE mt){
	// cout << "[xmesh] xmesh::setMaterialType" << endl;
	// cout << "[xmesh] current material type: " << mt << endl;

	node->setMaterialType( (E_MATERIAL_TYPE)mt );

	video::IMaterialRenderer* renderer = irr_driver->getMaterialRenderer( (E_MATERIAL_TYPE)mt );
	// display some problem text when problem
	if (!renderer || renderer->getRenderCapability() != 0){
		// cout << "[xmesh] unable to render effect: " << mt << endl;;
	}

} // xspheremesh::setMaterialType

void xspheremesh::addNormalMap( char * bump, float factor ){
	// cout << "[xmesh] xmesh::addNormalMap" << endl;
	video::ITexture* normalMap = irr_driver->getTexture( bump );
	irr_driver->makeNormalMapTexture( normalMap, factor );
	node->setMaterialTexture( 1, normalMap );
	// node->setMaterialType( EMT_NORMAL_MAP_SOLID);

} // xspheremesh::addNormalMap

void xspheremesh::addTexture( char * fn, int index ){
	// cout << "[xmesh] addTexture: " << tex_fn << endl;
	node->setMaterialTexture( index, irr_driver->getTexture( fn ) );
} // xspheremesh::addTexture
