// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xsimulation.h"
#include "profiler.h"
#include "XEffects.h"

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <vector>

// #ifndef IRR_PROFILE
// #define IRR_PROFILE 1
// #endif

// enable/disable compiling of profiler macros


class xelement;

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

eventReceiver xreceiver;

xsimulation::xsimulation( XDRIVER_TYPE dtype, int width, int height,
			int depth, bool fs, bool sbuff, bool vsynch ){

	// cout << "[xsimulation] xsimulation init" << endl;

	// if to run headless, do not create device?
	irr_device = createDevice ( (irr::video::E_DRIVER_TYPE)dtype, dimension2d<u32>(width, height), depth,
                        fs, sbuff, vsynch, 0);             // create the irrlicht devide
	irr_device->setWindowCaption(L"xenos engine"); // set caption to module_name
	// irr_device->setResizeAble(false);                 // <-- does not work.
	irr_driver = irr_device->getVideoDriver();           // collect irrlicht driver pointer
	irr_smgr = irr_device->getSceneManager();            // irrlicht scene manager pointer
	irr_env = irr_device->getGUIEnvironment();           // irrlicht gui pointer
	irr_device->setEventReceiver( &xreceiver );


	// joystick code, move it someware?
	core::array< SJoystickInfo > joystickInfo;
	if( irr_device->activateJoysticks(joystickInfo) ){
		cout << "[xsimulation] " << joystickInfo.size() << " joystick(s) detected" << endl;
		for ( int i = 0; i < joystickInfo.size(); i++ ){
			cout << "[xsimulation] joystick ID : " << i << endl;
			cout << "[xsimulation] joystick Name : " << joystickInfo[i].Name.c_str() << endl;
			cout << "[xsimulation] joystick Buttons : " << joystickInfo[i].Buttons << endl;
			cout << "[xsimulation] joystick Axes : " << joystickInfo[i].Axes << endl;
		}
	} else {
		cout << "[xsimulation] joystick support not enabled" << endl;
	}
	// end joystick code.

	xreceiver.setSim( this );
	clearKeyBuffer();
	keyDelay = 0;
	keyRepeat = false;
}

xsimulation::~xsimulation(){
}

int xsimulation::update(){
	// cout << "[xsimulation] xsimulation update" << endl;

	clearGUIBuffer();
	clearEventQue();
	updateKeyBuffer(); // TODO:wtf?

    // cout << "[xsimulation] remove null elements" << endl;
    // erase invalid element references
	for (int i = references.size()-1; i > 0 ; i--){
        if ( references[i]->getType() == XELEMENT_TYPE_NULL ){
            // cout << "[xsimulation] null element, removing index " << i << endl;
            delete references[i];
            references.erase( references.begin()+ i );
        }
	}

    // cout << "[xsimulation] update irrlicht" << endl;
	if( !irr_device->run() ) { exit(0); }

    // cout << "[xsimulation] update" << endl;
	return true;
}

int xsimulation::prerender(){
	// cout << "[xsimulation] xsimulation prerender" << endl;
    irr_driver->beginScene(true, true, scene_colour.getSColour() );
}

int xsimulation::render(){
	// cout << "[xsimulation] xsimulation render" << endl;
	prerender();
    irr_smgr->drawAll();
	// call render() on every element in simulation tree.
	for (int i = 0; i < references.size(); i++){
	 	references[i]->render();
	}

	// core::stringw str = L"Xenos Engine FPS:";
    //                     str += irr_driver->getFPS();
    //                    str += " | tris:";
    //                    str += irr_driver->getPrimitiveCountDrawn();

	// irr_device->setWindowCaption( str.c_str() );
	postrender();
	// cout << "[xsimulation] /xsimulation render" << endl;
}

void xsimulation::setColour ( xcolour c ){
	scene_colour = c;
}

int xsimulation::postrender(){
	// cout << "[xsimulation] xsimulation postrender" << endl;
    irr_env->drawAll();
    irr_driver->endScene();
	return true;
}

int xsimulation::renderWithBloom(){
	irr_driver->beginScene(true, true, 0);
	irr_driver->setRenderTarget( Bloom->rt0, true, true, video::SColor(0,0,0,0) ); //First Render The entire scene into the renderTarget
	irr_smgr->drawAll();                                                           //of the PostProcess
	Bloom->render();
	irr_driver->setRenderTarget(0);                                                //set The Rendertarget back to the main view
    irr_env->drawAll();
    irr_driver->endScene();
}

int xsimulation::renderWithBlur(){
	irr_driver->beginScene(true, true, 0);  //This time the setup is a little bit harder than normal.
	Blur->render();                     //For to hold it simple you just have to call two functions:
	irr_driver->setRenderTarget(0);         //render(), which does the mainwork(blending the differen Frames together)
	Blur->renderFinal();                //and renderFinal(); which will render the result into the given renderTarget.
    irr_env->drawAll();
	irr_driver->endScene();
}

int xsimulation::initBloom( float dist, float str, float mult) {
	Bloom = new IPostProcessBloom( irr_smgr->getRootSceneNode(), irr_smgr, 666);
	PPE_Bloom_Setup setup;
	setup.sampleDist=dist;
	setup.strength=str;
	setup.multiplier=mult;
	Bloom->initiate( 2024, 2024, setup, irr_smgr );
}

int xsimulation::initBlur( float str) {
	Blur = new IPostProcessMotionBlur( irr_smgr->getRootSceneNode(), irr_smgr, 666);
	Blur->initiate( 2024, 2024, str, irr_smgr );
}

void xsimulation::initShadows() {
	// This is the screen buffer resolution, it should preferably be the exact same
	// size as the screen, although some ATI cards have trouble with that so for
	// the purpose of this demo it will only be (512, 512) on ATI cards.
	// (This is also used in some of the shadow map calculations, so it can affect
	// the shadow map quality a little also.)

	dimension2du ScreenRTT =
		// Comment JUST the next line if "getVendorInfo()" is not defined.
		!irr_driver->getVendorInfo().equals_ignore_case("NVIDIA Corporation") ? dimension2du(512, 512) :
		irr_driver->getScreenSize();

	// Initialise the EffectHandler, pass it the working Irrlicht device,
	// the shader directory address, and the screen buffer resolution.
	// Shadow map resolution setting has been moved to SShadowLight for more flexibility.
	// (The screen buffer resolution need not be the same as the screen resolution.)
	// The last parameter enables soft round spot light masks on our shadow lights.

	effect = new EffectHandler(irr_device, "shaders", ScreenRTT, true);

	// Set the background clear color
	effect->setClearColour(SColor(255, 255, 255, 255));
	effect->setAmbientColor(SColor(255, 255, 255, 255));
}

int xsimulation::renderWithShadows(){
	irr_driver->beginScene(true, true, SColor(0x0));
	// EffectHandler->update() replaces smgr->drawAll(). It handles all
	// of the shadow maps, render targets switching, post processing, etc.
	effect->update();

    irr_env->drawAll();

	irr_driver->endScene();
}

// TODO: fixme! replace xlight?
void xsimulation::addShadowLight( int id, x3dpoint pos, x3dpoint tgt, xcolour c, float nv, float fv, float radius, bool directional ){
	u32 shadowDimen = 512;
	effect->addShadowLight( SShadowLight(shadowDimen, pos.getv3df(),
		tgt.getv3df(), c.getSColour(), nv, fv, radius, directional) );
}

void xsimulation::setShadowLightPosition(int i, x3dpoint pos ){
	effect->getShadowLight(i).setPosition( pos.getv3df() );
}

void xsimulation::setShadowLightTarget(int i, x3dpoint pos ){
	effect->getShadowLight(i).setTarget( pos.getv3df() );
}

void xsimulation::addShadowToElement( xelement * e ){
	effect->addShadowToNode( e->getGfxNode(), (E_FILTER_TYPE)2 );
}

void xsimulation::removeShadowFromElement( xelement * e ){
	effect->removeShadowFromNode( e->getGfxNode() );
}

void xsimulation::addPostProcessingEffect( char * filename ){ //
	// Add some post processing effects, a very subtle bloom here.
	// const stringc shaderExt = (irr_driver->getDriverType() == EDT_DIRECT3D9) ? ".hlsl" : ".glsl";
	effect->addPostProcessingEffectFromFile( filename );
}

bool xsimulation::event(irr::SEvent sevent) {
	// cout << "[xsimulation] xsimulation event " << endl;

	// populate event buffer
	// if (sevent.KeyInput.Key){
	// 	if ( sevent.KeyInput.Key < irr::KEY_KEY_CODES_COUNT ){
	// 		cout << "populate keybuffer[ " << sevent.KeyInput.Key << "]" << endl;
	// 		keybuffer[sevent.KeyInput.Key] = sevent.KeyInput.PressedDown;
	// 	}
	// }

	// translate into xevents and add to event_que.
	xevent* new_event = new xevent();

	if (sevent.EventType == irr::EET_GUI_EVENT) {  /// <------------- heh...
		// cout << "\n[xsimulation] EET_GUI_EVENT " << endl;
		// set up manuall events
		new_event->EventType = XGUI_EVENT; // the type ov event
		new_event->setGUIEventType( sevent.GUIEvent.EventType ); // Type of GUI Event.
		if ( getCaller( sevent.GUIEvent.Caller ) != NULL ) {
			new_event->Caller = getCaller( sevent.GUIEvent.Caller ); // who called the event.

			// set up auto events
			// this will have to call the element reference to check what ID event caller is.
			// then it will have to check the guibind to find the index to add a 'true' into
			int i = new_event->Caller->element_id;
			// cout << "[xsimulation] caller element id" << i << endl;
			// cout << "[xsimulation] sevent.GUIEvent.Caller->getID()" << sevent.GUIEvent.Caller->getID() << endl;

			// cout << "[xsimulation]event type" << new_event->getGUIEventType() << "(" << sevent.GUIEvent.EventType << ")" << endl;
			// cout << "[xsimulation] XGUI_ELEMENT_FOCUS_LOST" << XGUI_ELEMENT_HOVERED << endl;

			for ( int n = 0; n < gui.size(); n++ ) {
				if (sevent.GUIEvent.Caller->getID() == gui[n]->id && new_event->getGUIEventType() == gui[n]->gui_event){

					guibuffer[n] = true;
				} else if (i == gui[n]->id && new_event->getGUIEventType() == gui[n]->gui_event){
					guibuffer[n] = true;
				}
			}
			event_que.push_back( new_event );
		}
	} else if (sevent.EventType == irr::EET_MOUSE_INPUT_EVENT) {
		// cout << "[xsimulation] MOUSE_INPUT_EVENT " << endl;
		// TODO: this doesnt work, does it?
		new_event->EventType = XMOUSE_EVENT; // the type ov event
		new_event->setMouseEventType( sevent.MouseInput.Event ); // type of mouse event
		new_event->Wheel = sevent.MouseInput.Wheel; // mouse wheel delta, usually 1.0 or -1.0.
		new_event->X = sevent.MouseInput.X; // X position of mouse cursor.
		new_event->Y = sevent.MouseInput.Y; //Y position of mouse cursor.
		event_que.push_back( new_event );

	} else if (sevent.EventType == irr::EET_KEY_INPUT_EVENT) {
		// cout << "[xsimulation] KEY_INPUT_EVENT " << endl;
		// set up manual events
		new_event->EventType = XKEY_EVENT; // the type ov event
		new_event->Shift = sevent.KeyInput.Shift; // true if shift was also pressed
		new_event->Control = sevent.KeyInput.Control; // true if ctrl was also pressed
		new_event->PressedDown = sevent.KeyInput.PressedDown; // if not pressed, then the key was left up
		new_event->setKeyEventType ( sevent.KeyInput.Key ); // Key which has been pressed or released.

		// set up auto events
		if (sevent.KeyInput.Key < XKEY_XKEY_CODES_COUNT){
		    keybuffer_old[sevent.KeyInput.Key] = keybuffer[sevent.KeyInput.Key];
			keybuffer[sevent.KeyInput.Key] = sevent.KeyInput.PressedDown;
			// if ( keybuffer_delay[ sevent.KeyInput.Key ] == 0 ) { keybuffer_delay[ sevent.KeyInput.Key ] = keyDelay; }
		}
		event_que.push_back( new_event );

	} else if (sevent.EventType == irr::EET_JOYSTICK_INPUT_EVENT) {
		// cout << "[xsimulation] JOYSTICK_EVENT " << endl;
		new_event->EventType = XJOYSTICK_EVENT; // the type ov event

		// set up manual events
		for ( int n = 0; n < sevent.JoystickEvent.NUMBER_OF_AXES; n++ ) {
			new_event->Axis[n] = (float)sevent.JoystickEvent.Axis[n];
			// cout << "axis #" << n << " : " << new_event->Axis[n] << endl;
		}
		new_event->ButtonStates = sevent.JoystickEvent.ButtonStates;
		new_event->Joystick = sevent.JoystickEvent.Joystick;
		// TODO: set up auto events

		event_que.push_back( new_event );
	} else if (sevent.EventType == irr::EET_LOG_TEXT_EVENT) {
		// cout << "[xsimulation] LOG_TEXT_EVENT " << endl;
		// new_event->EventType = XLOG_EVENT; // the type ov event
		// new_event.LogEventType; // the log level
		// new_event.Text;  // pointer to text which has been logged
		// event_que.push_back( new_event );
	} else if (sevent.EventType == irr::EET_USER_EVENT) {
		// cout << "[xsimulation] USER_EVENT " << endl;
		// new_event->EventType = XLOG_EVENT; // the type ov event
		// event_que.push_back( new_event );
	} else {
		cout << "[xsimulation] ERROR : undefined event" << endl;
	}
	// cout << "[xsimulation] ERROR : event fail" << endl;
    // delete new_event;
	return false;
}


int xsimulation::getEventCount() {
	return event_que.size();
}

xevent* xsimulation::getEvent() {
	// cout << "[xsimulation] getEvent" << endl;
	// cout << "[xsimulation] event_que.size:" << event_que.size() << endl;
	if ( event_que.size() > 0) {
		xevent *e = event_que[0];
		return e;
	}
	return NULL;
} // xsimulation::getEvent()

xelement * xsimulation::getCaller( irr::gui::IGUIElement * c){
	// cout << "[xsimulation] getCaller" << endl;
	// test to see if xelement is irrelement
	for (int i = 0; i < references.size(); i++){
		if (c == references[i]->element_reference ){
			// cout << "[xsimulation] callerIndex:" << i << endl;
			return references[i];
		}
	}
	// cout << "[xsimulation] ERROR : undefined event reference." << endl;
	return NULL;
}

void xsimulation::destroy() {
	// cout << "[xsimulation] destroy" << endl;
	// irr_device->closeDevice();                // shut down IrrLicht device??

#if IRR_PROFILE
	cout << "[xsimulation] printing metrics" << endl;
	gPROFILER.PrintAll();
#endif // IRR_PROFILE

	irr_device->drop();
	exit(1);
}

//
// GUI elements
//

xtext * xsimulation::addText( int id, char * c, int x1, int x2, int y1, int y2 ){
	xtext *e = new xtext( irr_device, id, c, x1, x2, y1, y2  );
	addReference( e );
	return e;
} // xsimulation::addText

xcheckbox * xsimulation::addCheckbox(int id, bool b, char * c, int x1, int x2, int y1, int y2 ){
	xcheckbox *e = new xcheckbox( irr_device, id, b, c, x1, x2, y1, y2  );
	addReference( e );
	return e;
} // xsimulation::addCheckbox

xbutton * xsimulation::addButton( int id, x2drect r, char * c){
	xbutton *e = new xbutton( irr_device, id, r, c );
	addReference( e );
	return e;
} // xsimulation::addButton

xwindow * xsimulation::addWindow( int id, x2drect r, char * c){
	// cout << "[xsimulation] xsimulation::addWindow" << endl;
	xwindow *e = new xwindow( irr_device, id, r, c );
	addReference( e );
	return e;
} // xsimulation::addWindow

xlight * xsimulation::addLight(  int id, x3dpoint pos, xcolour c, float radius ){
	xlight * e = new xlight( irr_device, id, pos, c, radius );
	addReference( e );
	return e;
}

xcamera * xsimulation::addCamera(  int id, XCAMERA_TYPE camtype, int x1, int y1, int z1, int x2, int y2, int z2 ){
	xcamera *e = new xcamera( irr_device, id, camtype, x1, y1, z1, x2, y2, z2  );
	addReference( e );
	return e;
} // xsimulation::addCamera

xmesh * xsimulation::addMesh( int id, char * mesh_file ){
	xmesh *e = new xmesh(irr_device, id, mesh_file);
	addReference( e );
	return e;
} // xsimulation::addMesh

xcubemesh * xsimulation::addCubeMesh( int id ){
	xcubemesh *e = new xcubemesh(irr_device, id);
	addReference( e );
	return e;
} // xsimulation::addCubeMesh

xspheremesh * xsimulation::addSphereMesh( int id ){
	xspheremesh *e = new xspheremesh(irr_device, id);
	addReference( e );
	return e;
} // xsimulation::addSphereMesh

xterrainmesh * xsimulation::addTerrainMesh( int id, char * mesh_file ){
	xterrainmesh *e = new xterrainmesh(irr_device, id, mesh_file);
	addReference( e );
	return e;
} // xsimulation::addTerrainMesh

xparticlesystem * xsimulation::addParticleSystem( int id, char * tex_file ){
	xparticlesystem *e = new xparticlesystem(irr_device, id, tex_file);
	addReference( e );
	return e;
} // xsimulation::addParticleSystem

xskybox * xsimulation::addSkybox( int id, char * filename1,
			char * filename2,
			char * filename3,
			char * filename4,
			char * filename5,
			char * filename6){
	xskybox * e = new xskybox(irr_device, id, filename1, filename2, filename3, filename4, filename5, filename6 );
	addReference( e );
	return e;
} // xsimulation::addSkybox

xgrid * xsimulation::addGrid( int id, int sz, int num ){
	xgrid * e = new xgrid(irr_device, id, sz, num);
	addReference( e );
	return e;
}

xtextnode * xsimulation::addTextNode( int id, xfont * f, char * txt, xcolour c ){
	xtextnode * e = new xtextnode( irr_device, id, f, txt, c );
	addReference( e );
	return e;
}

xscrollbar * xsimulation::addScrollbar( int id, bool o, x2drect r ){
	xscrollbar * e = new xscrollbar(irr_device, id, o, r );
	addReference( e );
	return e;
} // xsimulation::addScrollbar

xlistbox * xsimulation::addListbox( int id, bool b, x2drect r ){
	xlistbox * e = new xlistbox(irr_device, id, b, r );
	addReference( e );
	return e;
} // xsimulation::addListbox

xcombobox * xsimulation::addCombobox( int id, x2drect r ){
	xcombobox * e = new xcombobox( irr_device, id, r );
	addReference( e );
	return e;
} // xsimulation::addCombobox

xcontextmenu * xsimulation::addContextMenu( int id, x2drect r ){
	xcontextmenu * e = new xcontextmenu( irr_device, id, r );
	addReference( e );
	return e;
} // xsimulation::addContextMenu

xcontextmenu * xsimulation::addMenu( int id ){
	xcontextmenu * e = new xcontextmenu( irr_device, id );
	addReference( e );
	return e;
} // xsimulation::addContextMenu

xtable * xsimulation::addTable( int id, bool b, x2drect r ){
	xtable * e = new xtable(irr_device, id, b, r );
	addReference( e );
	return e;
} // xsimulation::addTable

xeditbox * xsimulation::addEditbox( int id, x2drect r ){
	xeditbox * e = new xeditbox(irr_device, id, r );
	addReference( e );
	return e;
} // xsimulation::addEditbox

xtextbox * xsimulation::addTextbox( int id, x2drect r ){
	xtextbox * e = new xtextbox(irr_device, id, r );
	addReference( e );
	return e;
} // xsimulation::addTextbox

xchatbox * xsimulation::addChatbox( int id, x2drect r ){
	xchatbox * e = new xchatbox(irr_device, id, r );
	addReference( e );
	return e;
}

x3dline * xsimulation::add3dLine( int id, x3dpoint s, x3dpoint e ){
	x3dline * l = new x3dline(irr_device, id, s, e );
	addReference( l );
	return l;
} // xsimulation::add3dLine

x3dcircle * xsimulation::add3dCircle( int id, x3dpoint c, float r ){
	x3dcircle * l = new x3dcircle(irr_device, id, c, r );
	addReference( l );
	return l;
} // xsimulation::add3dCircle

xbeam * xsimulation::addBeam( int id, char * tx_fn1, char * tx_fn2  ){
	xbeam * b = new xbeam(irr_device, id, tx_fn1, tx_fn2 );
	addReference( b );
	return b;
} // xsimulation::addBeam

xbillboard * xsimulation::addBillboard( int id, x2dpoint p ){
	xbillboard * b = new xbillboard(irr_device, id, p);
	addReference( b );
	return b;
} // xsimulation::addBillboard

xsprite * xsimulation::addSprite( int id, char * tx_fn1, char * tx_fn2, char * tx_fn3, char * tx_fn4 ){
	xsprite * b = new xsprite(irr_device, id, tx_fn1, tx_fn2, tx_fn3, tx_fn4);
	addReference( b );
	return b;
} // xsimulation::addBillboard

ximage * xsimulation::addImage( int id, xtexture * t, x2dpoint p, x2drect r, xcolour c ){
	ximage * i = new ximage(irr_device, id, t, p, r, c );
	addReference( i );
	return i;
} // xsimulation::addImage

xshader * xsimulation::addShader(){
	// cout << "[xsimulation] adding shader" << endl;
	xshader * s = new xshader( irr_device );
	// addReference( l ); // not an element?
	return s;
} // xshader::add3dLine

void xsimulation::addReference(xelement* e ){
    // cout << "[xsimulation] addReference" << endl;
	references.push_back( e );
} // xsimulation::addReference

void xsimulation::printTree(){
	cout << "[xsimulation] printing tree" << endl;
	cout << "------------------------------------------" << endl;
	cout << "XSIMULATION" << endl;
	for (int i = 0; i < references.size(); i++){
		printElement( references[i] );
	}
	cout << "------------------------------------------" << endl;
	cout << "Size: " << references.size() << endl;

} // xsimulation::printTree

void xsimulation::printElement( xelement* e ){ // int depth
	// cout << "-xelement" << endl;
	e->printType();
	// e->attributes
	for (int i = 0; i< e->attributes.size(); i++){
		cout << "--" << e->attributes[i].name << " : " << e->attributes[i].value << endl;
	}
	// for (int i = 0; i< e->references.size(); i++){
	// 	printElement( e->references[i] );
	// }
} // xsimulation::printElement

void xsimulation::bindkey ( const char *a, XKEY_EVENT_TYPE e ){
	// cout << "[xsimulation] bindKey" << endl;
	xkeybind *keybind = new xkeybind();
	keybind->action = a;
	keybind->key_event = e;
	keys.push_back( keybind );
} // xsimulation::bindkey

// deprecated
bool xsimulation::isKey( const char *a ){
	// cout << "[xsimulation] isKey" << endl;
	for ( int n = 0; n < keys.size(); n++ ) {
		if (a == keys[n]->action ){
			return keybuffer[ keys[n]->key_event ];
		}
	}
	return false;
} // xsimulation::isKey

bool xsimulation::isKeyDown( const char * a ){
	// cout << "[xsimulation] isKey" << endl;
	for ( int n = 0; n < keys.size(); n++ ) {
		if (a == keys[n]->action ){
		    if ( keybuffer[ keys[n]->key_event ] == true and keybuffer_old[ keys[n]->key_event ] == false ){
                return true;
		    }
		}
	}
	return false;
} // xsimulation::isKeyDown

bool xsimulation::isKeyPressed( const char * a ){
	// cout << "[xsimulation] isKeyPressed" << endl;
	for ( int n = 0; n < keys.size(); n++ ) {
		if (a == keys[n]->action ){
            if ( keybuffer[ keys[n]->key_event ] == true and keybuffer_old[ keys[n]->key_event ] == true ){
                // cout << "keybuffer_delay:" << keybuffer_delay[ keys[n]->key_event ] << endl;
                if ( keybuffer_delay[ keys[n]->key_event ] == 0 ) {
                    keybuffer_delay[ keys[n]->key_event ] = keyDelay;
                    return true;
                }
            }
		}
	}
	return false;
} // xsimulation::isKeyPressed

bool xsimulation::isKeyUp( const char * a ){
	// cout << "[xsimulation] isKey" << endl;
	for ( int n = 0; n < keys.size(); n++ ) {
		if (a == keys[n]->action ){
		    if ( keybuffer[ keys[n]->key_event ] == false and keybuffer_old[ keys[n]->key_event ] == true ){
                return true;
		    }
		}
	}
	return false;
} // xsimulation::isKeyUp

void xsimulation::setKeyDelay( int d ){
    keyDelay = d;
} // xsimulation::setKeyDelay

void xsimulation::ignoreKeyRepeat( bool r ){
    keyRepeat = r;
} // xsimulation::ignoreKeyRepeat

void xsimulation::bindGUI ( const char *a, int i, XGUI_EVENT_TYPE e ){
	// cout << "[xsimulation] bindGUI" << endl;
	xguibind *guibind = new xguibind();
	guibind->action = a;
	guibind->gui_event = e;
	guibind->id = i;
	gui.push_back( guibind );
	guibuffer.push_back( false );
} // xsimulation::bindGUI

bool xsimulation::isGUI ( const char *a ){
	// cout << "[xsimulation] isGUI" << endl;
	for ( int n = 0; n < gui.size(); n++ ) {
		if (a == gui[n]->action ){
			if ( guibuffer[ n ] == true ){
				return true;
			} else {
				return false;
			}
		}
	}
	return false;
} // xsimulation::isGUI

void xsimulation::updateKeyBuffer(){
	// cout << "[xsimulation] updating keybuffer " << endl;
	for ( int i = 0; i< XKEY_XKEY_CODES_COUNT; i++ ){
	    if ( keybuffer_delay[i] > 0 ) { keybuffer_delay[i] = keybuffer_delay[i] - 1; }
	    keybuffer_old[i] = keybuffer[i];
		// keybuffer[i] = false;
	}
} // xsimulation::updateKeyBuffer

void xsimulation::clearKeyBuffer(){
    // cout << "[xsimulation] clearing keybuffer " << endl;
	for ( int i = 0; i< XKEY_XKEY_CODES_COUNT; i++ ){
		keybuffer[i] = false;
	    keybuffer_old[i] = false;
	    keybuffer_delay[i] = 0;
	}
} // xsimulation::clearKeyBuffer

void xsimulation::clearGUIBuffer(){
	// cout << "[xsimulation] clearing guibuffer " << endl;
	for ( int i = 0; i< guibuffer.size(); i++ ){
		guibuffer[i] = false;
	}
} // xsimulation::clearGUIBuffer

void xsimulation::clearEventQue(){
	// cout << "[xsimulation] clearing event que " << endl;
    // TODO: FIXME memory leak here ^^
    // delete all pointers in the vector
    while ( !event_que.empty() ) {
        // get first 'element'
        xevent *e = event_que.front();
        // remove it from the list
        event_que.erase( event_que.begin() );
        // delete the pointer
        delete e;
    }
} // xsimulation::clearGUIBuffer

void xsimulation::removeEvent() {
	// cout << "[xdynamics] removeCollision" << endl;
    xevent *e = event_que.front();
    // remove it from the list
    event_que.erase( event_que.begin() );
    // delete the pointer
    delete e;
} // xsimulation::removeEvent

void xsimulation::setWindowCaption(char * text){
	stream.str("");
	stream.clear();
	swdata  = text;
	wsdata  = swdata.c_str();
	irr_device->setWindowCaption( wsdata );
} // xsimulation::setWindowCaption

int xsimulation::getFPS(){
    return irr_driver->getFPS();
} // xsimulation::getFPS

int xsimulation::getTris(){
    return irr_driver->getPrimitiveCountDrawn();
} // xsimulation::getTris

void xsimulation::setSkinColor ( XGUI_DEFAULT_COLOR which, xcolour c ){
	SColor col;
	col.setAlpha( c.A );
	col.setRed( c.R );
	col.setGreen( c.G );
	col.setBlue( c.B );
	irr_env->getSkin()->setColor( (irr::gui::EGUI_DEFAULT_COLOR)which, col );
} // xsimulation::setSkinColor

xcolour * xsimulation::getSkinColor ( XGUI_DEFAULT_COLOR which ){
	SColor col = irr_env->getSkin()->getColor( (irr::gui::EGUI_DEFAULT_COLOR)which );
	// translate SColor to xcolour
	xcolour* xc = new xcolour();
	xc->A = col.getAlpha();
	xc->R = col.getRed();
	xc->G = col.getGreen();
	xc->B = col.getBlue();
	return xc;
} // xsimulation::getSkinColor

xtexture * xsimulation::getTexture(char * file){
	xtexture * texture = new xtexture( irr_device, file );
	return texture;
} // xsimulation::setTimestep

xfont * xsimulation::getFont(  char * file ){
	xfont * font = new xfont( irr_device, file );
	return font;
} // xsimulation::getFont

void xsimulation::setFont( xfont * f, int which ){
	irr_env->getSkin()->setFont ( f->getFont(), (EGUI_DEFAULT_FONT)which );
} // xsimulation::setFont

xfont * xsimulation::getBuiltInFont(){
	xfont * font = new xfont( irr_device );
	return font;
} // xsimulation::getBuiltInFont

void xsimulation::makeColorKeyTexture(xtexture * t, x2dpoint p){
	irr_driver->makeColorKeyTexture(t->texture, core::position2d<s32>( p.X, p.Y ));
} // xsimulation::makeColorKeyTexture

void xsimulation::draw2DImage( xtexture * t, x2dpoint p, x2drect r, xcolour c ){
	// cout << "[xsimulation] draw2DImage" << endl;
	irr_driver->draw2DImage( t->texture, core::position2d<s32>( p.X, p.Y ),
		core::rect< s32 >(r.X1, r.Y1,r.X2, r.Y2 ),
		0, video::SColor( c.A, c.R, c.G, c.B ), true);
} // xsimulation::draw2DImage

void xsimulation::setTimestep( float f){
	// timestamp = f;
} // xsimulation::setTimestep

void xsimulation::setAmbientLight( xcolour c ){
	// cout << "[xsimulation] setAmbientLight" << endl;
	// cout << "[xsimulation] A" << c.A << endl;
	// cout << "[xsimulation] R" << c.R << endl;
	// cout << "[xsimulation] G" << c.G << endl;
	// cout << "[xsimulation] B" << c.B << endl;
	irr_smgr->setAmbientLight( c.getSColour() );
	if ( effect ){
		effect->setAmbientColor( c.getSColour() );
	}
}

void xsimulation::setShadowColour( xcolour c ){
	// cout << "[xsimulation] setAmbientLight" << endl;
	// cout << "[xsimulation] A" << c.A << endl;
	// cout << "[xsimulation] R" << c.R << endl;
	// cout << "[xsimulation] G" << c.G << endl;
	// cout << "[xsimulation] B" << c.B << endl;
	irr_smgr->setShadowColor( c.getSColour() );
}

void xsimulation::setFog ( xcolour colour,  bool linear, float start, float end, float density, bool pixel, bool range ){
    // todo : fixme
	// irr_driver->setFog   ( colour.getSColour(), linear, start, end, density, pixel, range );
}

bool eventReceiver::OnEvent(const irr::SEvent& event){
	// cout << "[eventReceiver] eventReceiver event: " << event.EventType << endl;
	// if (event.EventType == irr::EET_LOG_TEXT_EVENT) {
	//	std::fstream LogFile("LogFile.log",std::ios::out|std::ios::app);
	//	LogFile << (event.LogEvent.Text) << std::endl;
	//	LogFile.close();
	//	return true;
	// }
	xsim->event( event );
	return false;
} // eventReceiver::OnEvent

int xsimulation::getNodeIDFromScreenCoordinates( x2dpoint p ){
	ISceneNode* tnode = irr_smgr->getSceneCollisionManager()->getSceneNodeFromScreenCoordinatesBB( position2d< s32 >( p.X, p.Y ), 0, false);
	//
	if ( tnode != NULL ) {
        return tnode->getID();
	}
    return NULL;
} // xsimulation::getNodeIDFromScreenCoordinates

x3dpoint xsimulation::getIntersection( x2dpoint p,  int nid ){ // 2d point on screen, node id?
	// cout << "[xsimulation] getIntersection" << endl;
	core::line3d<f32> line;
	line = irr_smgr->getSceneCollisionManager()->getRayFromScreenCoordinates( position2d< s32 >( p.X, p.Y ),
		irr_smgr->getActiveCamera() );

	core::vector3df intersection;
	core::triangle3df tri;

	x3dpoint i = x3dpoint(0,0,0);
	if ( irr_smgr->getSceneCollisionManager()->getSceneNodeAndCollisionPointFromRay( line, intersection, tri, 0) ) {
		i = x3dpoint( intersection.X, intersection.Y, intersection.Z );
		// cout << "[xsimulation] getIntersection ok" << endl;
		return i;
	}
	// cout << "[xsimulation] getIntersection fail" << endl;

	// irr_smgr->getSceneCollisionManager()->getCollisionPoint (line, sel, intersection, tri, tnode);
	return i;
} // xsimulation::getIntersection

x2dpoint xsimulation::getScreenCoordinatesFrom3DPoint( x3dpoint p ){
	// cout << "[xsimulation] getScreenCoordinatesFrom3DPoint" << endl;
	core::position2d<s32> sc = irr_smgr->getSceneCollisionManager()->getScreenCoordinatesFrom3DPosition( p.getv3df(), irr_smgr->getActiveCamera() );

	return x2dpoint( sc.X, sc.Y );
} // xsimulation::getNodeIDFromScreenCoordinates

x3dline * xsimulation::get3dlineFromScreenCoordinates( x2dpoint p ){
    // cout << "[xsimulation] get3dlineFromScreenCoordinates" << endl;
    core::line3d<f32> line;
    line = irr_smgr->getSceneCollisionManager()->getRayFromScreenCoordinates( position2d< s32 >( p.X, p.Y ) );
    // cout << "[xsimulation] get3dlineFromScreenCoordinates" << endl;
	x3dline * l = new x3dline( irr_device, -1, x3dpoint(line.start.X, line.start.Y, line.start.Z), x3dpoint(line.end.X, line.end.Y, line.end.Z) );
	l->setVisible( false );
	addReference( l );
	return l;
} // x2dpoint xsimulation::get3dlineFromScreenCoordinates
