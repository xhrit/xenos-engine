// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xterrainmesh.cpp

#include "xterrainmesh.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xterrainmesh::xterrainmesh( irr::IrrlichtDevice *dev, int id, char * mesh_fn ) : xelement( dev, id ){
	cout << "[xterrainmesh] create xterrainmesh" << endl;
	terrain = irr_smgr->addTerrainSceneNode(
                mesh_fn,
                0,                                      // parent node
                id,                                     // node id
                core::vector3df(0.f, 0.f, 0.f),         // position
                core::vector3df(0.f, 0.f, 0.f),         // rotation
                core::vector3df(1, 1, 1),               // scale
                video::SColor ( 255, 255, 255, 255 ),   // vertexColor
                5,                                      // maxLOD
                scene::ETPS_17,                         // patchSize
                4                                       // smoothFactor
                );
	setType( XELEMENT_TYPE_MESH );
}

xterrainmesh::~xterrainmesh(){
	cout << "[xterrainmesh] delete xterrainmesh" << endl;
}

int xterrainmesh::update(){
	// cout << "[xterrainmesh] xterrainmesh::update" << endl;
	return 1;
}

int xterrainmesh::render(){
	// cout << "[xterrainmesh] xterrainmesh::render" << endl;
	return 1;
}

bool xterrainmesh::event(){
	cout << "[xterrainmesh] xterrainmesh::event" << endl;
	return false;
} // xterrainmesh::event

void xterrainmesh::destroy(){
	cout << "[xterrainmesh] xterrainmesh::destroy" << endl;
	terrain->remove();
	setType( XELEMENT_TYPE_NULL );
} // xterrainmesh::destroy

void xterrainmesh::setPosition( x3dpoint p){
	// cout << "[xterrainmesh] setPosition" << endl;
	position = p;
	terrain->setPosition ( p.getv3df() );
} // xterrainmesh::setPosition

void xterrainmesh::setRotation( x3dpoint p){
	// cout << "[xterrainmesh] setRotation" << endl;
	rotation = p;
	terrain->setRotation ( p.getv3df() );
} // xterrainmesh::setRotation

void xterrainmesh::setScale(x3dpoint p) {
	cout << "[xterrainmesh] setScale" << endl;
	scale = p;
	terrain->setScale ( p.getv3df() );
} // xterrainmesh::setScale

void xterrainmesh::setMaterialFlag( XMATERIAL_FLAG mf, bool val ){
	terrain->setMaterialFlag ( (E_MATERIAL_FLAG)mf, val );
} // xmesh::setMaterialFlag

// TODO: fixme!!!
void xterrainmesh::setMaterialType( XMATERIAL_TYPE mt ){ // ?
	terrain->setMaterialType( (E_MATERIAL_TYPE)mt );
} // xmesh::setMaterialType

void xterrainmesh::addNormalMap( char * bump, float factor ){
	video::ITexture* normalMap = irr_driver->getTexture( bump );
	irr_driver->makeNormalMapTexture( normalMap, factor );
	terrain->setMaterialTexture( 1, normalMap );
} // xmesh::addNormalMap

void xterrainmesh::addTexture( char * fn, int index ){
	// cout << "[xterrainmesh] addTexture: " << tex_fn << endl;
	terrain->setMaterialTexture( index, irr_driver->getTexture( fn ) );
} // xterrainmesh::addTexture

void xterrainmesh::scaleTexture( float a, float b ){
	// cout << "[xterrainmesh] scaleTexture: " << tex_fn << endl;
	terrain->scaleTexture( a, b );
} // xterrainmesh::scaleTexture


