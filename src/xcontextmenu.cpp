// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xcontextmenu.cpp

#include "xcontextmenu.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

xcontextmenu::xcontextmenu( irr::IrrlichtDevice *dev, int id, x2drect r ) : xelement( dev, id ){
	cout << "[xcontextmenu" << id <<"] create xcontextmenu (dropdown)" << endl;
	contextmenu  = irr_env->addContextMenu( rect<int>( r.X1, r.Y1, r.X2, r.Y2 ), 0, id );
	element_reference = contextmenu;

	setType( XELEMENT_TYPE_CONTEXTMENU );
}

xcontextmenu::xcontextmenu( irr::IrrlichtDevice *dev, int id ) : xelement( dev, id ){
	cout << "[xcontextmenu" << id <<"] create xcontextmenu (menu)" << endl;
	contextmenu  = irr_env->addMenu( 0, id );
	element_reference = contextmenu;

	setType( XELEMENT_TYPE_CONTEXTMENU );
}

xcontextmenu::xcontextmenu( irr::IrrlichtDevice *dev, int id, IGUIContextMenu * m ) : xelement( dev, id ){
	cout << "[xcontextmenu" << id <<"] create xcontextmenu (submenu)" << endl;
	if (m == 0){ cout << "error, no submenu" << endl; }
	contextmenu  = m;
	element_reference = contextmenu;

	setType( XELEMENT_TYPE_CONTEXTMENU );
}

xcontextmenu::~xcontextmenu(){
	cout << "[xcontextmenu" << element_id <<"] delete xcontextmenu" << endl;
	// TODO: finish this.
}

int xcontextmenu::update(){
	// cout << "[xcontextmenu" << element_id <<"] xcontextmenu::update" << endl;
	return 1;
}

int xcontextmenu::render(){
	// cout << "[xcontextmenu" << element_id <<"] xcontextmenu::render" << endl;
	return 1;
}

bool xcontextmenu::event(){
	// cout << "[xcontextmenu" << element_id <<"] xcontextmenu::event" << endl;
	return false;
}

void xcontextmenu::destroy(){
	cout << "[xcontextmenu" << element_id <<"] xcontextmenu::destroy" << endl;
	contextmenu->remove();
}

void xcontextmenu::setVisible( bool v ){
	// cout << "[xcontextmenu" << element_id <<"] setVisible" << endl;
	contextmenu->setVisible( v );
} // xcontextmenu::setVisible

void xcontextmenu::setParent( xelement * x ){
	// cout << "[xcontextmenu" << element_id <<"] setParent" << endl;
	parent = x;
	// contextmenu->setParent( x->getGfxElement() );
} // xcontextmenu::setParent

void xcontextmenu::addItem( int id, char * c, bool sub ){
	// cout << "[xcontextmenu" << element_id <<"] addItem" << endl;
	stream.str("");
	stream.clear();
	swdata  = c;
	wsdata  = swdata.c_str();

	int a = contextmenu->addItem( wsdata, id, true, sub );
	// cout << "index : " << a << endl;
} // xcontextmenu::setVisible

char * xcontextmenu::getItem ( int i ){
	core::stringc s = contextmenu->getItemText( i );
	return strdup( s.c_str() );
} // xcontextmenu::getItem

int xcontextmenu::getSelected(){
	return contextmenu->getSelectedItem();
} // xcontextmenu::getSelected

xcontextmenu * xcontextmenu::getSubMenu( int id, int nid ){
	// cout << "[xcontextmenu" << element_id <<"] getSubMenu" << endl;
	// cout << "return submenu (id "<< nid << ") for menu item id " << id << endl;
	if ( contextmenu->getSubMenu( id ) == 0 ) { cout << "error, not returning a submenu" << endl; }
	xcontextmenu * e = new xcontextmenu( irr_device, nid, contextmenu->getSubMenu( id ) );
	// cout << "ok" << endl;
	return e;
} // xcontextmenu::getSelected

void xcontextmenu::addSeparator(){
	contextmenu->addSeparator();
} // xcontextmenu::getSelected
