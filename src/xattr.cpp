// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#include "xattr.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

xattr::xattr(){
}

xattr::~xattr(){
}

void xattr::setType( XATTR_TYPE t ){
	type = t;
}

void xattr::setName( char * n){
	name = n;
}

void xattr::setValue( char * v ){ // , XATTR_TYPE type
	value = v;
}

char * xattr::getName(){
	return name;
}

char * xattr::getValue(){
	return value;
}
