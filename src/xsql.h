// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence

#ifndef XSQL_H
#define XSQL_H

#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <iterator>

#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>

class xresult {
	public:
		xresult(){};
		~xresult(){};

		int getHeadCount(){ return vhead.size(); }
		int getDataCount(){ return vdata.size(); }
		int getRowCount(){
            if ( getHeadCount() == 0 ) {
                return 0;
            } else {
                return getDataCount() / getHeadCount();
            }
		}

		///
		/// return header.
		/// \param i index to return
		///
		const char *  getHead( int i ){
			while ( i >= vhead.size() ){
				i = i - vhead.size();
			}
			return vhead[ i ].c_str();
		}

		///
		/// return data.
		/// \param i index to return
		///
		const char * getData( int i ){ return vdata[ i ].c_str(); }

		void printAll(){
			cout << "[xsql] printing head" << endl;
			std::string p = "";
			for (int i = 0; i < getHeadCount(); i ++){
				// p = p + vhead[ i ].c_str() + ";";
				cout << vhead[ i ].c_str() << endl;
			}
			// cout << p << endl;
			cout << "[xsql] printing data" << endl;
			for (int i = 0; i < getDataCount(); i ++){
				cout << vdata[ i ].c_str() << endl;
				// int t = 0;
				// p = "";
				// for (int n = 0; n < getHeadCount(); n ++){
				// 	p = p + vdata[ t ].c_str() + ";";
				//	t++;
				//}
				// cout << p << endl;
			}
		}

		std::vector< std::string > vhead;
		std::vector< std::string > vdata;

	private:
};


///
/// xsql
///
/// This is the database interface.
///

class xsql {
	public:
		///
		/// constructor.
		/// \param tablename string of table's name
		///
		xsql( char * tablename ): zErrMsg(0), rc(0), db_open(0) {
			rc = sqlite3_open(tablename, &db);
			if( rc ){
				fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
				sqlite3_close(db);
			}
			db_open=1;
		}

		///
		/// destructor.
		///
		~xsql(){
			sqlite3_close(db);
		}

		///
		/// execute sql statement.
		/// \param s_exe string of statement to run
		///
		xresult * exe( char * s_exe ) {
			// cout << "\n[xsql] exe " << s_exe << endl;
			rc = sqlite3_get_table(
					db,              /* An open database */
					s_exe,       /* SQL to be executed */
					&result,       /* Result written to a char *[]  that this points to */
					&nrow,             /* Number of result rows written here */
					&ncol,          /* Number of result columns written here */
					&zErrMsg          /* Error msg written here */
					);
			// cout << "[xsql] a" << endl;

			if( rc == SQLITE_OK ){
				// cout << "[xsql] ncol:" << ncol << endl;
				// cout << "[xsql] nrow:" << nrow << endl;
				if ( ncol > 0 and nrow > 0 ){

                    xresult * r = new xresult();
                    for(int i=0; i < ncol; ++i)

                        if (result[i] != NULL ){    /* First row heading */
                            r->vhead.push_back( result[i] );
                        } else {
                            r->vhead.push_back( "" );
                        }
                    for(int i=0; i < ncol*nrow; ++i)
                        if (result[ncol+i] != NULL ){    /* First row heading */
                            r->vdata.push_back(result[ncol+i]);
                        } else {
                            r->vdata.push_back( "" );
                        }
                    sqlite3_free_table(result);
                    return r;
				} else {
                    // cout << "[xsql] query ok " << endl;
                    sqlite3_free_table(result);
                    return NULL;
				}
			} else {
				cout << "[xsql] error in query : " << s_exe << endl;
				cout << "[xsql] " << sqlite3_errmsg(db) << endl;
                sqlite3_free_table(result);
                return NULL;
			}
		}

        void removeResult( xresult * r){
            delete r;
        }

	private:
		sqlite3 *db;
		char *zErrMsg;
		char **result;
		int rc;
		int nrow,ncol;
		int db_open;
		int retcount;

		// vector< xresult * > result_que;
};

#endif // XSQL_H

