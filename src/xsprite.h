// xenos engine
// copyright 2007-2012 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xsprite.h

#ifndef XSPRITE_H
#define XSPRITE_H

#include "xelement.h"       // Simulation objects
#include "CSprite.h" // ...

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xsprite : 3d mesh
///
///
///

class xsprite : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param mesh character array ov mesh to load.
		/// \param tex texture to load.
		///
		xsprite( irr::IrrlichtDevice *dev, int id, char * tx_fn1, char * tx_fn2, char * tx_fn3, char * tx_fn4);

		///
		/// destructor.
		///
		~xsprite();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();

		void setPosition( x3dpoint p );
		void setSize( x2dpoint p );
        void setVisible( bool val );

		void setMaterialFlag( XMATERIAL_FLAG mf, bool val );
		void setMaterialType( XMATERIAL_TYPE mt );
        void setTextureMatrix( xmatrix4 m );
        void setTexture( char * tex, int layer );

		x3dpoint getPosition();
		x2dpoint getSize();

		CSprite* node;

	private:
		x3dpoint scale;
		bool vis;
};

#endif // XSPRITE_H
