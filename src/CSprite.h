#ifndef CSPRITE_H
#define CSPRITE_H
 #include <irrlicht.h>

 namespace irr
 {
     namespace scene
     {
         class CSprite : public ISceneNode
         {
         private:
            // The sprite material layers
            video::SMaterial material1;    // base
            video::SMaterial material2;    // armor
            video::SMaterial material3;    // head
            video::SMaterial material4;    // weapon


            core::dimension2d<f32> Size;
            core::aabbox3d<f32> BBox;

            video::S3DVertex vertices[4];
            u16 indices[6];
           	core::vector3df pos;

         public:

            CSprite(ISceneNode* parent, ISceneManager* mgr, s32 id, char* base, char* arm, char* head , char* wep);

            ~CSprite(void);

            virtual void OnRegisterSceneNode(void);

            virtual void OnAnimate(u32 timeMs);

            virtual void render(void);

            virtual const core::aabbox3d<f32>& getBoundingBox() const;

            virtual u32 getMaterialCount() const;

            virtual video::SMaterial& getMaterial(u32 i);
            void setTexture( char* tex, int layer );

            virtual void setSize(const core::dimension2d<f32>& size);

            virtual void setColor(const video::SColor & overallColor);

            virtual void setPosition( core::vector3df p );
            virtual core::vector3df getPosition();



        };
     }
 }



#endif // CSPRITE_H
