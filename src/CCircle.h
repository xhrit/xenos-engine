#ifndef CCIRCLE_H_INCLUDED
#define CCIRCLE_H_INCLUDED

#include <irrlicht.h>

namespace irr{
namespace scene{

class CCircle : public ISceneNode{
    public:
        CCircle(core::vector3df center, f32 radius, u32 vertexcount, core::vector3df normal, ISceneNode* parent, ISceneManager* smgr, s32 id=-1);
        virtual void OnRegisterSceneNode();
        virtual void render();
        virtual const core::aabbox3d<f32>& getBoundingBox() const;
        virtual u32 getMaterialCount();
        virtual video::SMaterial& getMaterial(u32 i);
        virtual void setMaterial(video::SMaterial newMaterial);
        void createCircle(core::vector3df center, f32 radius, u32 vertexcount, core::vector3df normal);
        void setColour(irr::video::SColor color);

    private:
        core::array<video::S3DVertex> lstVertices;
        core::array<u16> lstIndices;
        core::aabbox3df Box;
        video::SMaterial Material;
        video::IVideoDriver* Driver;
        f32 Radius;
        int vertex_count;
        core::vector3df Normal;
        core::vector3df Center;
};

}} // namespaces

#endif // CCIRCLE_H_INCLUDED
