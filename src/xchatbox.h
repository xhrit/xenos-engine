// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xchatbox.h

#ifndef XCHATBOX_H
#define XCHATBOX_H

#include "xelement.h"          // Simulation objects

#include <irrlicht.h>
#include <sstream>
#include <string>

using namespace std;
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

///
/// xchatbox : static text
///
/// This is a static text element.
///

class xchatbox : public xelement {
	public:

		///
		/// constructor.
		/// \param sim pointer to xsimulation
		/// \param txt string.
		/// \param x1 an integer argument.
		/// \param x2 an integer argument.
		/// \param y1 an integer argument.
		/// \param y21 an integer argument.
		///
		xchatbox( irr::IrrlichtDevice *dev, int id, x2drect r );

		///
		/// destructor.
		///
		~xchatbox();

		///
		/// update seq
		/// @return  error code
		///
		int update();

		///
		/// render seq
		/// @return  error code
		///
		int render();

		///
		/// event seq
		/// @return success
		///
		bool event();

		///
		/// Destroy element
		///
		void destroy();
        void setParent( xelement * x );
        void addMessage( char * txt, xcolour c );
        void setVisible( bool v );

        void setLife( int i );
        void setFont( xfont * f );

	private:
		ostringstream stream;      // conversion variables (?)
		core::stringw swdata;            // conversion variables (?)
		const wchar_t* wsdata;     // conversion variables (?)
		// std::string result;     // conversion variables (?)
		IGUIChatQueue* chatbox;
};

#endif // XCHATBOX_H
