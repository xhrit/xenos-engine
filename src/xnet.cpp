// xenos engine
// copyright 2007-2011 lukas.w.irwin [ lukas.w.irwin@gmail.com ]
// GNU Public Licence
// xnet.cpp

#include "xnet.h"
#include <irrNet.h>

void ServerNetCallback::onConnect(const u16 playerId) {
	cout << "[ServerNetCallback] onConnect" << endl;
	net::SOutPacket packet;
	core::stringc message;
	message = "Client number ";
	message += playerId;
	message += " has just connected.";
	cout << message.c_str() << endl;

	packet << XOP_CONN;
	packet << message;
	netManager->sendOutPacket(packet);

	xpacket * p = new xpacket();
	p->setMessage( strdup( message.c_str() ) );
	p->setID( playerId );
	p->setType( XOP_CONN );
	message_que.push_back( p ); // wtf??!
	// cout << "[ServerNetCallback] /onConnect" << endl;
} // ServerNetCallback::onConnect

// Similar to the onConnect function, except it happens when a
// player disconnects. When this happens we will just report
// which player has disconnected.
void ServerNetCallback::onDisconnect(const u16 playerId) {
	cout << "[ServerNetCallback] onDisconnect" << endl;
	net::SOutPacket packet;
	core::stringc message;
	message = "Client number ";
	message += playerId;
	message += " has disconnected.";
	packet << XOP_DISCO;
	packet << message;
	netManager->sendOutPacket(packet);

	// cout << "[ServerNetCallback] packet type: " << XPACKET_MESSAGE << endl; //XPACKET_DISCONNECT

	xpacket * p = new xpacket();
	p->setMessage( strdup( message.c_str() ) );
	p->setID( playerId );
	p->setType( XOP_DISCO );
	message_que.push_back( p ); // wtf??!
	// cout << "[ServerNetCallback] /onDisconnect" << endl;
} // ServerNetCallback::onDisconnect

// Handle the packets, as usual.
void ServerNetCallback::handlePacket(net::SInPacket& packet){
	// cout << "[ServerNetCallback] handlePacket" << endl;
	// Very simple callback, just add what the clients say to the server que.
	int type;
	packet >> type;
	core::stringc message;
	packet >> message;
	u16 playerId = packet.getPlayerId();
	// cout << "[ServerNetCallback] Client #" << playerId << " packet type: " << type << " message: "<< message.c_str() << endl;

	xpacket * p = new xpacket();
	p->setMessage( strdup( message.c_str() ) );
	p->setID( playerId );
	p->setType( type );
	message_que.push_back( p ); // wtf??!
	// cout << "[ServerNetCallback] /handlePacket" << endl;
} // ServerNetCallback::handlePacket

xpacket * ServerNetCallback::get() {
	// cout << "[ServerNetCallback] get" << endl;
	xpacket * n;
	if ( !message_que.empty() ) {
		n = message_que[ 0 ];
		message_que.erase( message_que.begin() );
	} else {
		// cout << "[ServerNetCallback] return null" << endl;
		return 0; // n = "null";
	}
	return n;
} // ServerNetCallback::get

int ServerNetCallback::getSize() {
    return message_que.size();
} // ServerNetCallback::get

void ClientNetCallback::handlePacket(net::SInPacket& packet) {
	// cout << "[ClientNetCallback] handlePacket" << endl;
	// Very simple callback, just add what the server says to the client que.
	int type;
	packet >> type;
	core::stringc message;
	packet >> message;
	// cout << "[ClientNetCallback] Server packet type: " << type << " message: "<< message.c_str() << endl;

	xpacket * p = new xpacket();
	p->setMessage( strdup( message.c_str() ) );
	p->setID(-1 );
	p->setType( type );
	message_que.push_back( p ); // wtf??!
	// cout << "[ClientNetCallback] /handlePacket" << endl;
} // ClientNetCallback::handlePacket

xpacket * ClientNetCallback::get() {
	// cout << "[ClientNetCallback] get" << endl;
	xpacket * n;
	if ( !message_que.empty() ) {
		n = message_que[ 0 ];
		message_que.erase( message_que.begin() );
	} else {
		// cout << "[ClientNetCallback] return null" << endl;
		return 0; // n = "null";
	}
	return n;
} // ClientNetCallback::get

int ClientNetCallback::getSize() {
    return message_que.size();
} // ClientNetCallback::get

xnet::xnet(){
    packets_sent = 0;
    packets_received = 0;
}

xnet::~xnet(){
	// Clean up.
	delete netManager;
	if ( net_state == 1){ delete clientCallback; }
	if ( net_state == 2){ delete serverCallback; }
}

void xnet::server( int p ){
	// cout << "[xnet] create server" << endl;
	// If they typed 's' they are the server else they are the client.
	// Create an irrNetLite server.
	netManager = net::createIrrNetServer(0, p);

	// Pass in a server specific net callback.
	serverCallback = new ServerNetCallback(netManager);
	netManager->setNetCallback(serverCallback);

	net_state = 2; // use enum?
} // xnet::server

void xnet::client( char * ip, int p ){
	// cout << "[xnet] create client" << endl;
	// Create a client and pass in the client callback.
	// You may want to change the ip address to a remote one and experiment
	// with connecting to a remote host, especially for this example as,
	// if you run all the clients from the same pc and ban one, you
	// won't be able to create anymore clients unless you restart the server.
	clientCallback = new ClientNetCallback();
	netManager = net::createIrrNetClient(clientCallback, ip, p);

	net_state = 1; // use enum?
} // xnet::client

int xnet::update(){
	// cout << "[xnet] update" << endl;
	// Here is the update loop, we will exit if there is a connection problem.
	if ( netManager->getConnectionStatus() != net::EICS_FAILED ) {
		// cout << "[xnet] netManager->update" << endl;
		// Here we update.
		netManager->update( 0 );
	} else {
		// cout << "[xnet] update error" << endl;
		// change status? return error code
		return 0;
	}
	// cout << "[xnet] /update" << endl;
    return 1;
} // xnet::update

void xnet::send( int t, char * m ){
	// cout << "[xnet] send" << endl;
	// If there wasn't a problem connecting we can send message.
	if ( netManager->getConnectionStatus() != net::EICS_FAILED ) {
		// cout << "[xnet] a" << endl;
		// Take the input & Send a packet with the message entered. std::string message
		net::SOutPacket packet;
		packet << t;
		packet << m;
		netManager->sendOutPacket(packet);
		// cout << "[xnet] b" << endl;

		packets_sent += 1;
	} else {
		// cout << "[xnet] transmission error" << endl;
		// change status / return error code
	}
} // xnet::send

int xnet::getPing(){
    return netManager->getPing();
} // xnet::getPing

int xnet::getPacketsSent(){
    return packets_sent;
} // xnet::getPacketsSen

int xnet::getPacketsReceived(){
    return packets_received;
} // xnet::getPacketsReceived


void xnet::setVerbose( bool b ){
    netManager->setVerbose( b );
} // xnet::getPacketsReceived

void xnet::sendTo( int t, char * m, int uid ){
	// cout << "[xnet] sendTo" << endl;
	// If there wasn't a problem connecting we can send message.
	if ( netManager->getConnectionStatus() != net::EICS_FAILED ) {
		// cout << "[xnet] ok" << endl;
		// Take the input & Send a packet with the message entered. std::string message
		if ( net_state == 1){
			// cout << "[xnet] client, send to server" << endl;
			// client
			send (t, m);
		} else {
			// cout << "[xnet] server, send to client id :" << uid << endl;
			net::SOutPacket packet;
			packet << t;
			packet << m;
			netManager->sendOutPacket(packet, uid);
		}

		packets_sent += 1;
	} else {
		// cout << "[xnet] transmission error" << endl;
		// change status / return error code
	}
} // xnet::sendTo

xpacket * xnet::get(){
	// cout << "[xnet] get" << endl;
	if ( net_state == 1){
	    if (clientCallback->getSize() > 0 ) { packets_received += 1; }
		return clientCallback->get();
	} else {
	    if (serverCallback->getSize() > 0 ) { packets_received += 1; }
		return serverCallback->get();
	}
} // xnet::get
