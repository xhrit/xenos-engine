#!/bin/sh
# xenos engine build script
#

echo "starting xenos engine build script"

# build Irrlicht
echo "building Irrlicht"
cd irrlicht_mod/source/Irrlicht
make
cd ../../..

# build irrNet
cd irrNetLite/source
make
cd ../..

echo "all dependencies built"
# build Xenos
cd src
make


echo "xenos engine build finished"
cp xenos ../xenos_engine/xenos

echo "cd to xenos_engine and type './xenos dungeontactics' to start"

exit
# end of file
